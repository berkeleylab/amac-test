# README #
                         
Software and Firmware for AMAC chip testing.

hdl project can be generated using hdlmake
sw applications can be generated with common gcc

Needs a test board with an AMAC mounted. Mojo is programmed with MOJO loader (https://embeddedmicro.com/mojo-v3.html)

Working SW that has been tested recently:
AMAC_test_full
AMAC_test_full_fast

