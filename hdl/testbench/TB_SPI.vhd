--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:38:56 08/07/2016
-- Design Name:   
-- Module Name:   C:/Users/tobia/Dropbox/Bachelor/_GIT/hdl/sim/TB_SPI.vhd
-- Project Name:  Mojo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DAC_spi
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_SPI IS
END TB_SPI;
 
ARCHITECTURE behavior OF TB_SPI IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DAC_spi
    PORT(
         clk_i : IN  std_logic;
         rst_i : IN  std_logic;
         channel_i : IN  std_logic_vector(3 downto 0);
         val_i : IN  std_logic_vector(11 downto 0);
         val_o : OUT  std_logic_vector(11 downto 0);
         write_i : IN  std_logic;
         spi_ss_o : OUT  std_logic_vector(2 downto 0);
         spi_sclk_o : OUT  std_logic_vector(2 downto 0);
         spi_mosi_o : OUT  std_logic_vector(2 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk_i : std_logic := '0';
   signal rst_i : std_logic := '0';
   signal channel_i : std_logic_vector(3 downto 0) := (others => '0');
   signal val_i : std_logic_vector(11 downto 0) := (others => '0');
   signal write_i : std_logic := '0';

 	--Outputs
   signal val_o : std_logic_vector(11 downto 0);
   signal spi_ss_o : std_logic_vector(2 downto 0);
   signal spi_sclk_o : std_logic_vector(2 downto 0);
   signal spi_mosi_o : std_logic_vector(2 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DAC_spi PORT MAP (
          clk_i => clk_i,
          rst_i => rst_i,
          channel_i => channel_i,
          val_i => val_i,
          val_o => val_o,
          write_i => write_i,
          spi_ss_o => spi_ss_o,
          spi_sclk_o => spi_sclk_o,
          spi_mosi_o => spi_mosi_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst_i <= '1';
      wait for 100 ns;	
		rst_i <= '0';
		wait for 100ns;
		val_i <= x"5A0";
		channel_i <= x"A";
		wait for clk_i_period;
		write_i <= '1';
		wait for clk_i_period;
		write_i <= '0';
		wait;

      wait for clk_i_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
