--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:55:21 08/12/2016
-- Design Name:   
-- Module Name:   C:/Users/tobia/Dropbox/Bachelor/_GIT/hdl/sim/TB_clk_ana.vhd
-- Project Name:  Mojo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk_ana
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_clk_ana IS
END TB_clk_ana;
 
ARCHITECTURE behavior OF TB_clk_ana IS 
 
  -- Component Declaration for the Unit Under Test (UUT)
 
  COMPONENT clk_ana
    PORT(
      clk_i : IN  std_logic;
      rst_i : IN  std_logic;
      clk_ana_i : IN  std_logic;
      high_time_o : OUT  std_logic_vector(15 downto 0);
      low_time_o : OUT  std_logic_vector(15 downto 0);
      trans_ctr_o : OUT  std_logic_vector(31 downto 0);
      timespan_i : IN  std_logic_vector(15 downto 0);
      start_i : IN  std_logic;
      busy_o : OUT  std_logic
      );
  END COMPONENT;


  --Inputs
  signal clk_i : std_logic := '0';
  signal rst_i : std_logic := '0';
  signal clk_ana_i : std_logic := '0';
  signal timespan_i : std_logic_vector(15 downto 0) := (others => '0');
  signal start_i : std_logic := '0';

  --Outputs
  signal high_time_o : std_logic_vector(15 downto 0);
  signal low_time_o : std_logic_vector(15 downto 0);
  signal trans_ctr_o : std_logic_vector(31 downto 0);
  signal busy_o : std_logic;

  -- Clock period definitions
  constant clk_i_period : time := 10 ns;
  constant clk_ana_i_period : time := 1000 ns;
 
BEGIN
 
  -- Instantiate the Unit Under Test (UUT)
  uut: clk_ana PORT MAP (
    clk_i => clk_i,
    rst_i => rst_i,
    clk_ana_i => clk_ana_i,
    high_time_o => high_time_o,
    low_time_o => low_time_o,
    trans_ctr_o => trans_ctr_o,
    timespan_i => timespan_i,
    start_i => start_i,
    busy_o => busy_o
    );

  -- Clock process definitions
  clk_i_process :process
  begin
    clk_i <= '0';
    wait for clk_i_period/2;
    clk_i <= '1';
    wait for clk_i_period/2;
  end process;
 
  clk_ana_i_process :process
  begin
    clk_ana_i <= '0';
    wait for clk_ana_i_period/5*3;
    clk_ana_i <= '1';
    wait for clk_ana_i_period/5*2;
    clk_ana_i <= '0';
    wait for clk_ana_i_period/5*1;
    clk_ana_i <= '1';
    wait for clk_ana_i_period/5*4;
  end process;


  -- Reset stimulus
  stim_reset: process
  begin		
    rst_i <= '1';
    wait for 100 ns;	
    rst_i <= '0';

    wait;
  end process;
	
  -- Transition counter stimulus
  stim_transctr: process
  begin		
    wait for 500 us;
    timespan_i <= x"000A";
    start_i <= '1';
    wait for 20 us;	
    start_i <= '0';
    wait;
  end process;

END;
