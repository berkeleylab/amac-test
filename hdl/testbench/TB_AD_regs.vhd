--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:10:04 08/12/2016
-- Design Name:   
-- Module Name:   C:/Users/tobia/Dropbox/Bachelor/_GIT/hdl/sim/TB_AD_regs.vhd
-- Project Name:  Mojo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: AD_regs
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY TB_AD_regs IS
END TB_AD_regs;
 
ARCHITECTURE behavior OF TB_AD_regs IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT AD_regs
    PORT(
         clk_i : IN  std_logic;
         rst_i : IN  std_logic;
         cclk_i : IN  std_logic;
         spi_miso_o : OUT  std_logic;
         spi_mosi_i : IN  std_logic;
         spi_sck_i : IN  std_logic;
         spi_ss_i : IN  std_logic;
         spi_channel_o : OUT  std_logic_vector(3 downto 0);
         ADC_channel0_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel1_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel4_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel5_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel6_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel7_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel8_o : OUT  std_logic_vector(9 downto 0);
         ADC_channel9_o : OUT  std_logic_vector(9 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk_i : std_logic := '0';
   signal rst_i : std_logic := '0';
   signal cclk_i : std_logic := '0';
   signal spi_mosi_i : std_logic := '1';
   signal spi_sck_i : std_logic := '1';
   signal spi_ss_i : std_logic := '1';

 	--Outputs
   signal spi_miso_o : std_logic;
   signal spi_channel_o : std_logic_vector(3 downto 0);
   signal ADC_channel0_o : std_logic_vector(9 downto 0);
   signal ADC_channel1_o : std_logic_vector(9 downto 0);
   signal ADC_channel4_o : std_logic_vector(9 downto 0);
   signal ADC_channel5_o : std_logic_vector(9 downto 0);
   signal ADC_channel6_o : std_logic_vector(9 downto 0);
   signal ADC_channel7_o : std_logic_vector(9 downto 0);
   signal ADC_channel8_o : std_logic_vector(9 downto 0);
   signal ADC_channel9_o : std_logic_vector(9 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
   constant SPI_clock_period : time := 1000 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: AD_regs PORT MAP (
          clk_i => clk_i,
          rst_i => rst_i,
          cclk_i => cclk_i,
          spi_miso_o => spi_miso_o,
          spi_mosi_i => spi_mosi_i,
          spi_sck_i => spi_sck_i,
          spi_ss_i => spi_ss_i,
          spi_channel_o => spi_channel_o,
          ADC_channel0_o => ADC_channel0_o,
          ADC_channel1_o => ADC_channel1_o,
          ADC_channel4_o => ADC_channel4_o,
          ADC_channel5_o => ADC_channel5_o,
          ADC_channel6_o => ADC_channel6_o,
          ADC_channel7_o => ADC_channel7_o,
          ADC_channel8_o => ADC_channel8_o,
          ADC_channel9_o => ADC_channel9_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      rst_i <= '1';
      cclk_i <= '0';
      wait for 100 ns;	
      rst_i <= '0';
		wait for 300 ns;
      cclk_i <= '1';
      wait;
   end process;

   -- SPI Stimulus process
   stim_SPI: process
		procedure send_byte_spi (
			val: in std_logic_vector(7 downto 0)) is
		begin
			wait for SPI_clock_period / 2;
			for i in val'range loop
				spi_sck_i <= '0';
				spi_mosi_i <= val(i);
				wait for SPI_clock_period / 2;
				spi_sck_i <= '1';
				wait for SPI_clock_period / 2;
			end loop;
			spi_mosi_i <= '1';
			wait for SPI_clock_period;			
		end;
		procedure send_val_spi (
			channel: in std_logic_vector(3 downto 0);
			val: in std_logic_vector(9 downto 0)) is
		begin
			spi_ss_i <= '0';
			send_byte_spi(val(7 downto 0));
			send_byte_spi(channel & "00" & val(9 downto 8));		
			spi_ss_i <= '1';
		end;
		variable val_to_send_v : integer := 16#111#;
   begin		
		wait until cclk_i = '1';
		wait for SPI_clock_period*5;
		while true loop
			send_val_spi(spi_channel_o, std_logic_vector(to_unsigned(val_to_send_v,10)));
			val_to_send_v := val_to_send_v * 2; 
			wait for SPI_clock_period;
		end loop;
   end process;

END;
