--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:15:02 07/12/2016
-- Design Name:   
-- Module Name:   C:/Users/tobia/Dropbox/Bachelor/_GIT/hdl/sim/TB_serial2wb.vhd
-- Project Name:  Mojo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rs232_syscon
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
use work.UART_behavioural_model.all; 
 
ENTITY TB_serial2wb IS
END TB_serial2wb;
 
ARCHITECTURE behavior OF TB_serial2wb IS 
 
  -- Component Declaration for the Unit Under Test (UUT)
 
  COMPONENT rs232_syscon
    PORT(
      clk_i : IN  std_logic;
      rst_p_i : IN  std_logic;
      ser_rxd_i : IN  std_logic;
      ser_txd_o : OUT  std_logic;
      wb_adr_o : OUT  std_logic_vector(15 downto 0);
      wb_dat_i : IN std_logic_vector(31 downto 0); 
      wb_dat_o : OUT std_logic_vector(31 downto 0); 
      wb_we_o : OUT  std_logic;
      wb_stb_o : OUT  std_logic;
      wb_cyc_o : OUT  std_logic;
      wb_rst_o : OUT  std_logic;
      wb_ack_i : IN  std_logic;
      wb_err_i : IN  std_logic
      );
  END COMPONENT;
    

  --Inputs
  signal clk_i : std_logic := '0';
  signal rst_p_i : std_logic := '0';
  signal ser_rxd_i : std_logic := '1';
  signal wb_ack_i : std_logic := '0';
  signal wb_err_i : std_logic := '0';

  --BiDirs
  signal wb_dat_i : std_logic_vector(31 downto 0);
  signal wb_dat_o : std_logic_vector(31 downto 0);

  --Outputs
  signal ser_txd_o : std_logic;
  signal wb_adr_o : std_logic_vector(15 downto 0);
  signal wb_we_o : std_logic;
  signal wb_stb_o : std_logic;
  signal wb_cyc_o : std_logic;
  signal wb_rst_o : std_logic;

  -- Clock period definitions
  constant clk_i_period : time := 10 ns;

  -- Simulation signals
  type buf_byte_t is array (Natural range <>) of std_logic_vector(7 downto 0);
  signal UART_rd_buf : buf_byte_t(1 to 10) := (others => (others => 'Z'));
  signal UART_rx_active : std_logic := '0';

BEGIN

  -- Instantiate the Unit Under Test (UUT)
  uut: rs232_syscon PORT MAP (
    clk_i => clk_i,
    rst_p_i => rst_p_i,
    ser_rxd_i => ser_rxd_i,
    ser_txd_o => ser_txd_o,
    wb_adr_o => wb_adr_o,
    wb_dat_o => wb_dat_o,
    wb_dat_i => wb_dat_i,
    wb_we_o => wb_we_o,
    wb_stb_o => wb_stb_o,
    wb_cyc_o => wb_cyc_o,
    wb_rst_o => wb_rst_o,
    wb_ack_i => wb_ack_i,
    wb_err_i => wb_err_i
    );

  -- Clock process definitions
  clk_i_process :process
  begin
    clk_i <= '0';
    wait for clk_i_period/2;
    clk_i <= '1';
    wait for clk_i_period/2;
  end process;


  -- Stimulus process
  stim_proc: process
  begin		
    -- hold reset state for 100 ns.
    rst_p_i <= '1';
    wait for 100 ns;	
    rst_p_i <= '0';
    wait for clk_i_period*10;

    -- insert stimulus here 

    wait;
  end process;
	
  -- Synchronous Stimulus process
  sync_proc: process(clk_i)
  begin		
    if rising_edge(clk_i) then
      wb_ack_i <= '0';
      wb_err_i <= '0';
      wb_dat_i <= (others => '0');
      if wb_stb_o = '1' then
        wb_ack_i <= '1';
        if wb_we_o = '0' then
          wb_dat_i <= x"12345678";
        end if;
      end if;
    end if;
  end process;
	
  UART_stim: process
    constant My_Baud_Rate: integer := 500000;
    procedure send (data: in std_logic_vector(7 downto 0)) is
    begin
      UART_tx(
        tx_line         => ser_rxd_i,
        data            => data,
        baud_rate       => My_Baud_Rate
        );
    end;
    procedure send_error (data: in std_logic_vector(7 downto 0)) is
    begin
      UART_tx_with_error(
        tx_line => ser_rxd_i,
        data => data,
        baud_rate => My_Baud_Rate
        );
    end;
    procedure write_wb (
      reg: in std_logic_vector(15 downto 0); 
      val: in std_logic_vector(31 downto 0)) is
    begin
      send('1' & reg(14 downto 8));
      send(reg(7 downto 0));
      send(val(31 downto 24));
      send(val(23 downto 16));
      send(val(15 downto 8));
      send(val(7 downto 0));
      send(x"0A"); --'Enter		
    end;
    procedure write_wb_error (
      reg: in std_logic_vector(15 downto 0); 
      val: in std_logic_vector(31 downto 0)) is
    begin
      send('1' & reg(14 downto 8));
      send(reg(7 downto 0));
      send(val(31 downto 24));
      send_error(val(23 downto 16));
      send(val(15 downto 8));
      send(val(7 downto 0));
      send(x"0A"); --'Enter		
    end;
    procedure read_wb (
      reg: in std_logic_vector(15 downto 0)) is
    begin
      send('0' & reg(14 downto 8));
      send(reg(7 downto 0));
      send(x"0A"); --'Enter		
    end;
  begin		
    wait until rst_p_i = '0';	
    wait for 100us;
    send(x"0A"); --Enter	
    wait for 100us;
    send(x"0D"); --Enter	
    wait for 100us;
    send(x"0A"); --Enter	
    wait for 100us;
    send(x"0D"); --Enter	again (Invalid command)

    wait for 100us;
    write_wb(x"2AAA", x"ABCDEF01");
    wait for 250us; -- For the module to init
    read_wb(x"2AAA");
    wait for 500us; -- For the module to init
		
    send(x"80");
    send(x"80");
    send(x"0A");
    send(x"0D");
    wait for 500us; -- For the module to init
		
    write_wb(x"AAAA", x"ABCDEF01");

    wait;
  end process;
	
  UART_read: process
    constant My_Baud_Rate: integer := 500000;
    procedure receive (data: out std_logic_vector(7 downto 0)) is
    begin
      UART_rx(
        rx_line => ser_txd_o,
        data => data,
        baud_rate => My_Baud_Rate
        );
    end;
    variable read_char : std_logic_vector(7 downto 0);
    variable index : integer := 1;
  begin
    UART_rx_active <= '0';
    wait until ser_txd_o = '0';
    UART_rx_active <= '1';
		
    receive(read_char);
    if read_char = x"0A" then
      for i in UART_rd_buf'range loop
        UART_rd_buf(i) <= (others => 'Z');
      end loop;
      index := 1;
    else
      UART_rd_buf(index) <= read_char;
      index := index + 1;
    end if;
  end process;

END;
