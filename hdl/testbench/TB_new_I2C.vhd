--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:51:15 07/14/2016
-- Design Name:   
-- Module Name:   C:/Users/tobia/Dropbox/Bachelor/_GIT/hdl/sim/TB_new_I2C.vhd
-- Project Name:  Mojo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: AMACtest_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
use work.UART_behavioural_model.all; 
 
ENTITY TB_new_I2C IS
END TB_new_I2C;
 
ARCHITECTURE behavior OF TB_new_I2C IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT AMACtest_top
    PORT(
         clk : IN  std_logic;
         rst_n : IN  std_logic;
         cclk : IN  std_logic;
         spi_sck : IN  std_logic;
         spi_ss : IN  std_logic;
         spi_mosi : IN  std_logic;
         spi_miso : OUT  std_logic;
         spi_channel : OUT  std_logic_vector(3 downto 0);
         avr_tx : IN  std_logic;
         avr_rx : OUT  std_logic;
         amac_scl : INOUT  std_logic;
         amac_sda : INOUT  std_logic;
         spi_dac_sck : OUT  std_logic_vector(2 downto 0);
         spi_dac_ss : OUT  std_logic_vector(2 downto 0);
         spi_dac_mosi : OUT  std_logic_vector(2 downto 0);
         amac_clk_o : OUT  std_logic;
         amac_clk_i : IN  std_logic;
         amac_resetb_o : OUT  std_logic;
         amac_id_o : OUT  std_logic_vector(3 downto 0);
         amac_mux_pos_o : OUT  std_logic_vector(1 downto 0);
         amac_mux_en_o : OUT  std_logic;
         en_1v5_o : OUT  std_logic;
         en_1v2_o : OUT  std_logic;
         en_IO_o : OUT  std_logic;
         amac_hvcntrl_i : IN  std_logic;
         amac_hvcntrlb_i : IN  std_logic;
         amac_lvcntrl_i : IN  std_logic;
         amac_lvcntrlb_i : IN  std_logic;
         amac_ilocklam_i : IN  std_logic;
         amac_warnlam_i : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst_n : std_logic := '0';
   signal cclk : std_logic := '0';
   signal spi_sck : std_logic := '0';
   signal spi_ss : std_logic := '0';
   signal spi_mosi : std_logic := '0';
   signal avr_tx : std_logic := '1';
   signal amac_clk_i : std_logic := '0';
   signal amac_hvcntrl_i : std_logic := '0';
   signal amac_hvcntrlb_i : std_logic := '0';
   signal amac_lvcntrl_i : std_logic := '0';
   signal amac_lvcntrlb_i : std_logic := '0';
   signal amac_ilocklam_i : std_logic := '0';
   signal amac_warnlam_i : std_logic := '0';

	--BiDirs
   signal amac_scl : std_logic;
   signal amac_sda : std_logic;

 	--Outputs
   signal spi_miso : std_logic;
   signal spi_channel : std_logic_vector(3 downto 0);
   signal avr_rx : std_logic;
   signal spi_dac_sck : std_logic_vector(2 downto 0);
   signal spi_dac_ss : std_logic_vector(2 downto 0);
   signal spi_dac_mosi : std_logic_vector(2 downto 0);
   signal amac_clk_o : std_logic;
   signal amac_resetb_o : std_logic;
   signal amac_id_o : std_logic_vector(3 downto 0);
   signal amac_mux_pos_o : std_logic_vector(1 downto 0);
   signal amac_mux_en_o : std_logic;
   signal en_1v5_o : std_logic;
   signal en_1v2_o : std_logic;
   signal en_IO_o : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant cclk_period : time := 10 ns;
 
	-- Simulation signals
	type buf_byte_t is array (Natural range <>) of std_logic_vector(7 downto 0);
	signal UART_rd_buf : buf_byte_t(1 to 10) := (others => (others => 'Z'));
	signal UART_rx_active : std_logic := '0';
  
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: AMACtest_top PORT MAP (
          clk => clk,
          rst_n => rst_n,
          cclk => cclk,
          spi_sck => spi_sck,
          spi_ss => spi_ss,
          spi_mosi => spi_mosi,
          spi_miso => spi_miso,
          spi_channel => spi_channel,
          avr_tx => avr_tx,
          avr_rx => avr_rx,
          amac_scl => amac_scl,
          amac_sda => amac_sda,
          spi_dac_sck => spi_dac_sck,
          spi_dac_ss => spi_dac_ss,
          spi_dac_mosi => spi_dac_mosi,
          amac_clk_o => amac_clk_o,
          amac_clk_i => amac_clk_i,
          amac_resetb_o => amac_resetb_o,
          amac_id_o => amac_id_o,
          amac_mux_pos_o => amac_mux_pos_o,
          amac_mux_en_o => amac_mux_en_o,
          en_1v5_o => en_1v5_o,
          en_1v2_o => en_1v2_o,
          en_IO_o => en_IO_o,
          amac_hvcntrl_i => amac_hvcntrl_i,
          amac_hvcntrlb_i => amac_hvcntrlb_i,
          amac_lvcntrl_i => amac_lvcntrl_i,
          amac_lvcntrlb_i => amac_lvcntrlb_i,
          amac_ilocklam_i => amac_ilocklam_i,
          amac_warnlam_i => amac_warnlam_i
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   cclk_process :process
   begin
		cclk <= '0';
		wait for cclk_period/2;
		cclk <= '1';
		wait for cclk_period/2;
   end process;
 
	rst_stim: process
	begin
		rst_n <= '0';
		wait for 100ns;
		rst_n <= '1';
		wait;
	end process;
	
	amac_scl <= 'H';
	amac_sda <= 'H';	
	
	UART_stim: process
		constant My_Baud_Rate: integer := 500000;
		procedure send (data: in std_logic_vector(7 downto 0)) is
		begin
			UART_tx(
			tx_line => avr_tx,
			data => data,
			baud_rate => My_Baud_Rate
			);
		end;
		procedure write_wb (
			reg: in std_logic_vector(15 downto 0); 
			val: in std_logic_vector(31 downto 0)) is
		begin
			send('1' & reg(14 downto 8));
			send(reg(7 downto 0));
			send(val(31 downto 24));
			send(val(23 downto 16));
			send(val(15 downto 8));
			send(val(7 downto 0));
			send(x"0A"); --'Enter		
		end;
		procedure read_wb (
			reg: in std_logic_vector(15 downto 0)) is
		begin
			send('0' & reg(14 downto 8));
			send(reg(7 downto 0));
			send(x"0A"); --'Enter		
		end;
   begin
      wait until rst_n = '1';	
		wait for 100us;
		send(x"0A"); --Enter	
		wait for 100us;
		send(x"0D"); --Enter	
		wait for 100us;
		send(x"0A"); --Enter	
		wait for 100us;
		send(x"0D"); --Enter	again (Invalid command)
		wait for 100us;

		--Set Enable bit (0x01) in I2C Control Register (0x112)
		write_wb(x"0112", x"00000001");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--Set I2C Clock Prescaler to 9 -> fI2C = 1MHz
		write_wb(x"0000", x"00000000");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		write_wb(x"0001", x"00000003");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--Set Enable bit (0x80) in Control Register (0x02)
		write_wb(x"0002", x"00000080");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		
		--Write TXR (0x03) with slave address (0x55) and write bit (0)
		write_wb(x"0003", x"000000AA");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--Start write by writing 0x90 to Control Register (0x04)
		write_wb(x"0004", x"00000090");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);	
		
		--Write TXR with byte to send (0x55)
		write_wb(x"0003", x"00000055");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--Continue write by writing 0x10 to Control Register (0x04)
		write_wb(x"0004", x"00000010");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);	
		
		--Write TXR with byte to send (0x00)
		write_wb(x"0003", x"00000000");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--End of writing (Byte + Stop Condition) by writing 0x50 to Control Register (0x04)
		write_wb(x"0004", x"00000050");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);		
		
		
		--Write TXR (0x03) with slave address (0x55) and read bit (1)
		write_wb(x"0003", x"000000AB");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);
		--Start write by writing 0x90 to Control Register (0x04)
		write_wb(x"0004", x"00000090");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);	
		--Read Byte by writing 0x20 to Control Register (0x04)
		write_wb(x"0004", x"00000020");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);	
		--Get read Byte by reading the Transfer Register (0x03)
		read_wb(x"0003");
		wait until UART_rx_active = '0' and UART_rx_active'stable(10us);	
		
		wait;
   end process;
	
	UART_read: process
		constant My_Baud_Rate: integer := 500000;
		procedure receive (data: out std_logic_vector(7 downto 0)) is
		begin
			UART_rx(
			rx_line => avr_rx,
			data => data,
			baud_rate => My_Baud_Rate
			);
		end;
		variable read_char : std_logic_vector(7 downto 0);
		variable index : integer := 1;
	begin
		UART_rx_active <= '0';
		wait until avr_rx = '0';
		UART_rx_active <= '1';
		
		receive(read_char);
		if read_char = x"0A" then
			for i in UART_rd_buf'range loop
				UART_rd_buf(i) <= (others => 'Z');
			end loop;
			index := 1;
		else
			UART_rd_buf(index) <= read_char;
			index := index + 1;
		end if;
	end process;

END;
