target = "xilinx"
action = "synthesis"

syn_device = "xc6slx9"
syn_grade = "-2"
syn_package = "tqg144"
syn_top = "AMACtest_top"
syn_project = "AMACtest.xise"
syn_tool = "ise"

files = [
    "mojo.ucf"]

modules = { "local" : ["../../rtl",
                       "../../ip_cores/clk"]}
