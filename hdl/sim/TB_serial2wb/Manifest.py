action = "simulation"
sim_tool = "isim"
sim_top = "TB_serial2wb"

sim_post_cmd = "./isim_proj -gui -view ../../testbench/TB_serial2wb.wcfg"

modules = {
    "local" : [ "../../testbench" ],
}
