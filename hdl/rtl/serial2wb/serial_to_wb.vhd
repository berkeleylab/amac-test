----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:34:32 06/09/2016 
-- Design Name: 
-- Module Name:    serial_to_wb - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_misc.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;



-- Composition:
-- 2 Bytes ADDR : Bit 15 = R(1)/W(0)
-- 4 Byte Data
-- Transfer Format:
         -- WRITE:
         --   Master: Address (2)
         --   Master: Data (4)
         --   Slave: Status(1)
         -- READ:
         --   Master: Address (2)
         --   Slave: Status(1)
         --   Slave: Data(4) [when no error]
 -- Timeout for answer: 1ms
 -- Every transmission is terminated with a synchronization character (0x0A)


entity rs232_syscon is
  PORT(
    clk_i       : IN  std_logic;
    rst_p_i     : IN  std_logic;
    ser_rxd_i   : IN  std_logic;   
    ser_txd_o   : OUT std_logic; 
    wb_adr_o    : OUT std_logic_vector(15 downto 0);
    wb_dat_i    : IN  std_logic_vector(31 downto 0); 
    wb_dat_o    : OUT std_logic_vector(31 downto 0); 
    wb_we_o     : OUT std_logic;  
    wb_stb_o    : OUT std_logic;
    wb_cyc_o    : OUT std_logic;   
    wb_rst_o    : OUT std_logic;
    wb_ack_i    : IN  std_logic;
    wb_err_i    : IN  std_logic
    );
end rs232_syscon;

architecture arch of rs232_syscon is

  constant START_BITS : integer := 1;
  constant DATA_BITS : integer := 8;
  constant STOP_BITS : integer := 1;
  constant CLOCK_FACTOR : integer := 8;
  constant c_timeout_cycles : integer := 5000;

  component rs232tx is
    generic(
      START_BITS_PP            : integer := 1;
      DATA_BITS_PP             : integer := 8;
      STOP_BITS_PP             : integer := 1;
      CLOCK_FACTOR_PP          : integer := 16;
      TX_BIT_COUNT_BITS_PP     : integer := 4
      );
    port(
      clk              : in  std_logic;
      tx_clk           : in  std_logic;
      reset            : in  std_logic;
      load             : in  std_logic;
      data             : in  std_logic_vector(DATA_BITS_PP - 1 downto 0);
      load_request     : out std_logic;
      txd              : out std_logic
      );
  end component;

  component rs232rx is
    generic(
      START_BITS_PP     : integer := 1;
      DATA_BITS_PP      : integer := 8;
      STOP_BITS_PP      : integer := 1;
      CLOCK_FACTOR_PP   : integer := 16
      );
    port(
      clk               : in  std_logic;
      rx_clk            : in  std_logic;
      reset             : in  std_logic;
      rxd               : in  std_logic;
      read              : in  std_logic;
      data              : out std_logic_vector(DATA_BITS_PP - 1 downto 0);
      data_ready        : out std_logic;
      error_over_run    : out std_logic;
      error_under_run   : out std_logic;
      error_all_low     : out std_logic
      );
  end component;

  component auto_baud is
    generic(
      CLOCK_FACTOR_PP          : integer := 16;
      LOG2_MAX_COUNT_PP        : integer := 16
      );
    port(
      clk_i                    : in std_logic;
      reset_i                  : in std_logic;
      serial_dat_i             : in std_logic;
      auto_baud_locked_o       : out std_logic;
      baud_clk_o               : out std_logic
      );       
  end component;

  signal rst_int_s, rst_rx_s : std_logic;
	
  type buf_byte_t is array (Natural range <>) of std_logic_vector(7 downto 0);
  signal rx_buf_s : buf_byte_t(6 downto 0);
  signal tx_buf_s : buf_byte_t(5 downto 0);

  signal serial_clock_s, serial_clock_locked_s : std_logic;
  signal serial_data_tx_s, serial_data_rx_s : std_logic_vector(DATA_BITS - 1 downto 0);
  signal serial_load_s, serial_load_requested_s : std_logic;
  signal rx_data_rdy_s, rx_read_s : std_logic;
  signal rx_error_s : std_logic_vector(2 downto 0);
  signal rx_cmd_error_s : std_logic;
  signal cmd_ready_s, wb_active_s : std_logic;
  signal tx_send_s, wb_we_s, tx_databytes_s : std_logic;

  -- wishbone fsm
  type wb_state_t is (idle,waitack);
  signal wb_state_s : wb_state_t;
  
  -- rx fsm
  type rx_state_t is (idle,recieve);
  signal rx_state_s : rx_state_t;

  -- tx fsm
  type tx_state_t is (idle,send,errsend0,errsend1);
  signal tx_state_s : tx_state_t;

begin

  Inst_baud_detect : auto_baud
    generic map(
      CLOCK_FACTOR_PP   => CLOCK_FACTOR,
      LOG2_MAX_COUNT_PP => 16
      )
    port map(
      clk_i             => clk_i,
      reset_i           => rst_p_i,
      serial_dat_i      => ser_rxd_i,
      auto_baud_locked_o=> serial_clock_locked_s,
      baud_clk_o        => serial_clock_s
      );

  Inst_tx : rs232tx
    generic map(
      START_BITS_PP     => START_BITS,
      DATA_BITS_PP      => DATA_BITS,
      STOP_BITS_PP      => STOP_BITS,
      CLOCK_FACTOR_PP   => CLOCK_FACTOR
      )
    port map(
      clk               => clk_i,
      reset             => rst_p_i,
      tx_clk            => serial_clock_s,
      load              => serial_load_s,
      data              => serial_data_tx_s,
      load_request      => serial_load_requested_s,
      txd               => ser_txd_o
      );

  Inst_rx : rs232rx
    generic map(
      START_BITS_PP     => START_BITS,
      DATA_BITS_PP      => DATA_BITS,
      STOP_BITS_PP      => STOP_BITS,
      CLOCK_FACTOR_PP   => CLOCK_FACTOR
      )
    port map(
      clk               => clk_i,
      reset             => rst_rx_s,
      rx_clk            => serial_clock_s,
      read              => rx_read_s,
      data              => serial_data_rx_s,
      rxd               => ser_rxd_i,
      data_ready        => rx_data_rdy_s,
      error_over_run    => rx_error_s(0),
      error_under_run   => rx_error_s(1),
      error_all_low     => rx_error_s(2)
      );
	
  -- Reset logic until locked to baud rate
  rst_int_s <= rst_p_i or (not serial_clock_locked_s);
  -- Reset rx module if error appeared
  rst_rx_s <= rst_int_s or OR_REDUCE(rx_error_s);

  serial_read: process(rst_int_s, clk_i)
    variable byte_ctr_v : integer;
  begin
    if rst_int_s = '1' then
      rx_state_s                <=  idle;
      rx_read_s                 <= '0';
      cmd_ready_s               <= '0';
      rx_cmd_error_s            <= '0';
      byte_ctr_v                := 0;
    elsif rising_edge(clk_i) then
      case rx_state_s is
        when idle =>         
          rx_read_s             <= '0';
          cmd_ready_s           <= '0';
          rx_cmd_error_s        <= '0';
          byte_ctr_v            := 0;
          -- Check if new data available
          if rx_data_rdy_s = '1' and wb_state_s = idle and rx_read_s = '0' then
            rx_read_s   <= '1';
            rx_buf_s(byte_ctr_v)<= serial_data_rx_s;
            byte_ctr_v          := byte_ctr_v + 1;

            rx_state_s <= recieve;
          end if;
        when recieve =>
          rx_read_s             <= '0';
          cmd_ready_s           <= '0';
          rx_cmd_error_s        <= '0';
          -- Check if new data available
          if rx_data_rdy_s = '1' and wb_state_s = idle and rx_read_s = '0' then
            rx_read_s           <= '1';
            rx_buf_s(byte_ctr_v)<= serial_data_rx_s;

            -- Reset requested
            if rx_buf_s(0) = x"0D" and byte_ctr_v = 1 and serial_data_rx_s = x"0A" then
              rx_cmd_error_s    <= '1';
              rx_state_s        <= idle;
            -- check for end-of-command
            elsif (wb_we_s = '0' and byte_ctr_v = 2) or byte_ctr_v = 6 then
              if serial_data_rx_s = x"0A" then
                cmd_ready_s     <= '1';
              else
                rx_cmd_error_s  <= '1';
              end if;
              rx_state_s        <= idle;
            end if;

            -- otherwise continue
            byte_ctr_v          := byte_ctr_v + 1;
          end if;
      end case;
    end if;
  end process;
	
  -- Generation of wb signals (controlled with wb_active_s)
  wb_adr_o <= '0' & rx_buf_s(0)(6 downto 0) & rx_buf_s(1);
  wb_dat_o <= (others => '0') when (wb_active_s = '0' or wb_we_s = '0') else
              rx_buf_s(2) & rx_buf_s(3) & rx_buf_s(4) & rx_buf_s(5);
  wb_we_s  <= rx_buf_s(0)(7);
  wb_we_o  <= wb_we_s and wb_active_s;
  wb_stb_o <= wb_active_s;
  wb_cyc_o <= wb_active_s;

  wb_rst_o <= rst_int_s;
		
  cmd_exec: process(rst_int_s, clk_i)
    variable rw_v               : std_logic;
    variable timeout_ctr_v      : integer;
    variable wb_timeout_v       : std_logic;
  begin
    if rst_int_s = '1' then
      tx_databytes_s            <= '0';
      wb_active_s               <= '0';
      wb_state_s                <= idle;
      timeout_ctr_v             := 0;
      wb_timeout_v              := '0';
    elsif rising_edge(clk_i) then
      case wb_state_s is
        when idle =>
          tx_databytes_s        <= '0';
          timeout_ctr_v         := 0;
          if cmd_ready_s = '1' then
            wb_active_s         <= '1';            
            wb_state_s          <= waitack;
          end if;
        when waitack =>
          -- Check for timeout
          timeout_ctr_v         := timeout_ctr_v + 1;
          if (timeout_ctr_v > c_TIMEOUT_CYCLES) then
            wb_timeout_v        := '1';
          else
            wb_timeout_v        := '0';
          end if;

          -- Wait for acknowledge by slave
          if wb_ack_i = '1' or wb_timeout_v = '1' then
            wb_active_s         <= '0';
            if wb_we_s = '0' then -- Tx the read
              tx_databytes_s    <= '1';
              tx_buf_s(5)       <= x"0A";
              tx_buf_s(4)       <= wb_dat_i( 7 downto 0);
              tx_buf_s(3)       <= wb_dat_i(15 downto 8);
              tx_buf_s(2)       <= wb_dat_i(23 downto 16);
              tx_buf_s(1)       <= wb_dat_i(31 downto 24);
              tx_buf_s(0)       <= x"0" & '1' & '0' & wb_timeout_v & wb_err_i;
            end if;
            wb_state_s          <= idle;          
          end if;
      end case;
    end if;
  end process;
	
  -- When a response is ready, immediately send next byte when previous byte is fully sent
  serial_load_s <= serial_load_requested_s when tx_send_s = '1' else '0';

  serial_write: process(rst_int_s, clk_i)
    variable byte_ctr_v : integer;
  begin
    if rst_int_s = '1' then
      tx_state_s                <= idle;
      byte_ctr_v                := 0;
      tx_send_s                 <= '0';
    elsif rising_edge(clk_i) then
      case tx_state_s is
        when idle =>
          byte_ctr_v            := 0;
          tx_send_s             <= '0';
          -- If no transmission active, check for required response
          if rx_cmd_error_s = '1' then -- respond with ACK
            tx_state_s          <= errsend0;
            tx_send_s           <= '1';
            serial_data_tx_s    <= x"00";
          elsif tx_databytes_s = '1'  then
            tx_state_s          <= send;
            tx_send_s           <= '1';
            serial_data_tx_s    <= tx_buf_s(0);
            byte_ctr_v          := 1;
          end if;
        when send =>
          if serial_load_requested_s = '1' then
            if byte_ctr_v = 6 then
              tx_send_s         <= '0';
              tx_state_s        <= idle;
            else
              tx_send_s         <= '1';
              serial_data_tx_s  <= tx_buf_s(byte_ctr_v);
              byte_ctr_v        := byte_ctr_v + 1;
              tx_state_s        <= send;
            end if;
          end if;          
        when errsend0 =>
          if serial_load_requested_s = '1' then
            tx_send_s           <= '1';
            serial_data_tx_s    <= x"0A";
            tx_state_s          <= errsend1;
          end if;
        when errsend1 =>
          if serial_load_requested_s = '1' then
            tx_send_s           <= '0';
            tx_state_s          <= idle;
          end if;
      end case;
    end if;
  end process;

end arch;
