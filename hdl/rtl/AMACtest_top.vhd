----------------------------------------------------------------------------------
-- Mojo_top VHDL
-- Translated from Mojo-base Verilog project @ http://embeddedmicro.com/frontend/files/userfiles/files/Mojo-Base.zip
-- by Xark
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity AMACtest_top is
  port (
    clk			: in  std_logic;		-- 50Mhz clock
    rst_n		: in  std_logic;		-- "reset" button input (negative logic)
    cclk		: in  std_logic;		-- configuration clock (?) from AVR (to detect when AVR ready)
    spi_sck		: in  std_logic;		-- SPI clock to from AVR
    spi_ss		: in  std_logic;		-- SPI slave select from AVR
    spi_mosi	        : in  std_logic;		-- SPI serial data master out, slave in (AVR -> FPGA)
    spi_miso	        : out std_logic;		-- SPI serial data master in, slave out (AVR <- FPGA)
    spi_channel         : out std_logic_vector(3 downto 0);  -- analog read channel (input to AVR service task)
    avr_tx		: in  std_logic;		-- serial data transmited from AVR/USB (FPGA recieve)
    avr_rx		: out std_logic;		-- serial data for AVR/USB to receive (FPGA transmit)

    ftdi_tx		: in  std_logic;		-- serial data transmited from AVR/USB (FPGA recieve)
    ftdi_rx		: out std_logic;		-- serial data for AVR/USB to receive (FPGA transmit)
    
    amac_scl 	        : inout std_logic;
    amac_sda		: inout std_logic;
    spi_dac_sck         : out std_logic_vector(2 downto 0);
    spi_dac_ss          : out std_logic_vector(2 downto 0);
    spi_dac_mosi        : out std_logic_vector(2 downto 0);
    amac_clk_o          : out std_logic;
    amac_clk_i          : in  std_logic;
    amac_resetb_o       : out std_logic;
    amac_id_o           : out std_logic_vector(3 downto 0);
    amac_mux_pos_o      : out std_logic_vector(1 downto 0);
    amac_mux_en_o       : out std_logic;
    en_1v5_o            : out std_logic;
    en_1v2_o            : out std_logic;
    en_IO_o             : out std_logic;
    amac_hvcntrl_i      : in  std_logic;
    amac_hvcntrlb_i     : in  std_logic;
    amac_lvcntrl_i      : in  std_logic;
    amac_lvcntrlb_i     : in  std_logic;
    amac_ilocklam_i     : in  std_logic;
    amac_warnlam_i      : in  std_logic
    );
end AMACtest_top;

architecture RTL of AMACtest_top is

  constant isSimulation : std_logic := '0';

  component clk_wiz_v3_6 is
    port
      (-- Clock in ports
        CLK_IN1           : in     std_logic;
        -- Clock out ports
        CLK_OUT1          : out    std_logic;
        -- Status and control signals
        RESET             : in     std_logic;
        LOCKED            : out    std_logic
        );
  end component;
	
  COMPONENT rs232_syscon
    PORT(
      clk_i : IN std_logic;
      rst_p_i : IN std_logic;
      ser_rxd_i : IN std_logic;   
      ser_txd_o : OUT std_logic; 
      wb_adr_o : OUT std_logic_vector(15 downto 0);
      wb_dat_i : IN std_logic_vector(31 downto 0); 
      wb_dat_o : OUT std_logic_vector(31 downto 0); 
      wb_we_o : OUT std_logic;  
      wb_stb_o : OUT std_logic;
      wb_cyc_o : OUT std_logic;   
      wb_rst_o : OUT std_logic;
      wb_ack_i : IN std_logic;
      wb_err_i : IN std_logic
      );
  END COMPONENT;
	
  component i2c_master_wb is
    generic (
      ARST_LVL : integer := 0
      );
    port (
      wb_clk_i : in  std_logic;
      wb_rst_i : in  std_logic;
      arst_i   : in  std_logic;
      wb_adr_i : in  std_logic_vector(2 downto 0);
      wb_dat_i : in  std_logic_vector(7 downto 0);
      wb_dat_o : out std_logic_vector(7 downto 0);
      wb_we_i  : in  std_logic;
      wb_stb_i : in  std_logic;
      wb_cyc_i : in  std_logic;
      wb_ack_o : out std_logic;
      wb_inta_o: out std_logic;
      scl      : inout std_logic;
      sda      : inout std_logic;
      scl_active_i : in std_logic
      );
  end component;
	
  component AD_regs is
    port (
      -- General signals
      clk_i				: in  std_logic;
      rst_i				: in  std_logic;
      cclk_i			: in  std_logic;				-- Indicates that uC is ready
      -- SPI interface
      spi_miso_o		: out std_logic;
      spi_mosi_i		: in  std_logic;
      spi_sck_i			: in  std_logic;
      spi_ss_i			: in  std_logic;
      spi_channel_o		: out std_logic_vector(3 downto 0);
      -- Registers
      ADC_channel0_o	: out std_logic_vector(9 downto 0);
      ADC_channel1_o	: out std_logic_vector(9 downto 0);
      ADC_channel4_o	: out std_logic_vector(9 downto 0);
      ADC_channel5_o	: out std_logic_vector(9 downto 0);
      ADC_channel6_o	: out std_logic_vector(9 downto 0);
      ADC_channel7_o	: out std_logic_vector(9 downto 0);
      ADC_channel8_o	: out std_logic_vector(9 downto 0);
      ADC_channel9_o	: out std_logic_vector(9 downto 0)
      );
  end component;

  component wb_regs is
    port (
      rst_n_i                                  : in     std_logic;
      clk_sys_i                                : in     std_logic;
      wb_adr_i                                 : in     std_logic_vector(4 downto 0);
      wb_dat_i                                 : in     std_logic_vector(31 downto 0);
      wb_dat_o                                 : out    std_logic_vector(31 downto 0);
      wb_cyc_i                                 : in     std_logic;
      wb_sel_i                                 : in     std_logic_vector(3 downto 0);
      wb_stb_i                                 : in     std_logic;
      wb_we_i                                  : in     std_logic;
      wb_ack_o                                 : out    std_logic;
      wb_stall_o                               : out    std_logic;
      -- Port for std_logic_vector field: 'ADC 0 val' in reg: 'Analog channel 0 '
      amactest_mojo_adc0_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 1 val' in reg: 'Analog channel 1 '
      amactest_mojo_adc1_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 4 val' in reg: 'Analog channel 4 '
      amactest_mojo_adc4_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 5 val' in reg: 'Analog channel 5 '
      amactest_mojo_adc5_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 6 val' in reg: 'Analog channel 6 '
      amactest_mojo_adc6_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 7 val' in reg: 'Analog channel 7 '
      amactest_mojo_adc7_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 8 val' in reg: 'Analog channel 8 '
      amactest_mojo_adc8_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ADC 9 val' in reg: 'Analog channel 9 '
      amactest_mojo_adc9_i                     : in     std_logic_vector(9 downto 0);
      -- Port for std_logic_vector field: 'ID' in reg: 'AMAC inputs'
      amactest_mojo_amac_in_id_o               : out    std_logic_vector(3 downto 0);
      -- Port for BIT field: 'RESETB' in reg: 'AMAC inputs'
      amactest_mojo_amac_in_resetb_o           : out    std_logic;
      -- Port for BIT field: 'HVCNTRL' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_hvcntrl_i         : in     std_logic;
      -- Port for BIT field: 'HVCNTRLB' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_hvcntrlb_i        : in     std_logic;
      -- Port for BIT field: 'ILOCKLAM' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_ilocklam_i        : in     std_logic;
      -- Port for BIT field: 'WARNLAM' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_warnlam_i         : in     std_logic;
      -- Port for BIT field: 'LVCNTRLB' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_lvcntrlb_i        : in     std_logic;
      -- Port for BIT field: 'LVCNTRL' in reg: 'AMAC outputs'
      amactest_mojo_amac_out_lvcntrl_i         : in     std_logic;
      -- Port for std_logic_vector field: 'ILockLAM' in reg: 'LAM pulse counter'
      amactest_mojo_pulse_ctr_ilocklam_i       : in     std_logic_vector(15 downto 0);
      -- Port for std_logic_vector field: 'WarnLAM' in reg: 'LAM pulse counter'
      amactest_mojo_pulse_ctr_warnlam_i        : in     std_logic_vector(15 downto 0);
      -- Port for std_logic_vector field: 'Position' in reg: 'Current Mux'
      amactest_mojo_mux_pos_o                  : out    std_logic_vector(1 downto 0);
      -- Port for BIT field: 'Enabled' in reg: 'Current Mux'
      amactest_mojo_mux_en_o                   : out    std_logic;
      -- Port for BIT field: '1.5V' in reg: 'Supply Voltage Enable'
      amactest_mojo_sup_en_1v5_o               : out    std_logic;
      -- Port for BIT field: '1.2V' in reg: 'Supply Voltage Enable'
      amactest_mojo_sup_en_1v2_o               : out    std_logic;
      -- Port for BIT field: 'IO' in reg: 'Supply Voltage Enable'
      amactest_mojo_sup_en_io_o                : out    std_logic;
      -- Port for std_logic_vector field: 'Divider' in reg: 'Clock Generator'
      amactest_mojo_clkgen_o                   : out    std_logic_vector(15 downto 0);
      -- Port for std_logic_vector field: 'high time' in reg: 'AMAC Clock Analysis - High/Low times'
      amactest_mojo_clkana_hilo_hi_i           : in     std_logic_vector(15 downto 0);
      -- Port for std_logic_vector field: 'low time' in reg: 'AMAC Clock Analysis - High/Low times'
      amactest_mojo_clkana_hilo_lo_i           : in     std_logic_vector(15 downto 0);
      -- Port for std_logic_vector field: 'Number of rising edges at clock input' in reg: 'AMAC Clock Analysis - Transition Counter'
      amactest_mojo_clkana_ctr_val_i           : in     std_logic_vector(31 downto 0);
      -- Ports for PASS_THROUGH field: 'Timespan for transition counting' in reg: 'AMAC Clock Analysis - control register'
      amactest_mojo_clkana_ctrl_ctr_time_o     : out    std_logic_vector(15 downto 0);
      amactest_mojo_clkana_ctrl_ctr_time_wr_o  : out    std_logic;
      -- Port for BIT field: 'Counter busy' in reg: 'AMAC Clock Analysis - control register'
      amactest_mojo_clkana_ctrl_busy_i         : in     std_logic;
      -- Port for std_logic_vector field: 'Clock Analysis Input select' in reg: 'AMAC Clock Analysis - control register'
      amactest_mojo_clkana_ctrl_inp_o          : out    std_logic_vector(1 downto 0);
      -- Port for std_logic_vector field: 'The DAC channel that gets written /read' in reg: 'DAC channel'
      amactest_mojo_dac_ch_o                   : out    std_logic_vector(3 downto 0);
      -- Port for std_logic_vector field: 'The value of the DAC channel specified in reg 'DAC channel'' in reg: 'DAC value'
      amactest_mojo_dac_val_o                  : out    std_logic_vector(11 downto 0);
      amactest_mojo_dac_val_i                  : in     std_logic_vector(11 downto 0);
      amactest_mojo_dac_val_load_o             : out    std_logic;
      -- Port for BIT field: 'I2C core reset' in reg: 'Logic control'
      amactest_mojo_log_ctrl_i2c_rst_o         : out    std_logic;
      -- Port for BIT field: 'I2C SCL active' in reg: 'Logic control'
      amactest_mojo_log_ctrl_scl_act_o         : out    std_logic
      );
  end component;
	
  component DAC_spi is
    Generic (
      SPI_CLK_PRES_g : integer := 50 -- Has to be a multiple of 2
      );
    Port ( 
      clk_i : in std_logic;
      rst_i : in std_logic;
      channel_i : in  STD_LOGIC_VECTOR (3 downto 0);
      val_i : in  STD_LOGIC_VECTOR (11 downto 0);
      val_o : out  STD_LOGIC_VECTOR (11 downto 0);
      write_i : in  STD_LOGIC;
      spi_ss_o : out  STD_LOGIC_VECTOR (2 downto 0);
      spi_sclk_o : out  STD_LOGIC_VECTOR (2 downto 0);
      spi_mosi_o : out  STD_LOGIC_VECTOR (2 downto 0)
      );
  end component;
	
  component clk_gen is
    Port ( 
      clk_i : in std_logic;
      rst_i : in std_logic;
      divider_i : in std_logic_vector(15 downto 0);
      clk_o : out std_logic
      );
  end component;
	
  component clk_ana is
    Generic (
      TIMESPAN_MULT_g : integer := 10_000 --Number of cycles that equal to 1 timespan
      );
    Port ( 
      clk_i : in std_logic;
      rst_i : in std_logic;
      clk_ana_i : in std_logic;
      high_time_o : out std_logic_vector(15 downto 0);
      low_time_o : out std_logic_vector(15 downto 0);
      trans_ctr_o : out std_logic_vector(31 downto 0);
      timespan_i : in std_logic_vector(15 downto 0);
      start_i : in std_logic;
      busy_o : out std_logic
      );
  end component;

  -- General purpose signals
  signal rst, rst_s, rst_n_s	: std_logic;		-- reset signal (rst_n inverted for postive Logicc)
  signal clk_s				: std_logic;		-- Clk output of PLL

  -- Wishbone signals
  signal wb_ack, wb_i2c_ack, wb_reg_ack : std_logic;
  signal wb_we, wb_i2c_we, wb_reg_we : std_logic;
  signal wb_stb, wb_i2c_stb, wb_reg_stb : std_logic;
  signal wb_adr : std_logic_vector(15 downto 0);
  signal wb_dat_i, wb_i2c_dat_i, wb_reg_dat_i : std_logic_vector(31 downto 0);
  signal wb_dat_o : std_logic_vector(31 downto 0);

  -- Registers
  signal ADC_channel0_s, ADC_channel1_s, ADC_channel4_s, ADC_channel5_s,
    ADC_channel6_s, ADC_channel7_s, ADC_channel8_s, ADC_channel9_s: std_logic_vector(9 downto 0);
  signal amac_id_s : std_logic_vector(3 downto 0);
  signal amac_resetb_s : std_logic;
  signal amac_hvcntrl_s, amac_hvcntrlb_s, amac_lvcntrl_s, amac_lvcntrlb_s,
    amac_ilocklam_s, amac_warnlam_s  : std_logic;
  signal mux_pos_s : std_logic_vector(1 downto 0);
  signal mux_en_s : std_logic;
  signal en_1v2_s, en_1v5_s, en_IO_s : std_logic;
  signal clkgen_divider_s : std_logic_vector(15 downto 0);
  signal clkana_hi_s, clkana_lo_s : std_logic_vector(15 downto 0);
  signal clkana_ctr_s : std_logic_vector(31 downto 0);
  signal clkana_time_s : std_logic_vector(15 downto 0);
  signal clkana_time_wr_s : std_logic;
  signal clkana_busy_s : std_logic;
  signal clkana_input_s : std_logic_vector(1 downto 0);
  signal clkana_clkin_s : std_logic;
  signal dac_channel_s : std_logic_vector(3 downto 0);
  signal dac_val_rd_s, dac_val_wr_s : std_logic_vector(11 downto 0);
  signal dac_write_s : std_logic;
	
  signal rst_i2c_s : std_logic;
  signal i2c_scl_act_s : std_logic;

  signal ilocklam_pulse_ctr_s : std_logic_vector(15 downto 0);
  signal warnlam_pulse_ctr_s : std_logic_vector(15 downto 0);
begin
	
  -- ##################
  -- General signals
  -- ##################
	
  rst <= not rst_n;

  Clock_wiz: if isSimulation = '0' generate
    Inst_clk_wiz : clk_wiz_v3_6 port map(
      CLK_IN1 => clk,
      CLK_OUT1 => clk_s,
      RESET => rst,
      LOCKED => rst_n_s
      );
  end generate;
  No_clock_wiz: if isSimulation = '1' generate
    clk_s <= clk;
    rst_n_s <= not rst;	
  end generate;
	
  rst_s	<= NOT rst_n_s;
	
  -- ##################
  -- RS232 - WB
  -- ##################
  ftdi_rx        <= '1';
  Inst_rs232_syscon: rs232_syscon PORT MAP(
    -- General signals
    clk_i => clk_s,
    rst_p_i => rst_s,
    -- Serial interface
    ser_rxd_i => avr_tx,	
    ser_txd_o => avr_rx,
    -- Wishbone Master interface
    wb_ack_i => wb_ack,
    wb_err_i => '0',
    wb_dat_i => wb_dat_i,
    wb_dat_o => wb_dat_o,
    wb_rst_o => open,
    wb_stb_o => wb_stb,
    wb_cyc_o => open,
    wb_adr_o => wb_adr,
    wb_we_o => wb_we
    );
	
  -- ##################
  -- Wishbone crossbar
  -- ##################
  -- Address range 	0...255 		-> regs
  --						256...511 	-> i2c
	
  wb_dat_i      <= wb_i2c_dat_i when wb_adr(8) = '1' else
                   wb_reg_dat_i;
  wb_ack        <= wb_i2c_ack when wb_adr(8) = '1' else
                   wb_reg_ack;
  wb_i2c_we     <= wb_we  when wb_adr(8) = '1' else '0';
  wb_reg_we     <= wb_we  when wb_adr(8) = '0' else '0';
  wb_i2c_stb    <= wb_stb when wb_adr(8) = '1' else '0';
  wb_reg_stb    <= wb_stb when wb_adr(8) = '0' else '0';
	
  wb_i2c_dat_i(31 downto 8) <= (others => '0');
	
  -- ##################
  -- I2C Master
  -- ##################
	
  i2c_master_sb_inst : i2c_master_wb 
    port map(
      -- General signals
      wb_clk_i  => clk_s,
      wb_rst_i  => rst_s,
      arst_i    => rst_n_s and rst_i2c_s,
      -- Wishbone interface
      wb_adr_i  => wb_adr(2 downto 0),
      wb_dat_i  => wb_dat_o(7 downto 0),
      wb_dat_o  => wb_i2c_dat_i(7 downto 0),
      wb_we_i   => wb_i2c_we,
      wb_stb_i  => wb_i2c_stb,
      wb_cyc_i  => wb_i2c_stb,
      wb_ack_o  => wb_i2c_ack,
      wb_inta_o => open,
      -- I2C interface
      scl       => amac_scl,
      sda       => amac_sda,
      scl_active_i => i2c_scl_act_s

      );
	
  -- ##################
  -- Wishbone registers (generated core)
  -- ##################
  Inst_wb_regs : wb_regs
    port map(
      rst_n_i => rst_n_s,
      clk_sys_i => clk_s,
      wb_adr_i => wb_adr(6 downto 2),
      wb_dat_i => wb_dat_o,
      wb_dat_o => wb_reg_dat_i,
      wb_cyc_i => wb_reg_stb,
      wb_sel_i => "1111", -- Always writing all bytes
      wb_stb_i => wb_reg_stb,
      wb_we_i => wb_reg_we,
      wb_ack_o=> wb_reg_ack,
      wb_stall_o => open,
      amactest_mojo_adc0_i => ADC_channel0_s,
      amactest_mojo_adc1_i => ADC_channel1_s,
      amactest_mojo_adc4_i => ADC_channel4_s,
      amactest_mojo_adc5_i => ADC_channel5_s,
      amactest_mojo_adc6_i => ADC_channel6_s,
      amactest_mojo_adc7_i => ADC_channel7_s,
      amactest_mojo_adc8_i => ADC_channel8_s,
      amactest_mojo_adc9_i => ADC_channel9_s,
      amactest_mojo_amac_in_id_o => amac_id_s,
      amactest_mojo_amac_in_resetb_o => amac_resetb_s,
      amactest_mojo_amac_out_hvcntrl_i => amac_hvcntrl_s,
      amactest_mojo_amac_out_hvcntrlb_i => amac_hvcntrlb_s,
      amactest_mojo_amac_out_ilocklam_i  => amac_ilocklam_s,
      amactest_mojo_amac_out_warnlam_i => amac_warnlam_s,
      amactest_mojo_amac_out_lvcntrlb_i => amac_lvcntrlb_s,
      amactest_mojo_amac_out_lvcntrl_i => amac_lvcntrl_s,
      amactest_mojo_pulse_ctr_ilocklam_i => ilocklam_pulse_ctr_s,
      amactest_mojo_pulse_ctr_warnlam_i => warnlam_pulse_ctr_s,
      amactest_mojo_mux_pos_o => mux_pos_s,
      amactest_mojo_mux_en_o => mux_en_s,
      amactest_mojo_sup_en_1v5_o => en_1v5_s,
      amactest_mojo_sup_en_1v2_o => en_1v2_s,
      amactest_mojo_sup_en_IO_o => en_IO_s,
      amactest_mojo_clkgen_o => clkgen_divider_s,
      amactest_mojo_clkana_hilo_hi_i => clkana_hi_s,
      amactest_mojo_clkana_hilo_lo_i => clkana_lo_s,
      amactest_mojo_clkana_ctr_val_i => clkana_ctr_s,
      amactest_mojo_clkana_ctrl_ctr_time_o => clkana_time_s,
      amactest_mojo_clkana_ctrl_ctr_time_wr_o => clkana_time_wr_s,
      amactest_mojo_clkana_ctrl_busy_i => clkana_busy_s,
      amactest_mojo_clkana_ctrl_inp_o => clkana_input_s,
      amactest_mojo_dac_ch_o => dac_channel_s,
      amactest_mojo_dac_val_o => dac_val_wr_s,
      amactest_mojo_dac_val_i => dac_val_rd_s,
      amactest_mojo_dac_val_load_o => dac_write_s,
      amactest_mojo_log_ctrl_i2c_rst_o  => rst_i2c_s,
      amactest_mojo_log_ctrl_scl_act_o => i2c_scl_act_s
      );

  -- ##################
  -- Peripherals
  -- ##################
	
  Inst_DAC_spi : DAC_spi
    generic map(
      SPI_CLK_PRES_g => 100
      )
    port map(
      clk_i => clk_s,
      rst_i => rst_s,
      channel_i => dac_channel_s,
      val_i => dac_val_wr_s,
      val_o => dac_val_rd_s,
      write_i => dac_write_s,
      spi_ss_o => spi_dac_ss,
      spi_sclk_o => spi_dac_sck,
      spi_mosi_o => spi_dac_mosi
      );

  Inst_clk_gen : clk_gen
    port map(
      clk_i => clk_s,
      rst_i => rst_s,
      divider_i => clkgen_divider_s,
      clk_o => amac_clk_o
      );

  Inst_AD_regs : AD_regs
    port map(
      -- General signals
      clk_i => clk_s,
      rst_i => rst_s,
      cclk_i => cclk,
      -- SPI interface
      spi_miso_o => spi_miso,
      spi_mosi_i => spi_mosi,
      spi_sck_i => spi_sck,
      spi_ss_i => spi_ss,
      spi_channel_o => spi_channel,
      -- Registers
      ADC_channel0_o	=> ADC_channel0_s,
      ADC_channel1_o	=> ADC_channel1_s,
      ADC_channel4_o	=> ADC_channel4_s,
      ADC_channel5_o	=> ADC_channel5_s,
      ADC_channel6_o	=> ADC_channel6_s,
      ADC_channel7_o	=> ADC_channel7_s,
      ADC_channel8_o	=> ADC_channel8_s,
      ADC_channel9_o	=> ADC_channel9_s
      );
	
  with clkana_input_s select
    clkana_clkin_s <= 	amac_clk_i when "00",
    amac_hvcntrl_i when "01",
    amac_hvcntrlb_i when "10",
    '0' when others;
	
  Inst_clk_ana : clk_ana
    generic map(
      TIMESPAN_MULT_g => 10_000
      )
    port map(
      clk_i => clk_s,
      rst_i => rst_s,
      clk_ana_i => clkana_clkin_s,
      high_time_o => clkana_hi_s,
      low_time_o => clkana_lo_s,
      trans_ctr_o => clkana_ctr_s,
      timespan_i => clkana_time_s,
      start_i => clkana_time_wr_s,
      busy_o => clkana_busy_s
      );
		
  -- To increase stability, all the direct I/Os are synchronized with the clock
  IOsync: process(rst_s, clk_s)
  begin
    if rst_s = '1' then
      amac_resetb_o <= '0';
      amac_id_o <= (others => '0');
      amac_mux_pos_o <= (others => '0');
      amac_mux_en_o <= '1';
      amac_hvcntrl_s <= '0';
      amac_hvcntrlb_s <= '0';
      amac_lvcntrl_s <= '0';
      amac_lvcntrlb_s <= '0';
      amac_ilocklam_s <= '0';
      amac_warnlam_s <= '0';
      en_1v5_o <= '0';
      en_1v2_o <= '0';
      en_IO_o <= '0';
    elsif rising_edge(clk_s) then
      amac_resetb_o <= amac_resetb_s;
      amac_id_o <= amac_id_s;
      amac_mux_pos_o <= mux_pos_s;
      amac_mux_en_o <= not mux_en_s;
      amac_hvcntrl_s <= amac_hvcntrl_i;
      amac_hvcntrlb_s <= amac_hvcntrlb_i;
      amac_lvcntrl_s <= amac_lvcntrl_i;
      amac_lvcntrlb_s <= amac_lvcntrlb_i;
      amac_ilocklam_s <= amac_ilocklam_i;
      amac_warnlam_s <= amac_warnlam_i;
      en_1v5_o <= en_1v5_s;
      en_1v2_o <= en_1v2_s;
      en_IO_o <= en_IO_s;
    end if;
  end process;
	
  -- ON Inputs ILockLAM and WarnLAM, there are only pulses available.
  -- These pulses get counted in order to read them out to know how many pulses appeared
  Pulse_Ctr: process(rst_s, clk_s)
    variable ilocklam_ctr_v : integer;
    variable warnlam_ctr_v : integer;
    variable ilocklam_old_v : std_logic;
    variable warnlam_old_v : std_logic;
  begin
    if rst_s = '1' then
      ilocklam_pulse_ctr_s <= (others => '0');
      warnlam_pulse_ctr_s <= (others => '0');
      ilocklam_ctr_v := 0;
      warnlam_ctr_v := 0;
      ilocklam_old_v := '0';
      warnlam_old_v := '0';
    elsif rising_edge(clk_s) then
      if amac_ilocklam_i = '0' and ilocklam_old_v = '1' then
        ilocklam_ctr_v := ilocklam_ctr_v + 1;
        ilocklam_pulse_ctr_s <= std_logic_vector(to_unsigned(ilocklam_ctr_v, 16));
      end if;
      ilocklam_old_v := amac_ilocklam_i;
      if amac_warnlam_i = '0' and warnlam_old_v = '1' then
        warnlam_ctr_v := warnlam_ctr_v + 1;
        warnlam_pulse_ctr_s <= std_logic_vector(to_unsigned(warnlam_ctr_v, 16));
      end if;
      warnlam_old_v := amac_warnlam_i;
    end if;
  end process;
end RTL;
