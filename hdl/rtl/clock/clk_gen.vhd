----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:04:06 06/09/2016 
-- Design Name: 
-- Module Name:    clk_gen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_gen is
	Port ( 
		clk_i : in std_logic;
		rst_i : in std_logic;
		divider_i : in std_logic_vector(15 downto 0);
		clk_o : out std_logic
		);
end clk_gen;

architecture Behavioral of clk_gen is
	signal clk_s : std_logic;
begin

	clk_o <= clk_s;

	Clk_gen : process(rst_i, clk_i)
		variable ctr_v : integer;
	begin
		if rst_i = '1' then
			clk_s <= '0';
			ctr_v := 0;
		elsif rising_edge(clk_i) then
			-- Increment counter by 2, so a divider value of 2 actually causes
			-- a division by 2 (Output inversion only happens on rising edge of input clock)
			ctr_v := ctr_v + 2;
			if ctr_v >= to_integer(unsigned(divider_i)) then
				ctr_v := 0;
				clk_s <= not clk_s;
			end if;
		end if;
	end process;

end Behavioral;

