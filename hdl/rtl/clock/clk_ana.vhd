----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:14:02 06/09/2016 
-- Design Name: 
-- Module Name:    clk_ana - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_ana is
	Generic (
		TIMESPAN_MULT_g : integer := 10_000 --Number of cycles that equal to 1 timespan
		);
	Port ( 
		clk_i : in std_logic;
		rst_i : in std_logic;
		clk_ana_i : in std_logic;
		high_time_o : out std_logic_vector(15 downto 0);
		low_time_o : out std_logic_vector(15 downto 0);
		trans_ctr_o : out std_logic_vector(31 downto 0);
		timespan_i : in std_logic_vector(15 downto 0);
		start_i : in std_logic;
		busy_o : out std_logic
		);
end clk_ana;

architecture Behavioral of clk_ana is
	signal busy_s : std_logic;
	signal clk_ana_buf_s : std_logic;
begin

	-- One-stage input buffer to avoid metastabilities
	inbuf : process(rst_i, clk_i)
	begin
		if rst_i = '1' then
			clk_ana_buf_s <= '0';
		elsif rising_edge(clk_i) then
			clk_ana_buf_s <= clk_ana_i;
		end if;
	end process;

	-- High/Low counter
	hilo_ctr : process(rst_i, clk_i)
		variable ctr_v : integer;
		variable clk_ana_old_v : std_logic;
		variable ctr_overflow_v : std_logic;
	begin
		if rst_i = '1' then
			low_time_o <= (others => '0');
			high_time_o <= (others => '0');
			ctr_v := 0;
			clk_ana_old_v := '0';
		elsif rising_edge(clk_i) then
			ctr_overflow_v := '0';
			ctr_v := ctr_v + 1;
			-- high/low time values are 16-bit
			-- If the high/low time exceeds maximum value, suppose a constant voltage level
			-- In this case, one counter = 0, other counter = max
			if ctr_v >= 65535 then
				low_time_o <= (others => '0');
				high_time_o <= (others => '0');
				ctr_overflow_v := '1';
			end if;
			-- If polarity changes OR counter full, update output
			if (clk_ana_old_v /= clk_ana_buf_s) or (ctr_overflow_v = '1') then
				if clk_ana_old_v = '0' then
					low_time_o <= std_logic_vector(to_unsigned(ctr_v, 16));
				else
					high_time_o <= std_logic_vector(to_unsigned(ctr_v, 16));
				end if;
				ctr_v := 0;
			end if;
			clk_ana_old_v := clk_ana_buf_s;
		end if;
	end process;
	
	-- Transition counter
	busy_o <= busy_s;
	trans_ctr : process(rst_i, clk_i)
		variable time_ctr_v : integer;
		variable trans_ctr_v : integer;
		variable clk_ana_old_v : std_logic;
	begin
		if rst_i = '1' then
			busy_s <= '0';
			trans_ctr_o <= (others => '0');
			clk_ana_old_v := '0';
		elsif rising_edge(clk_i) then
			-- If no measurement running
			if busy_s = '0' then
				time_ctr_v := TIMESPAN_MULT_g * to_integer(unsigned(timespan_i));
				-- impulse on start_i starts measurement
				-- When 0 is written, only the input channel was changed
				if start_i = '1' and not (time_ctr_v = 0) then
					busy_s <= '1';
					trans_ctr_v := 0;
				end if;
			else
				-- Detect rising_edge
				if clk_ana_old_v = '0' and clk_ana_buf_s = '1' then
					trans_ctr_v := trans_ctr_v + 1;
				end if;
				-- Evaluate remaining time
				time_ctr_v := time_ctr_v - 1;
				if time_ctr_v = 0 then
					-- End of measurement
					busy_s <= '0';
					trans_ctr_o <= std_logic_vector(to_unsigned(trans_ctr_v, 32));
				end if;
			end if;
			clk_ana_old_v := clk_ana_buf_s;
		end if;
	end process;

end Behavioral;

