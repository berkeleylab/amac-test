----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:20:49 06/08/2016 
-- Design Name: 
-- Module Name:    AD_regs - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AD_regs is
  port (
    -- General signals
    clk_i				: in  std_logic;
    rst_i				: in  std_logic;
    cclk_i			: in  std_logic;				-- Indicates that uC is ready
    -- SPI interface
    spi_miso_o		: out std_logic;
    spi_mosi_i		: in  std_logic;
    spi_sck_i			: in  std_logic;
    spi_ss_i			: in  std_logic;
    spi_channel_o		: out std_logic_vector(3 downto 0);
    -- Registers
    ADC_channel0_o	: out std_logic_vector(9 downto 0);
    ADC_channel1_o	: out std_logic_vector(9 downto 0);
    ADC_channel4_o	: out std_logic_vector(9 downto 0);
    ADC_channel5_o	: out std_logic_vector(9 downto 0);
    ADC_channel6_o	: out std_logic_vector(9 downto 0);
    ADC_channel7_o	: out std_logic_vector(9 downto 0);
    ADC_channel8_o	: out std_logic_vector(9 downto 0);
    ADC_channel9_o	: out std_logic_vector(9 downto 0)
    );
end AD_regs;

architecture Behavioral of AD_regs is
  component cclk_detector is
    port (
      clk		: in  std_logic;	-- 50Mhz clock 
      rst		: in  std_logic;	-- reset signal
      cclk	: in  std_logic;	-- configuration clock (?) from AVR
      ready	: out std_logic		-- output signal that FPGA I/O is safe to use
      );
  end component cclk_detector;
  
  component spi_slave is
    port (
      clk		: in  std_logic;	-- 50Mhz clock 
      rst		: in  std_logic;	-- reset signal
      ss		: in  std_logic;	-- SPI slave select
      mosi	: in  std_logic;	-- SPI master out, slave in
      miso	: out std_logic;	-- SPI master in, slave out
      miso_en	: out std_logic;	-- true when slave selected (else miso should be 'Z')
      sck		: in  std_logic;	-- SPI clock
      done	: out std_logic;	-- done output signal
      din		: in  std_logic_vector(7 downto 0);	-- input data byte to send
      dout	: out std_logic_vector(7 downto 0)	-- output data byte received
      );
  end component spi_slave;

  signal ready, n_rdy : std_logic;

  signal spi_miso_s, spi_miso_en_s, spi_done_s : std_logic;
  signal spi_dout_s : std_logic_vector(7 downto 0);

  signal byte_ct_s : std_logic;
--signal sample_s : std_logic_vector(9 downto 0);
--signal sample_channel_s : std_logic_vector(3 downto 0);

begin

  n_rdy <= NOT ready;

  Inst_cclk_detector : cclk_detector
    port map (
      clk		=> clk_i,
      rst		=> rst_i,
      cclk	=> cclk_i,
      ready	=> ready
      );

  Inst_spi_slave	: spi_slave
    port map (
      clk		=> clk_i,
      rst		=> n_rdy,
      ss		=> spi_ss_i,
      mosi	=> spi_mosi_i,
      miso	=> spi_miso_s,
      miso_en	=> spi_miso_en_s,
      sck		=> spi_sck_i,
      done	=> spi_done_s,
      din		=> "11111111",
      dout	=> spi_dout_s
      );
	
  spi_miso_o <= spi_miso_s when ready = '1' and spi_miso_en_s = '1' else 'Z';
  spi_logic : process(clk_i, rst_i)
    variable sample_s : std_logic_vector(9 downto 0);
    variable sample_channel_s : std_logic_vector(3 downto 0);
  begin
    if rst_i = '1' then
      spi_channel_o <= x"0";
      byte_ct_s <= '0';
      sample_s := (others => '0');
      sample_channel_s := (others => '0');
      ADC_channel0_o <= (others => '0');
      ADC_channel1_o <= (others => '0');
      ADC_channel4_o <= (others => '0');
      ADC_channel5_o <= (others => '0');
      ADC_channel6_o <= (others => '0');
      ADC_channel7_o <= (others => '0');
      ADC_channel8_o <= (others => '0');
      ADC_channel9_o <= (others => '0');
    elsif rising_edge(clk_i) then
      if spi_ss_i = '1' then
        byte_ct_s <= '0';
      end if;
      if spi_done_s = '1' then
        if byte_ct_s = '0' then
          sample_s(7 downto 0)	:= spi_dout_s;
          byte_ct_s				<= '1';
        else
          sample_s(9 downto 8)	:= spi_dout_s(1 downto 0);
          sample_channel_s			:= spi_dout_s(7 downto 4);
          byte_ct_s					<= '0';
          case sample_channel_s is
            when x"0" =>
              ADC_channel0_o <= sample_s;
              spi_channel_o <= x"1";
            when x"1" =>
              ADC_channel1_o <= sample_s;
              spi_channel_o <= x"4";
            when x"4" =>
              ADC_channel4_o <= sample_s;
              spi_channel_o <= x"5";
            when x"5" =>
              ADC_channel5_o <= sample_s;
              spi_channel_o <= x"6";
            when x"6" =>
              ADC_channel6_o <= sample_s;
              spi_channel_o <= x"7";
            when x"7" =>
              ADC_channel7_o <= sample_s;
              spi_channel_o <= x"8";
            when x"8" =>
              ADC_channel8_o <= sample_s;
              spi_channel_o <= x"9";
            when x"9" =>
              ADC_channel9_o <= sample_s;
              spi_channel_o <= x"0";
            when others =>
              spi_channel_o <= x"0";
          end case;
        end if;
      end if;
    end if;
  end process;

end Behavioral;

