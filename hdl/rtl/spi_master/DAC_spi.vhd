----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:34:32 06/09/2016 
-- Design Name: 
-- Module Name:    DAC_spi - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DAC_spi is
  Generic (
    SPI_CLK_PRES_g : integer := 100 -- Has to be a multiple of 2
    );
  Port ( 
    clk_i : in std_logic;
    rst_i : in std_logic;
    channel_i : in  STD_LOGIC_VECTOR (3 downto 0);
    val_i : in  STD_LOGIC_VECTOR (11 downto 0);
    val_o : out  STD_LOGIC_VECTOR (11 downto 0);
    write_i : in  STD_LOGIC;
    spi_ss_o : out  STD_LOGIC_VECTOR (2 downto 0);
    spi_sclk_o : out  STD_LOGIC_VECTOR (2 downto 0);
    spi_mosi_o : out  STD_LOGIC_VECTOR (2 downto 0)
    );
end DAC_spi;

architecture Behavioral of DAC_spi is

  component SPI_master is
    Generic (
      SPI_CLK_PRES_g : integer := 100 -- Has to be a multiple of 2
      );
    Port ( 
      clk_i : in  STD_LOGIC;
      rst_i : in  STD_LOGIC;
      channel_i : in  STD_LOGIC_VECTOR (1 downto 0);
      val_i : in  STD_LOGIC_VECTOR (11 downto 0);
      write_i : in  STD_LOGIC;
      busy_o : out  STD_LOGIC;
      spi_dout_o : out  STD_LOGIC;
      spi_sclk_o : out  STD_LOGIC;
      spi_ss_o : out  STD_LOGIC
      );
  end component;

  type reg_t is array(0 to 11) of std_logic_vector(11 downto 0);
  signal reg_s : reg_t;
  signal channel_s : integer range 0 to 3;
  signal dac_s : integer range 0 to 2;
	
  signal SPI_clk_s : std_logic;
  signal SPI_busy_s : std_logic_vector(2 downto 0);
  signal SPI_write_s : std_logic_vector(2 downto 0);

begin

  channel_s <= to_integer(unsigned(channel_i(1 downto 0)));
  dac_s <= to_integer(unsigned(channel_i(3 downto 2)));

  -- Register Handling
  val_o <= reg_s(4*dac_s + channel_s);
  write_reg: process(clk_i, rst_i)
  begin
    if rst_i = '1' then
      reg_s <= (others => (others => '0'));
    elsif rising_edge(clk_i) then
      if write_i = '1' and SPI_busy_s(dac_s) = '0' then
        reg_s(4*dac_s + channel_s) <= val_i;
      end if;
    end if;
  end process;

  -- SPI channel selection	
  SPI_write_s(0) <= write_i when dac_s = 0 else '0';
  SPI_write_s(1) <= write_i when dac_s = 1 else '0';
  SPI_write_s(2) <= write_i when dac_s = 2 else '0';

  -- SPI instatiation
  SPI_inst: for i in 0 to 2 generate
    SPI_comp : SPI_master
      generic map(
        SPI_CLK_PRES_g => SPI_CLK_PRES_g
        )
      port map(
        clk_i => clk_i,
        rst_i => rst_i,
        channel_i => channel_i(1 downto 0),
        val_i => val_i,
        write_i => SPI_write_s(i),
        busy_o => SPI_busy_s(i),
        spi_dout_o => spi_mosi_o(i),
        spi_sclk_o => spi_sclk_o(i),
        spi_ss_o => spi_ss_o(i)
        );
  end generate;

end Behavioral;

