----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:47:53 06/09/2016 
-- Design Name: 
-- Module Name:    SPI_master - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SPI_master is
	Generic (
		SPI_CLK_PRES_g : integer := 50 -- Has to be a multiple of 2
		);
	Port ( 
		clk_i : in  STD_LOGIC;
		rst_i : in  STD_LOGIC;
		channel_i : in  STD_LOGIC_VECTOR (1 downto 0);
		val_i : in  STD_LOGIC_VECTOR (11 downto 0);
		write_i : in  STD_LOGIC;
		busy_o : out  STD_LOGIC;
		spi_dout_o : out  STD_LOGIC;
		spi_sclk_o : out  STD_LOGIC;
		spi_ss_o : out  STD_LOGIC
		);
end SPI_master;

architecture Behavioral of SPI_master is
	signal SPI_clk_s : std_logic;
	signal SPI_ss_s : std_logic;
	signal SPI_dout_s : std_logic;
	signal SPI_write_req_s : std_logic;
	signal SPI_writing_s : std_logic;
	signal SPI_data_s : std_logic_vector(15 downto 0);
begin

	busy_o <= SPI_writing_s;
	
	--Command Input Handler
	cmd_input: process(rst_i, clk_i)
	begin
		if rst_i = '1' then
			SPI_write_req_s <= '0';
			SPI_data_s <= (others => '0');
		elsif rising_edge(clk_i) then
			if SPI_writing_s = '1' then
				SPI_write_req_s <= '0';
			elsif write_i='1' then
				SPI_write_req_s <= '1';
				SPI_data_s <= channel_i & "01" & val_i;
			end if;
		end if;
	end process;
	
	-- SPI Clock generation
	spi_clk_gen: process(rst_i, clk_i)
		variable ctr_v : integer;
	begin
		if rst_i = '1' then
			ctr_v := 0;
			SPI_clk_s <= '0';
		elsif rising_edge(clk_i) then
			ctr_v := ctr_v + 1;
			if ctr_v = (SPI_CLK_PRES_g/2) then
				SPI_clk_s <= not SPI_clk_s;
				ctr_v := 0;
			end if;
		end if;
	end process;
	
	-- SPI Output Handler
	SPI_output: process(rst_i, clk_i)
		variable SPI_clk_old_v : std_logic;
		variable clk_ctr_v : integer;
	begin
		if rst_i = '1' then
			SPI_clk_old_v := '0';
			clk_ctr_v := 0;
			SPI_writing_s <= '0';
			spi_dout_s <= '0';
			spi_ss_s <= '0';
		elsif falling_edge(clk_i) then
			-- SPI_clk_s rising edge
			if SPI_clk_old_v = '0' and SPI_clk_s = '1' then
				spi_ss_s <= '0';
				spi_dout_s <= '0';
				-- When transmission already running
				if SPI_writing_s = '1' then
					clk_ctr_v := clk_ctr_v + 1;
					-- When end of data transmission
					if clk_ctr_v = 17 then
						SPI_writing_s <= '0';
					-- Otherwise emit data on dout
					else
						spi_dout_s <= SPI_data_s(16-clk_ctr_v);
					end if;
				-- When transmission required
				elsif SPI_write_req_s = '1' then
					SPI_writing_s <= '1';
					clk_ctr_v := 0;
					-- First rising edge -> Apply SS for 1 clock cycle 
					spi_ss_s <= '1';
				end if;
			end if;
			SPI_clk_old_v := SPI_clk_s;
		end if;
	end process;
	
	spi_out_latch: process(rst_i, clk_i)
	begin
		if rst_i = '1' then
			spi_sclk_o <= '0';
			spi_ss_o <= '0';
			spi_dout_o <= '0';
		elsif rising_edge(clk_i) then
			spi_sclk_o <= '0';
			spi_ss_o <= '0';
			spi_dout_o <= '0';
			if SPI_writing_s = '1' then
				spi_sclk_o <= spi_clk_s;
				spi_ss_o <= spi_ss_s;
				spi_dout_o <= spi_dout_s;
			end if;		
		end if;
	end process;

end Behavioral;

