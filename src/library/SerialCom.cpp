#include "SerialCom.h"

SerialCom::SerialCom() {
	dev = 0;
	baudrate = B115200;
}

SerialCom::SerialCom(std::string deviceName)
{
  dev = 0;
  baudrate = B115200;
  this->deviceName = deviceName;
  this->init(); 
}

SerialCom::~SerialCom() 
{
  if (dev) close(dev);
}

void SerialCom::init()
{
  dev = open(deviceName.c_str(), O_RDWR | O_NOCTTY);
  this->config();
}

void SerialCom::config()
{
  //Asynchronous read
  fcntl(dev, F_SETFL, O_NONBLOCK);
  _isConnected = true;

  if (tcgetattr(dev, &tty))
    {
      std::cerr << "[ERROR] " << __PRETTY_FUNCTION__ << " : Could not get tty attributes!" << std::endl;
      _isConnected = false;
    }

  tty_old = tty;

  cfsetospeed(&tty, baudrate);
  cfsetispeed(&tty, baudrate);

  tty.c_cflag &= ~PARENB; 	// no parity
  tty.c_cflag &= ~CSTOPB; 	// one stop bit
  tty.c_cflag &= ~CRTSCTS; 	// no flow control
  tty.c_cflag &= ~CSIZE;
  tty.c_cflag |= CS8; 		// 8 databits
  tty.c_cflag |= CREAD | CLOCAL;// turn on READ & ignore ctrl lines

  cfmakeraw(&tty);
  tty.c_lflag &= ~(ICANON|ECHO);// Non-canonical mode, no echo

  tcflush(dev, TCIFLUSH);	// Empty buffers

  if (tcsetattr(dev, TCSANOW, &tty))
    {
      std::cerr << "[ERROR] " << __PRETTY_FUNCTION__ << " : Could not set tty attributes!" << std::endl;
      _isConnected = false;
    }
}

bool SerialCom::isConnected()
{
  return _isConnected;
}

int SerialCom::flush()
{
  //tcflush seems not to work
  //return tcflush(dev, TCIFLUSH);
  char inbuf[2];
  while(this->read(inbuf,2) != 0);
  return 0;
}

int SerialCom::write(char *buf, size_t length)
{
  return ::write(dev, buf, length);
}

int SerialCom::read(char *buf, size_t length)
{
  unsigned int val = 0, timeout_ctr = 0, bytes_to_read = length;
  int temp = 0;

  // Read until enough bytes have arrived or the timeout time has elapsed
  do
    {
      temp = ::read(dev, buf + val, length - val);
      if(temp > 0)
	{
	  val += temp;
	  bytes_to_read -= temp;
	}
      if(val == length) break;
      usleep(100);
    }
  while(++timeout_ctr < timeout_ms * 10);

  return val;
}

