#include <MOJO.h>

using namespace std;

Mojo::Mojo(){
  //init();
}

Mojo::~Mojo(){
  delete Com;
}

void Mojo::setCom(ComInterface* _Com)
{
  this->Com = _Com;
}

int Mojo::init(){
  if(resetCom()) return -1;

  Com->flush();

#ifdef MOJO_DEBUG
  cout << "--Connected" << endl;
#endif

  return 0;
}

bool Mojo::isConnected()
{
  return (Com->isConnected() && Communication_established);
}

int Mojo::resetCom(){
  char outbuf[] = "\r\n";
  char inbuf[2];
  int try_ctr = 0;

  Communication_established = false;

  do
    {
      Com->flush();
#ifdef MOJO_DEBUG
      cout << "--Write: " ;
      for(int i = 0; i < 2; i++) cout << hex << (int)(unsigned char)outbuf[i] << dec << " ";
      cout << endl;
#endif
      // Write reset request
      Com->write(outbuf,2);
      usleep(10000);	

      if(Com->read(inbuf,2) < 2)
#ifdef MOJO_DEBUG
	cout << "  No Answer" << endl; 
#else
      ;
#endif
      else
	{
#ifdef MOJO_DEBUG
	  cout << "  Answer: " << (int)(unsigned char)inbuf[0] << " " << (int)(unsigned char)inbuf[1] << endl;
#endif
	  //Check for synchronization character at end
	  if (inbuf[1] == '\n') break;
	}
      //Delay to receive characters, if they were just sent
      usleep(10000);	
    }
  while(++try_ctr < Com_maxAttempts);

  if(try_ctr >= Com_maxAttempts) return -1;

  Communication_established = true;
  return 0;
}

int Mojo::writeReg(int reg, int val)
{
  char outbuf[7];

  //Check if communication established
  if(!Com->isConnected()) return -1;

  //Init write buffer
  outbuf[0] = 0x80 | ((reg >> 8) & 0xFF);
  outbuf[1] = reg & 0xFF;
  outbuf[2] = (val >> 24 ) & 0xFF;
  outbuf[3] = (val >> 16 ) & 0xFF;
  outbuf[4] = (val >> 8 ) & 0xFF;
  outbuf[5] = val & 0xFF;
  outbuf[6] = '\n';

#ifdef MOJO_DEBUG
  cout << "--Write: " ;
  for(int i = 0; i < 7; i++) cout << hex << (int)(unsigned char)outbuf[i] << " ";
  cout << endl;
#endif
  Com->write(outbuf,7);	
  return 0;
}

int Mojo::readReg(int reg, int &val)
{
  char outbuf[3], inbuf[6];
  int try_ctr = 0;

  //Check if communication established
  if(!Com->isConnected()) return -1;

  //Init write buffer
  outbuf[0] = 0x00 | ((reg >> 8) & 0xFF);
  outbuf[1] = reg & 0xFF;
  outbuf[2] = '\n';

  //Com->flush();
#ifdef MOJO_DEBUG
  cout << "--Read: " ;
  for(int i = 0; i < 3; i++) cout << hex << (int)(unsigned char)outbuf[i] << " ";
  cout << endl;
#endif
  Com->write(outbuf,3);	

  //Check if Databytes following
  if(Com->read(inbuf,6) < 4)     { Communication_established=false; return -3; }
#ifdef MOJO_DEBUG
  cout << "  Answer: ";
  for(int i = 0; i < 6; i++) cout << (int)(unsigned char)inbuf[i] << " ";
  std::cout << std::endl;
#endif
  //Check for Error flags in status byte (not Databytes flag)
  if(inbuf[0] & 0x07)            { Communication_established=false; return -4; }
  //Check for synchronization character
  if(inbuf[5] != '\n')           { Communication_established=false; return -5; }
  //Transaction succeeded
  //Re-assemble register
  val = 0;
  for(int i = 1; i < 5; i++) val = (val<<8) + (unsigned char)inbuf[i];
#ifdef MOJO_DEBUG
  cout << " -> " << val << endl;
#endif
  return 0;
}
