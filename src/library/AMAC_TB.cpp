#include <AMAC_TB.h>

using namespace std;

//=========================================================================
//= Basic Functions
//=========================================================================

AMAC_TB::AMAC_TB(std::string serial_port)
{
  this->serial_port = serial_port;
  Init();
}

AMAC_TB::AMAC_TB()
{
  serial_port = "/dev/ttyACM0";
  Init();
}

AMAC_TB::~AMAC_TB()
{
  delete MOJO;
}

int AMAC_TB::Init()
{
  MOJO = new Mojo();
  SerialCom* serial = new SerialCom(serial_port);
  MOJO->setCom(serial);
  MOJO->init();
  if(!MOJO->isConnected()) return -1;
  return 0;
}

bool AMAC_TB::isConnected()
{
  return MOJO->isConnected();
}

//=========================================================================
//= Read / Write register with Error Handling
//=========================================================================

int AMAC_TB::readMojoReg(int regnr, int &regvalue)
{
  int error=MOJO->readReg(regnr, regvalue);
#ifdef AMAC_TB_DEBUG
  cout << " R " << hex << regnr << ": " << regvalue << " " << endl;
#endif
  if(error == 0) return 0;
  else return -1;
}

int AMAC_TB::writeMojoReg(int regnr, int regvalue)
{
#ifdef AMAC_TB_DEBUG
  cout << " W " << hex << regnr << ": " << regvalue << " " << endl;
#endif
  int error=MOJO->writeReg(regnr, regvalue);
  if(error == 0) return 0;
  else return -1;
}

//=========================================================================
//= Board Access
//=========================================================================

int AMAC_TB::setReset(bool reset_active)
{
  int regs_old;
#ifdef AMAC_TB_DEBUG
  cout << "Reset: " << reset_active << endl;
#endif

  //Get current state of AMAC input register
  if(readMojoReg(AMACTEST_MOJO_REG_AMAC_IN, regs_old)) return -1;

  //Change Reset output
  if(reset_active) 	regs_old &= ~AMACTEST_MOJO_AMAC_IN_RESETB;
  else			regs_old |= AMACTEST_MOJO_AMAC_IN_RESETB;

  //Write AMAC input register
  if(writeMojoReg(AMACTEST_MOJO_REG_AMAC_IN, regs_old)) return -1;
  return 0;
}

int AMAC_TB::doReset()
{
  if(setReset(true)) return -1;
  usleep(100000);
  if(setReset(false)) return -1;

  return 0;
}

int AMAC_TB::PowerUp()
{
#ifdef AMAC_TB_DEBUG
  cout << "Power Up" << endl;
#endif

  if(setReset(true)) return -1;
  usleep(100000);

  //Configure VCC_IO (DAC ch4)
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_CH, 4)) return -1;
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_VAL, VCC_IO_Vset)) return -1;
  //Enable Power supply
  if(writeMojoReg(AMACTEST_MOJO_REG_SUP, 7)) return -1;
  usleep(100000);
	
  //Release AMAC from Reset
  if(setReset(false)) return -1;

  return 0;
}

int AMAC_TB::PowerDown(){
	#ifdef AMAC_TB_DEBUG
	cout << "Power Down" << endl;
	#endif
	if(writeMojoReg(AMACTEST_MOJO_REG_SUP, 0)) return -1;
	return 0;
}

int AMAC_TB::Restart(){
	if(PowerDown())return -1;
	sleep(1);
	if(PowerUp()) return -1;
	return 0;
}

int AMAC_TB::setAMAC_ID(int _ID){
	int regs_old;

	//Store ID locally
	this->ID = _ID & 0xF;
	#ifdef AMAC_TB_DEBUG
	cout << "New ID: " << ID << endl;
	#endif

	//Write ID to test board
	//	Get current state of AMAC input register
	if(readMojoReg(AMACTEST_MOJO_REG_AMAC_IN, regs_old)) return -1;

	//	Change ID
	regs_old &= ~AMACTEST_MOJO_AMAC_IN_ID_MASK;
	regs_old |= AMACTEST_MOJO_AMAC_IN_ID_W(ID);

	//	Write AMAC input register
	if(writeMojoReg(AMACTEST_MOJO_REG_AMAC_IN, regs_old)) return -1;
	return 0;
}

int AMAC_TB::setVCC_IO(int Vset)
{
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_CH, 4)) return -1;
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_VAL, Vset)) return -1;
  VCC_IO_Vset = Vset;
  return 0;
}

int AMAC_TB::setVCC_IO(double value)
{
  //Search DAC value closest to requested value
  const double diff = VCC_IO_MAX - VCC_IO_MIN;
  const double stepsize = diff / 4096;
  int Vset = (int) ((VCC_IO_MAX - value) / stepsize);
  //If value outside limits was requested, get as close possible
  if(Vset > 0xFFF) Vset = 0xFFF;
  if(Vset < 0)	 Vset = 0;
  return setVCC_IO(Vset);
}

int AMAC_TB::getVCC_IO(double &value)
{
  const double diff = VCC_IO_MAX - VCC_IO_MIN;
  const double stepsize = diff / 4096;
  value = VCC_IO_MAX - (double)VCC_IO_Vset * stepsize;
  return 0;
}

int AMAC_TB::I2C_reset()
{
#ifdef AMAC_TB_DEBUG
  cout << "Reset I2C" << endl;
#endif
  //Disable I2C module
  if(writeMojoReg(0x102, 0x0)) return -1;

  // Software pulse the reset (+ drive SCL)
  if(writeMojoReg(AMACTEST_MOJO_REG_LOG_CTRL, 0)) return -1;
  //if(writeMojoReg(AMACTEST_MOJO_REG_LOG_CTRL, AMACTEST_MOJO_LOG_CTRL_I2C_RST | AMACTEST_MOJO_LOG_CTRL_SCL_ACT)) return -1;
  if(writeMojoReg(AMACTEST_MOJO_REG_LOG_CTRL, AMACTEST_MOJO_LOG_CTRL_I2C_RST)) return -1;

  //Initialization of I2C module
  if(writeMojoReg(0x100, 0x00)) return -1;
  if(writeMojoReg(0x101, 0x01)) return -1;
  if(writeMojoReg(0x102, 0x80)) return -1;
  return 0;
}

int AMAC_TB::I2C_enable()
{
#ifdef AMAC_TB_DEBUG
  cout << "Enable I2C" << endl;
#endif
  //Release I2C module from reset (SCL is driven actively)
  if(writeMojoReg(AMACTEST_MOJO_REG_LOG_CTRL, 0)) return -1;
  if(writeMojoReg(AMACTEST_MOJO_REG_LOG_CTRL, AMACTEST_MOJO_LOG_CTRL_I2C_RST | AMACTEST_MOJO_LOG_CTRL_SCL_ACT)) return -1;
  //Initialization of I2C module
  if(writeMojoReg(0x100, 0x00)) return -1;
  if(writeMojoReg(0x101, 0x01)) return -1;
  if(writeMojoReg(0x102, 0x80)) return -1;
  return 0;
}

int AMAC_TB::getDACvoltage(int channel, double &voltage)
{
  //Could be replaced by Calibration curves
  if(channel < 8) 	voltage = (double)AMAC_1V2 / 4096 * DAC_values[channel];
  else			voltage = (double)MOJO_3V3 / 4096 * DAC_values[channel];
  return 0;
}

int AMAC_TB::setDAC(int channel, int value)
{
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_CH , channel)) return -1;
  if(writeMojoReg(AMACTEST_MOJO_REG_DAC_VAL, value  )) return -1;
  DAC_values[channel] = value & 0xFFF;
  return 0;
}

int AMAC_TB::setDACvoltage(int channel, double voltage)
{
  double actual_voltage;
  int i;
  //Try to get as close as possible to target voltage
  //Could be replaced by calibration curve
  for(i = 0; i < 4096; i++)
    {
      DAC_values[channel] = i;
      getDACvoltage(channel, actual_voltage);
      if(actual_voltage >= voltage) break;
    }
  return setDAC(channel, i);
}

int AMAC_TB::setCurrent(bool left_channel, double value)
{
  int DACval, DACval_old;
  double Current, Current_old;
  int channel;
  //Get best combination (channel & value) of DAC configuration for current generation
  //	Read calibration data from config file
  for(channel = 3; channel>= 0; channel--)
    {
      std::fstream configfile(("config/Current_cal_ch" + to_string(channel) + ".txt"), fstream::in);
      DACval_old = 0;
      Current_old = 0.0;
      while(configfile)
	{
	  configfile >> DACval >> Current;
	  if(Current > value) break;
	  if(DACval > 4000) break;
	  DACval_old = DACval; Current_old = Current;
	}
      if(Current > value) break;
    }
  //	combination with current higher than target value in DACval/Current
  //	combination with current lower than target value in DACval_old/Current_old
  //	Linear interpolation
  int diff = DACval - DACval_old;
  double step = (Current - Current_old) / diff;
  int val_to_add = (int) ((value - Current_old) / step);
  int val_to_write = DACval_old + val_to_add;
  setCurrent(left_channel, channel, val_to_write);
	
  return 0;
}

int AMAC_TB::setCurrent(bool left_channel, int channel, double value)
{
  int DACval, DACval_old;
  double Current, Current_old;

  std::fstream configfile(("config/Current_cal_ch" + to_string(channel) + ".txt"), fstream::in);
  DACval_old = 0;
  Current_old = 0.0;
  while(configfile)
    {
      configfile >> DACval >> Current;
      if(Current > value) break;
      if(DACval > 4000) break;
      DACval_old = DACval; Current_old = Current;
    }

  //	combination with current higher than target value in DACval/Current
  //	combination with current lower than target value in DACval_old/Current_old
  //	Linear interpolation
  int diff = DACval - DACval_old;
  double step = (Current - Current_old) / diff;
  int val_to_add = (int) ((value - Current_old) / step);
  int val_to_write = DACval_old + val_to_add;
  setCurrent(left_channel, channel, val_to_write);
	
  return 0;
}

int AMAC_TB::setCurrent(bool left_channel, int channel, int value)
{
  int reg;	
  int mux_channel = 0; if(left_channel) mux_channel = 1;
  //if right channel is not yet selected on current mux
  if(channel != Current_channels[mux_channel])
    {
      reg = AMACTEST_MOJO_MUX_POS_W(channel);
      //MUX Enable low while switching channels
      if(writeMojoReg(AMACTEST_MOJO_REG_MUX, reg)) return -1;
    }

  int DAC_channel = 8; if(left_channel) DAC_channel = 9;
  if(setDAC(DAC_channel, value)) return -1;

  //if current mux channel was switched, re-enable current mux
  if(channel != Current_channels[mux_channel])
    {
      //Enable High
      reg |= AMACTEST_MOJO_MUX_EN;
      if(writeMojoReg(AMACTEST_MOJO_REG_MUX, reg)) return -1;
      Current_channels[mux_channel] = channel;
    }
  Current_values[mux_channel] = value;
  return 0;
}

int AMAC_TB::enableCurrent(bool enable)
{
  int reg;
  if(readMojoReg(AMACTEST_MOJO_REG_MUX, reg)) return -1;
  if(enable) 	reg |= AMACTEST_MOJO_MUX_EN;
  else		reg &= ~AMACTEST_MOJO_MUX_EN;
  if(writeMojoReg(AMACTEST_MOJO_REG_MUX, reg)) return -1;
  return 0;
}

int AMAC_TB::getCurrent(bool left_channel, double &value)
{
  int DACval, DACval_old;
  double Current, Current_old;
  int mux_channel = 0; if(left_channel) mux_channel = 1;
  std::fstream configfile(("config/Current_cal_ch" + to_string(Current_channels[mux_channel]) + ".txt"), fstream::in);

  DACval_old = 0;
  Current_old = 0.0;
  //Get upper and lower value from configfile
  while(configfile)
    {
      configfile >> DACval >> Current;
      if(DACval > Current_values[mux_channel]) break;
      DACval_old = DACval; Current_old = Current;
    }
  //Apply linear interpolation
  int diff = DACval - DACval_old;
  double step = (Current - Current_old) / diff;
  value = Current_old + (Current_values[mux_channel] - DACval_old) * step;
  return 0;
}

int AMAC_TB::getADC(int channel, int &value)
{
  int reg;
  switch(channel)
    {
    case 0: reg = AMACTEST_MOJO_REG_ADC0; break;
    case 1: reg = AMACTEST_MOJO_REG_ADC1; break;
    case 4: reg = AMACTEST_MOJO_REG_ADC4; break;
    case 5: reg = AMACTEST_MOJO_REG_ADC5; break;
    case 6: reg = AMACTEST_MOJO_REG_ADC6; break;
    case 7: reg = AMACTEST_MOJO_REG_ADC7; break;
    case 8: reg = AMACTEST_MOJO_REG_ADC8; break;
    case 9: reg = AMACTEST_MOJO_REG_ADC9; break;
    default: return -2; break;
    }
  if(readMojoReg(reg, value)) return -1;
  return 0;
}

int AMAC_TB::getADCvoltage(int channel, double &value)
{
  int ret, ADCval;
  ret = getADC(channel, ADCval);
  value = MOJO_3V3 /1024 * ADCval;
  return ret;
}

//Should be calibrated
int AMAC_TB::getADCcurrent(int channel, double &value)
{
  int ret, ADCval;
  ret = getADC(channel, ADCval);
  value = MOJO_3V3 / 1024 * ADCval / 14 * 1.0872;
  return ret;
}	

int AMAC_TB::ClkOut_analysis(int channel, int timespan_ms, int &frequency, int &DutyCycle)
{
  int reg;
  // Select Input
  reg = AMACTEST_MOJO_CLKANA_CTRL_INP_W(channel);
  if(writeMojoReg(AMACTEST_MOJO_REG_CLKANA_CTRL, reg)) return -1;
  // Start Transition counter
  reg = AMACTEST_MOJO_CLKANA_CTRL_CTR_TIME_W(timespan_ms * 10) | AMACTEST_MOJO_CLKANA_CTRL_INP_W(channel);
  if(writeMojoReg(AMACTEST_MOJO_REG_CLKANA_CTRL, reg)) return -1;
  //Wait until completed
  do
    {
      if(readMojoReg(AMACTEST_MOJO_REG_CLKANA_CTRL, reg)) return -1;
    }
  while(reg & AMACTEST_MOJO_CLKANA_CTRL_BUSY);
  //Read and evaluate transition counter
  if(readMojoReg(AMACTEST_MOJO_REG_CLKANA_CTR, reg)) return -1;
  //cout << "Counter reg: " << reg << endl;
  double quotient = 1000.0 / timespan_ms;
  frequency = (int)(quotient * reg);
  //Read and evaluate hilo counter (averaging out of 10)
  int hi=0, lo=0;
  for( int i = 0; i < 10; i++)
    {
      if(readMojoReg(AMACTEST_MOJO_REG_CLKANA_HILO, reg)) return -1;
      hi += AMACTEST_MOJO_CLKANA_HILO_HI_R(reg);
      lo += AMACTEST_MOJO_CLKANA_HILO_LO_R(reg);
      usleep(100);
    }
  DutyCycle = (int)(100*(double)hi/(hi+lo));
  return 0;
}

int AMAC_TB::set_ClkIn(int frequency){
	int prescaler = MOJO_CLK_FREQUENCY / frequency;
	if(writeMojoReg(AMACTEST_MOJO_REG_CLKGEN, prescaler)) return -1;
	return 0;
}

int AMAC_TB::get_AMACout_LVCTRL(int &LVCTRL, int &LVCTRLB){
	int reg;
	if(readMojoReg(AMACTEST_MOJO_REG_AMAC_OUT, reg)) return -1;
	LVCTRL = (int)(bool)(reg & AMACTEST_MOJO_AMAC_OUT_LVCNTRL);
	LVCTRLB = (int)(bool)(reg & AMACTEST_MOJO_AMAC_OUT_LVCNTRLB);
	return 0;
}

int AMAC_TB::get_AMACout_HVCTRL(int &HVCTRL, int &HVCTRLB)
{
  int reg;
  if(readMojoReg(AMACTEST_MOJO_REG_AMAC_OUT, reg)) return -1;
  HVCTRL = (int)(bool)(reg & AMACTEST_MOJO_AMAC_OUT_HVCNTRL);
  HVCTRLB = (int)(bool)(reg & AMACTEST_MOJO_AMAC_OUT_HVCNTRLB);
  return 0;
}

//Continuous pulse counter. Read once in the beginning, so _old values are initialized
int AMAC_TB::get_AMACout_LAM_pulses(int &ILOCKLAM, int &WARNLAM){
	static unsigned int ctr_ilock_old = 0, ctr_warn_old = 0;
	unsigned int ctr_ilock, ctr_warn;
	int reg;
	if(readMojoReg(AMACTEST_MOJO_REG_PULSE_CTR, reg)) return -1;
	ctr_ilock = AMACTEST_MOJO_PULSE_CTR_ILOCKLAM_R(reg);
	ctr_warn = AMACTEST_MOJO_PULSE_CTR_WARNLAM_R(reg);
	ILOCKLAM = ctr_ilock - ctr_ilock_old;
	if(ILOCKLAM < 0) ILOCKLAM += 65536;
	WARNLAM = ctr_warn - ctr_warn_old;
	if(WARNLAM < 0) WARNLAM += 65536;
	ctr_ilock_old = ctr_ilock;
	ctr_warn_old = ctr_warn;
	return 0;
}

int AMAC_TB::HVCNTRL_isOn(bool &isOn)
{
  int freq, dutycycle;
  bool HVCNTRL_isOn = true, HVCNTRLB_isOn = true;

  //Read HVCTRL output and inverting output. One of them should be constant 0 (freq & dutycycle = 0), the other not
  if(ClkOut_analysis(CLK_CHANNEL_HVCNTRL, 100, freq, dutycycle)) return -1;
  if(freq == 0 && dutycycle == 0) HVCNTRL_isOn = false;

  if(ClkOut_analysis(CLK_CHANNEL_HVCNTRLB, 100, freq, dutycycle)) return -1;
  if(freq == 0 && dutycycle == 0) HVCNTRLB_isOn = false;

  //If both are 0 ore none of them is -> Error
  if (HVCNTRL_isOn == HVCNTRLB_isOn) return -2;
  isOn = HVCNTRL_isOn;
  return 0;
}

int AMAC_TB::LVCNTRL_isOn(bool &isOn)
{
  int LVCNTRL, LVCNTRLB;

  if(get_AMACout_LVCTRL(LVCNTRL, LVCNTRLB)) return -1;

  if(LVCNTRL && !LVCNTRLB) 	isOn = true;
  else if(!LVCNTRL && LVCNTRLB) isOn = false;
  else				return -2;

  return 0;
}

//=========================================================================
//= AMAC access
//=========================================================================

int AMAC_TB::I2C_writeBytes(int num, int* Bytes)
{ // At least 1 address and 1 Data Byte
#ifdef AMAC_TB_DEBUG
  cout << "Write I2C" << endl;
#endif
  int reg;

  if(num < 2)
    {
      //cout << "    Too few bytes to send" << endl;
      return -1;
    }

  // Write Byte + Start Condition
  if(writeMojoReg(0x103,*Bytes)) return -1;
  if(writeMojoReg(0x104,0x90)) return -1;
  do
    {
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg&0x02);
  if(reg != 0x41)
    return -1;
  Bytes++;

  // Write Bytes
  for(int i = 1; i < (num - 1); i++, Bytes++)
    {
      if(writeMojoReg(0x103,*Bytes)) return -1;
      if(writeMojoReg(0x104,0x10)) return -1;
      do
	{
	  if(readMojoReg(0x104, reg)) return -1;
	}
      while(reg&0x02);
      if(reg != 0x41)
  	return -1;
    }

  // Write Byte + Stop Condition
  if(writeMojoReg(0x103,*Bytes)) return -1;
  if(writeMojoReg(0x104,0x50)) return -1;
  do
    {
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg&0x02);
  if(reg != 0x01)
    return -1;

  return 0;
}

int AMAC_TB::I2C_readBytes(int slave_addr, int reg_addr, int num, int* &Bytes)
{
  int i, reg;
  Bytes = (int*) malloc(num * sizeof(int));
#ifdef AMAC_TB_DEBUG
  cout << "Read I2C" << endl;
#endif

  // Write Slave Address(RW = W) + Start Condition
  if(writeMojoReg(0x103, slave_addr*2)) return -1;
  if(writeMojoReg(0x104,0x90)) return -1;
  do
    {
      usleep(1000);
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg & 0x02);
  if(reg != 0x41)
    return -1;

  // Write Register Address
  if(writeMojoReg(0x103, reg_addr)) return -1;
  if(writeMojoReg(0x104,0x10)) return -1;
  do
    {
      usleep(1000);
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg & 0x02);
  if(reg != 0x41)
    return -1;

  // Write Slave Address(RW = R) + Repeated Start Condition
  if(writeMojoReg(0x103, slave_addr*2+1)) return -1;
  if(writeMojoReg(0x104,0x90)) return -1;
  do
    {
      usleep(1000);
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg & 0x02);
  if(reg != 0x41)
    return -1;

  // Read Bytes
  for(i = 0; i < (num - 1); i++)
    {;
      if(writeMojoReg(0x104,0x20)) return -1;
      do
	{
	  usleep(1000);
	  if(readMojoReg(0x104, reg)) return -1;
	}
      while(reg & 0x02);
      if(reg != 0x41)
	return -1;
      if(readMojoReg(0x103, Bytes[i])) return -1;
    }

  // Read last byte + Stop Condition & NACK
  if(writeMojoReg(0x104,0x68)) return -1;
  do
    {
      usleep(1000);
      if(readMojoReg(0x104, reg)) return -1;
    }
  while(reg & 0x02);
  if(reg != 0x81)
    return -1;
  if(readMojoReg(0x103, Bytes[i])) return -1;

  return 0;
}

int AMAC_TB::I2C_write(int addr, int num, int* Bytes)
{
  int BytesToSend[num + 2];
  BytesToSend[0] = ID * 2;
  BytesToSend[1] = addr;
  for(int i = 0; i < num; i++)
    BytesToSend[i+2] = Bytes[i];
  return I2C_writeBytes( num + 2, BytesToSend);
}

int AMAC_TB::I2C_read(int addr, int num, int* &Buf)
{
  return I2C_readBytes(ID, addr, num, Buf);
}

//Works until up to 4 bytes
int AMAC_TB::AMAC_readBits(int startreg, int num_bits, int offset, int &value){
  int num_bytes = ((num_bits + offset -1) / 8) + 1;
  int* buf;
  int temp;
  if(I2C_read(startreg,num_bytes,buf)) return -1;	
  temp = 0;
  for(int i = num_bytes - 1; i >= 0; i--){
    temp <<= 8;
    temp += (buf[i] & 0xFF);
  }
  value = (temp >> offset) & ((1 << num_bits) -1);
  return 0;
}

//Works until up to 4 bytes
//Read register, modify bits, write register
int AMAC_TB::AMAC_writeBits(int startreg, int num_bits, int offset, int value){
  int num_bytes = ((num_bits + offset -1) / 8) + 1;
  int* buf;
  int temp;
  int mask = ((1 << num_bits) -1) << offset;
  if(I2C_read(startreg,num_bytes,buf)) return -1;	
  temp = 0;
  for(int i = num_bytes - 1; i >= 0; i--){
    temp <<= 8;
    temp += (buf[i] & 0xFF);
  }
  temp &= ~mask;
  temp |= ((value << offset) & mask);
  for(int i = 0; i < num_bytes; i++, temp >>= 8)
    buf[i] = temp & 0xFF;
  if(I2C_write(startreg,num_bytes,buf)) return -1;
  return 0;
}
		
//Reads the according bits from the according AMAC register
int AMAC_TB::readAMAC(AMACreg reg, int &val){
	switch(reg){
		case AMACreg::STATUS_HV_ILOCK: 	return AMAC_readBits(0,1,0, val);
		case AMACreg::STATUS_LV_ILOCK: 	return AMAC_readBits(0,1,1, val);
		case AMACreg::STATUS_HV_WARN:   return AMAC_readBits(0,1,2, val);
		case AMACreg::STATUS_LV_WARN: 	return AMAC_readBits(0,1,3, val);
		case AMACreg::STATUS_ID: 	return AMAC_readBits(0,4,4, val);
		case AMACreg::VALUE_LEFT_CH0: 	return AMAC_readBits(20,10,0, val);
		case AMACreg::VALUE_LEFT_CH1: 	return AMAC_readBits(21,10,2, val);
		case AMACreg::VALUE_LEFT_CH2: 	return AMAC_readBits(22,10,4, val);
		case AMACreg::VALUE_LEFT_CH3: 	return AMAC_readBits(23,10,6, val);
		case AMACreg::VALUE_LEFT_CH4: 	return AMAC_readBits(25,10,0, val);
		case AMACreg::VALUE_LEFT_CH5: 	return AMAC_readBits(26,10,2, val);
		case AMACreg::VALUE_LEFT_CH6: 	return AMAC_readBits(27,10,4, val);
		case AMACreg::VALUE_RIGHT_CH0: 	return AMAC_readBits(29,10,0, val);
		case AMACreg::VALUE_RIGHT_CH1: 	return AMAC_readBits(30,10,2, val);
		case AMACreg::VALUE_RIGHT_CH2: 	return AMAC_readBits(31,10,4, val);
		case AMACreg::VALUE_RIGHT_CH3: 	return AMAC_readBits(32,10,6, val);
		case AMACreg::VALUE_RIGHT_CH4: 	return AMAC_readBits(34,10,0, val);
		case AMACreg::VALUE_RIGHT_CH5: 	return AMAC_readBits(35,10,2, val);
		case AMACreg::VALUE_RIGHT_CH6: 	return AMAC_readBits(36,10,4, val);	
		case AMACreg::HV_ENABLE:	return AMAC_readBits(43,1,0,val);	
		case AMACreg::LV_ENABLE:	return AMAC_readBits(44,1,0,val);
		case AMACreg::HV_FREQ:		return AMAC_readBits(43,2,1,val);
		case AMACreg::ILOCK_FLAG_HV_LEFT_HI:	return AMAC_readBits(1,7,0,val);
		case AMACreg::ILOCK_FLAG_HV_LEFT_LO:	return AMAC_readBits(2,7,0,val);
		case AMACreg::ILOCK_FLAG_HV_RIGHT_HI:	return AMAC_readBits(3,7,0,val);
		case AMACreg::ILOCK_FLAG_HV_RIGHT_LO:	return AMAC_readBits(4,7,0,val);
		case AMACreg::ILOCK_FLAG_LV_LEFT_HI:	return AMAC_readBits(5,7,0,val);
		case AMACreg::ILOCK_FLAG_LV_LEFT_LO:	return AMAC_readBits(6,7,0,val);
		case AMACreg::ILOCK_FLAG_LV_RIGHT_HI:	return AMAC_readBits(7,7,0,val);
		case AMACreg::ILOCK_FLAG_LV_RIGHT_LO:	return AMAC_readBits(8,7,0,val);
		case AMACreg::WARN_FLAG_HV_LEFT_HI:	return AMAC_readBits(9,7,0,val);
		case AMACreg::WARN_FLAG_HV_LEFT_LO:	return AMAC_readBits(10,7,0,val);
		case AMACreg::WARN_FLAG_HV_RIGHT_HI:	return AMAC_readBits(11,7,0,val);
		case AMACreg::WARN_FLAG_HV_RIGHT_LO:	return AMAC_readBits(12,7,0,val);
		case AMACreg::WARN_FLAG_LV_LEFT_HI:	return AMAC_readBits(13,7,0,val);
		case AMACreg::WARN_FLAG_LV_LEFT_LO:	return AMAC_readBits(14,7,0,val);
		case AMACreg::WARN_FLAG_LV_RIGHT_HI:	return AMAC_readBits(15,7,0,val);
		case AMACreg::WARN_FLAG_LV_RIGHT_LO:	return AMAC_readBits(16,7,0,val);
		case AMACreg::ILOCK_EN_HV_LEFT_HI:	return AMAC_readBits(50,7,0,val);
		case AMACreg::ILOCK_EN_HV_LEFT_LO:	return AMAC_readBits(51,7,0,val);
		case AMACreg::ILOCK_EN_HV_RIGHT_HI:	return AMAC_readBits(52,7,0,val);
		case AMACreg::ILOCK_EN_HV_RIGHT_LO:	return AMAC_readBits(53,7,0,val);
		case AMACreg::ILOCK_EN_LV_LEFT_HI:	return AMAC_readBits(54,7,0,val);
		case AMACreg::ILOCK_EN_LV_LEFT_LO:	return AMAC_readBits(55,7,0,val);
		case AMACreg::ILOCK_EN_LV_RIGHT_HI:	return AMAC_readBits(56,7,0,val);
		case AMACreg::ILOCK_EN_LV_RIGHT_LO:	return AMAC_readBits(57,7,0,val);
		case AMACreg::WARN_EN_HV_LEFT_HI:	return AMAC_readBits(58,7,0,val);
		case AMACreg::WARN_EN_HV_LEFT_LO:	return AMAC_readBits(59,7,0,val);
		case AMACreg::WARN_EN_HV_RIGHT_HI:	return AMAC_readBits(60,7,0,val);
		case AMACreg::WARN_EN_HV_RIGHT_LO:	return AMAC_readBits(61,7,0,val);
		case AMACreg::WARN_EN_LV_LEFT_HI:	return AMAC_readBits(62,7,0,val);
		case AMACreg::WARN_EN_LV_LEFT_LO:	return AMAC_readBits(63,7,0,val);
		case AMACreg::WARN_EN_LV_RIGHT_HI:	return AMAC_readBits(64,7,0,val);
		case AMACreg::WARN_EN_LV_RIGHT_LO:	return AMAC_readBits(65,7,0,val);
		default:
			return -2; break;
	}
}

//Writes the according bits in the according AMAC register
int AMAC_TB::writeAMAC(AMACreg reg, int val){
	switch(reg){
		case AMACreg::LEFT_RAMP_GAIN: 		return AMAC_writeBits(45,2,0,val);	
		case AMACreg::RIGHT_RAMP_GAIN: 		return AMAC_writeBits(45,2,4,val);	
		case AMACreg::OPAMP_GAIN_RIGHT:		return AMAC_writeBits(42,4,4,val);
		case AMACreg::OPAMP_GAIN_LEFT:		return AMAC_writeBits(42,4,0,val);
		case AMACreg::BANDGAP_CONTROL: 		return AMAC_writeBits(40,5,0,val);		
		case AMACreg::RT_CH3_GAIN_SEL: 		return AMAC_writeBits(45,1,7,val);		
		case AMACreg::LT_CH3_GAIN_SEL: 		return AMAC_writeBits(45,1,3,val);		
		case AMACreg::RT_CH0_SEL: 		return AMAC_writeBits(45,1,6,val);		
		case AMACreg::LT_CH0_SEL: 		return AMAC_writeBits(45,1,2,val);	
		case AMACreg::CLK_OUT_ENABLE : 		return AMAC_writeBits(41,1,0,val);	
		case AMACreg::CLK_SELECT:		return AMAC_writeBits(49,1,0,val);	
		case AMACreg::HV_FREQ:			return AMAC_writeBits(43,2,1,val);	
		case AMACreg::HV_ENABLE:		return AMAC_writeBits(43,1,0,val);	
		case AMACreg::LV_ENABLE:		return AMAC_writeBits(44,1,0,val);	
		case AMACreg::HV_ILOCK:			return AMAC_writeBits(43,1,7,val);
		case AMACreg::HV_ILOCK_LAM:		return AMAC_writeBits(43,1,5,val);	
		case AMACreg::LV_ILOCK:			return AMAC_writeBits(44,1,7,val);
		case AMACreg::LV_ILOCK_LAM:		return AMAC_writeBits(44,1,5,val);
		case AMACreg::RESET_HV_ILOCK:		return AMAC_writeBits(48,1,0,val);
		case AMACreg::RESET_HV_WARN:		return AMAC_writeBits(48,1,1,val);
		case AMACreg::RESET_LV_ILOCK:		return AMAC_writeBits(48,1,2,val);
		case AMACreg::RESET_LV_WARN:		return AMAC_writeBits(48,1,3,val);
		case AMACreg::ILOCK_EN_HV_LEFT_HI:	return AMAC_writeBits(50,7,0,val);
		case AMACreg::ILOCK_EN_HV_LEFT_LO:	return AMAC_writeBits(51,7,0,val);
		case AMACreg::ILOCK_EN_HV_RIGHT_HI:	return AMAC_writeBits(52,7,0,val);
		case AMACreg::ILOCK_EN_HV_RIGHT_LO:	return AMAC_writeBits(53,7,0,val);
		case AMACreg::ILOCK_EN_LV_LEFT_HI:	return AMAC_writeBits(54,7,0,val);
		case AMACreg::ILOCK_EN_LV_LEFT_LO:	return AMAC_writeBits(55,7,0,val);
		case AMACreg::ILOCK_EN_LV_RIGHT_HI:	return AMAC_writeBits(56,7,0,val);
		case AMACreg::ILOCK_EN_LV_RIGHT_LO:	return AMAC_writeBits(57,7,0,val);
		case AMACreg::WARN_EN_HV_LEFT_HI:	return AMAC_writeBits(58,7,0,val);
		case AMACreg::WARN_EN_HV_LEFT_LO:	return AMAC_writeBits(59,7,0,val);
		case AMACreg::WARN_EN_HV_RIGHT_HI:	return AMAC_writeBits(60,7,0,val);
		case AMACreg::WARN_EN_HV_RIGHT_LO:	return AMAC_writeBits(61,7,0,val);
		case AMACreg::WARN_EN_LV_LEFT_HI:	return AMAC_writeBits(62,7,0,val);
		case AMACreg::WARN_EN_LV_LEFT_LO:	return AMAC_writeBits(63,7,0,val);
		case AMACreg::WARN_EN_LV_RIGHT_HI:	return AMAC_writeBits(64,7,0,val);
		case AMACreg::WARN_EN_LV_RIGHT_LO:	return AMAC_writeBits(65,7,0,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH0:	return AMAC_writeBits(66,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH1:	return AMAC_writeBits(67,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH2:	return AMAC_writeBits(68,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH3:	return AMAC_writeBits(69,10,6,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH4:	return AMAC_writeBits(71,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH5:	return AMAC_writeBits(72,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_HI_L_CH6:	return AMAC_writeBits(73,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH0:	return AMAC_writeBits(75,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH1:	return AMAC_writeBits(76,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH2:	return AMAC_writeBits(77,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH3:	return AMAC_writeBits(78,10,6,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH4:	return AMAC_writeBits(80,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH5:	return AMAC_writeBits(81,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_LO_L_CH6:	return AMAC_writeBits(82,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH0:	return AMAC_writeBits(84,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH1:	return AMAC_writeBits(85,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH2:	return AMAC_writeBits(86,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH3:	return AMAC_writeBits(87,10,6,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH4:	return AMAC_writeBits(89,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH5:	return AMAC_writeBits(90,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_HI_R_CH6:	return AMAC_writeBits(91,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH0:	return AMAC_writeBits(93,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH1:	return AMAC_writeBits(94,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH2:	return AMAC_writeBits(95,10,4,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH3:	return AMAC_writeBits(96,10,6,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH4:	return AMAC_writeBits(98,10,0,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH5:	return AMAC_writeBits(99,10,2,val);
		case AMACreg::ILOCK_HV_THRESH_LO_R_CH6:	return AMAC_writeBits(100,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH0:	return AMAC_writeBits(102,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH1:	return AMAC_writeBits(103,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH2:	return AMAC_writeBits(104,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH3:	return AMAC_writeBits(105,10,6,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH4:	return AMAC_writeBits(107,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH5:	return AMAC_writeBits(108,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_HI_L_CH6:	return AMAC_writeBits(109,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH0:	return AMAC_writeBits(111,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH1:	return AMAC_writeBits(112,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH2:	return AMAC_writeBits(113,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH3:	return AMAC_writeBits(114,10,6,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH4:	return AMAC_writeBits(116,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH5:	return AMAC_writeBits(117,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_LO_L_CH6:	return AMAC_writeBits(118,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH0:	return AMAC_writeBits(120,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH1:	return AMAC_writeBits(121,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH2:	return AMAC_writeBits(122,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH3:	return AMAC_writeBits(123,10,6,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH4:	return AMAC_writeBits(125,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH5:	return AMAC_writeBits(126,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_HI_R_CH6:	return AMAC_writeBits(127,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH0:	return AMAC_writeBits(129,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH1:	return AMAC_writeBits(130,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH2:	return AMAC_writeBits(131,10,4,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH3:	return AMAC_writeBits(132,10,6,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH4:	return AMAC_writeBits(134,10,0,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH5:	return AMAC_writeBits(135,10,2,val);
		case AMACreg::ILOCK_LV_THRESH_LO_R_CH6:	return AMAC_writeBits(136,10,4,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH0:	return AMAC_writeBits(138,10,0,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH1:	return AMAC_writeBits(139,10,2,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH2:	return AMAC_writeBits(140,10,4,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH3:	return AMAC_writeBits(141,10,6,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH4:	return AMAC_writeBits(143,10,0,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH5:	return AMAC_writeBits(144,10,2,val);
		case AMACreg::WARN_HV_THRESH_HI_L_CH6:	return AMAC_writeBits(145,10,4,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH0:	return AMAC_writeBits(147,10,0,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH1:	return AMAC_writeBits(148,10,2,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH2:	return AMAC_writeBits(149,10,4,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH3:	return AMAC_writeBits(150,10,6,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH4:	return AMAC_writeBits(152,10,0,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH5:	return AMAC_writeBits(153,10,2,val);
		case AMACreg::WARN_HV_THRESH_LO_L_CH6:	return AMAC_writeBits(154,10,4,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH0:	return AMAC_writeBits(156,10,0,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH1:	return AMAC_writeBits(157,10,2,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH2:	return AMAC_writeBits(158,10,4,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH3:	return AMAC_writeBits(159,10,6,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH4:	return AMAC_writeBits(161,10,0,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH5:	return AMAC_writeBits(162,10,2,val);
		case AMACreg::WARN_HV_THRESH_HI_R_CH6:	return AMAC_writeBits(163,10,4,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH0:	return AMAC_writeBits(165,10,0,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH1:	return AMAC_writeBits(166,10,2,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH2:	return AMAC_writeBits(167,10,4,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH3:	return AMAC_writeBits(168,10,6,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH4:	return AMAC_writeBits(170,10,0,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH5:	return AMAC_writeBits(171,10,2,val);
		case AMACreg::WARN_HV_THRESH_LO_R_CH6:	return AMAC_writeBits(172,10,4,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH0:	return AMAC_writeBits(174,10,0,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH1:	return AMAC_writeBits(175,10,2,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH2:	return AMAC_writeBits(176,10,4,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH3:	return AMAC_writeBits(177,10,6,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH4:	return AMAC_writeBits(179,10,0,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH5:	return AMAC_writeBits(180,10,2,val);
		case AMACreg::WARN_LV_THRESH_HI_L_CH6:	return AMAC_writeBits(181,10,4,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH0:	return AMAC_writeBits(183,10,0,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH1:	return AMAC_writeBits(184,10,2,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH2:	return AMAC_writeBits(185,10,4,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH3:	return AMAC_writeBits(186,10,6,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH4:	return AMAC_writeBits(188,10,0,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH5:	return AMAC_writeBits(189,10,2,val);
		case AMACreg::WARN_LV_THRESH_LO_L_CH6:	return AMAC_writeBits(190,10,4,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH0:	return AMAC_writeBits(192,10,0,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH1:	return AMAC_writeBits(193,10,2,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH2:	return AMAC_writeBits(194,10,4,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH3:	return AMAC_writeBits(195,10,6,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH4:	return AMAC_writeBits(197,10,0,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH5:	return AMAC_writeBits(198,10,2,val);
		case AMACreg::WARN_LV_THRESH_HI_R_CH6:	return AMAC_writeBits(199,10,4,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH0:	return AMAC_writeBits(201,10,0,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH1:	return AMAC_writeBits(202,10,2,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH2:	return AMAC_writeBits(203,10,4,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH3:	return AMAC_writeBits(204,10,6,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH4:	return AMAC_writeBits(206,10,0,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH5:	return AMAC_writeBits(207,10,2,val);
		case AMACreg::WARN_LV_THRESH_LO_R_CH6:	return AMAC_writeBits(208,10,4,val);
		default:
			return -2; break;
	}
}

int AMAC_TB::writeAMACreg(int reg, int val){
	if(I2C_write(reg,1,&val)) return -1;
	return 0;
}
