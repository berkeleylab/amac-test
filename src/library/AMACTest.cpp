#include <AMACTest.h>

using namespace std;

//////////////////////////////////////
// Control Functions
//////////////////////////////////////

AMACTest::AMACTest()
{
  TestName = "Test";
}

AMACTest::AMACTest(const std::string& name)
{
  TestName = name;
}

AMACTest::~AMACTest()
{
  TestBoard->PowerDown();
}

void AMACTest::setTestBoard(AMAC_TB* TB)
{
  TestBoard = TB;
}

void AMACTest::setErrorLog(bool log)
{
  log_errors = log;
}

void AMACTest::clearCriticalErrors()
{
  criticalErrors.clear();
}

void AMACTest::addCriticalErrors(vector<int> criticals)
{
  for(int i : criticals)
    if(find(criticalErrors.begin(), criticalErrors.end(), i) == criticalErrors.end()) 
      criticalErrors.push_back(i);
}

void AMACTest::addCriticalErrors(int range_start, int range_stop){
  for(int i = range_start; i <= range_stop; i++)
    if(find(criticalErrors.begin(), criticalErrors.end(), i) == criticalErrors.end()) 
      criticalErrors.push_back(i);
}

int AMACTest::Init()
{
#ifdef VERBOSE
  cout << "[" << TestName << "] Init" << endl;
#endif
  //If no TestBoard has been specified, connect to default TestBoard on serial 0
  if(!TestBoard)
    TestBoard = new AMAC_TB();
  if(!TestBoard->isConnected()) return -1;

  while(TestBoard->PowerUp()) setSWError(1);
  while(TestBoard->I2C_enable()) setSWError(2);

#ifdef VERBOSE
  cout << "[" << TestName << "] TestBoard Up and Running" << endl;
#endif
  return 0;
}

int AMACTest::getErrors(vector<int> &_errors)
{
  _errors = Errors;
  return 0;
}

int AMACTest::getSWErrors(vector<int> &_errors)
{
  _errors = SWErrors;
  return 0;
}

int AMACTest::evaluateTest(bool &passed, int &num_Errors, int &num_Criticals){
	passed = true;
	num_Errors = 0;
	num_Criticals = 0;
	
	for(int i : Errors){
		num_Errors++;
		if(find(criticalErrors.begin(), criticalErrors.end(), i) != criticalErrors.end()) {
			num_Criticals++;
			passed = false;
		}
	}

	return 0;
}

//////////////////////////////////////
// Calibrate Functions
//////////////////////////////////////

void AMACTest::calib_ADC_ext(const std::string& name, bool log_append,
			     double start_val, double stop_val, double Stepsize, int Step_delay_ms,
			     int DAC_ch, AMACreg ADC_ch)
{
  //Logfile
  std::string logpath = "log/" + TestName + "_calib_ADC_V_" + name + ".log";
  std::fstream logfile;
  if(log_append) 	logfile.open(logpath, std::fstream::out | std::fstream::app);
  else {
    logfile.open(logpath, std::fstream::out);
    logfile << "InputVoltage BandgapControl RampGain ADCvalue" << std::endl;
  }

#ifdef DEBUG
  cout << "--Calibrate ADC: " << start_val << " to " << stop_val << ", steps of " << Stepsize << endl;
#endif

  // Reusable variables
  double actual_val, target_val;
  int result;

  // Loop over Bandgap control settings
  for(int BGC = 0; BGC < 16; BGC++)
    {
      TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, BGC);
      // Loop over RampGain settings
      for(int RG = 0; RG < 4; RG++)
	{
	  TestBoard->writeAMAC(AMACreg::RIGHT_RAMP_GAIN, RG);
	  TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN , RG);

	  //
	  // Loop over input voltage levels
	  for(target_val = start_val; target_val <= stop_val; target_val += Stepsize)
	    {
	      if(TestBoard->setDACvoltage(DAC_ch, target_val)) setSWError(1001);
	      if(TestBoard->getDACvoltage(DAC_ch, actual_val)) setSWError(1002);
	      usleep(Step_delay_ms*1000);

	      // Read the AMAC ADC value
	      if(TestBoard->readAMAC(ADC_ch, result)) setSWError(1003);
	      logfile << actual_val << " " << BGC << " " << RG << " " << result << std::endl;
#ifdef DEBUG
	      std::cout << "----Voltage: " << actual_val << ",\tBandgap Control: " << BGC << ",\tRampGain: " << RG << ",\tADC: " << result << std::endl;
#endif
	    }
	}
    }
}

void AMACTest::noise_ADC_ext(const std::string& name, bool log_append,
			     double target_voltage, uint nReads,
			     int BGC, int RG,
			     int DAC_ch, AMACreg ADC_ch)
{
  //Logfile
  std::string logpath = "log/" + TestName + "_noise_ADC_V_" + name + ".log";
  std::fstream logfile;
  if(log_append) 	logfile.open(logpath, std::fstream::out | std::fstream::app);
  else {
    logfile.open(logpath, std::fstream::out);
    logfile << "InputVoltage BandgapControl RampGain ADCvalue" << std::endl;
  }

  // Reusable variables
  double actual_val;
  int result;

  TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, BGC);
  TestBoard->writeAMAC(AMACreg::RIGHT_RAMP_GAIN, RG);
  TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN , RG);

  if(TestBoard->setDACvoltage(DAC_ch, target_voltage)) setSWError(1001);
  if(TestBoard->getDACvoltage(DAC_ch, actual_val)) setSWError(1002);

  for(uint iRead=0;iRead<nReads;iRead++)
    {
      // Read the AMAC ADC value
      if(TestBoard->readAMAC(ADC_ch, result)) setSWError(1003);
      logfile << actual_val << " " << BGC << " " << RG << " " << result << std::endl;
#ifdef DEBUG
      std::cout << "----Voltage: " << actual_val << ",\tBandgap Control: " << BGC << ",\tRampGain: " << RG << ",\tADC: " << result << std::endl;
#endif
    }
}

void AMACTest::calib_ADC_int(const std::string& name, bool log_append,
			     int DAC_ch, AMACreg ADC_ch)
{
  //Logfile
  std::string logpath = "log/" + TestName + "_calib_ADC_V_" + name + ".log";
  std::fstream logfile;
  if(log_append) logfile.open(logpath, std::fstream::out | std::fstream::app);
  else {
    logfile.open(logpath, std::fstream::out);
    logfile << "InputVoltage BandgapControl RampGain ADCvalue" << std::endl;
  }

  // Reusable variables
  double readout_val;
  int result;

  // Loop over Bandgap control settings
  for(int BGC = 0; BGC < 16; BGC++)
    {
      TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, BGC);
      // Loop over RampGain settings
      for(int RG = 0; RG < 4; RG++)
	{
	  TestBoard->writeAMAC(AMACreg::RIGHT_RAMP_GAIN, RG);
	  TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN , RG);

	  //
	  // Read the external ADC measurement
	  if(TestBoard->getADCvoltage(DAC_ch, readout_val)) setSWError(1001);

	  // Read the AMAC ADC value
	  if(TestBoard->readAMAC(ADC_ch, result)) setSWError(1002);
	  logfile << readout_val << " " << BGC << " " << RG << " " << result << std::endl;
#ifdef DEBUG
	  std::cout << "----Voltage: " << readout_val << ",\tBandgap Control: " << BGC << ",\tRampGain: " << RG << ",\tADC: " << result << std::endl;
#endif
	}
    }
}

//////////////////////////////////////
// Test Functions
//////////////////////////////////////

void AMACTest::test_ADC_ext(const std::string& name, bool log_append, const std::string& limits_file, 
			    double start_val, double stop_val, double Stepsize, int Step_delay_ms,
			    int DAC_ch, AMACreg readout_ch)
{
  //Logfile
  string logpath = "log/" + TestName + "_ADC_V_" + name + ".log";
  fstream logfile;
  if(log_append) 	logfile.open(logpath, fstream::out | fstream::app);
  else {
    logfile.open(logpath, fstream::out);
    logfile << "#InputVoltage[V] ADCreg_value" << endl;
  }

#ifdef DEBUG
  cout << "--Test ADC: " << start_val << " to " << stop_val << ", steps of " << Stepsize << endl;
#endif

  double actual_val, target_val;
  int result;
  double result_min=0, result_max=0;
  for(target_val = start_val; target_val <= stop_val; target_val += Stepsize)
    {
      if(TestBoard->setDACvoltage(DAC_ch, target_val)) setSWError(1001);
      if(TestBoard->getDACvoltage(DAC_ch, actual_val)) setSWError(1002);
      usleep(Step_delay_ms*1000);
      if(TestBoard->readAMAC(readout_ch, result)) setSWError(1003);
      logfile << actual_val << " " << result << endl;
      if(limits_file != "")
	{
	  if(getLimits(limits_file, actual_val, result_min, result_max)) setSWError(1004);
	  if(result < result_min) setError(1001);
	  if(result > result_max) setError(1002);
	}
#ifdef DEBUG
      cout << "----Voltage: " << actual_val << ",	read: " << result << ",	limits " << result_min << "..." << result_max << endl;
#endif
    }
}

void AMACTest::test_LVCTRL(const std::string& name)
{
  int LVCTRL, LVCTRLB;

  //Logfile
  string logpath = "log/" + TestName + "_LVCTRL_" + name + ".log";
  fstream logfile(logpath, fstream::out);

  // LV enabled
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 1)) setSWError(2001);
  if(TestBoard->readAMAC(AMACreg::LV_ENABLE, LVCTRL)) setSWError(2002);
  if(LVCTRL != 1) setError(2001);
  logfile << "Write_1_to_enable__readback: " << LVCTRL << endl;
  if(TestBoard->get_AMACout_LVCTRL(LVCTRL, LVCTRLB)) setSWError(2003);
  if(LVCTRL != 1) setError(2002);
  if(LVCTRLB != 0) setError(2003);
  logfile << "Write_1_to_enable__LVCTRL: " << LVCTRL << endl;
  logfile << "Write_1_to_enable__LVCTRLB: " << LVCTRLB << endl;

  // LV disabled
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 0)) setSWError(2004);
  if(TestBoard->readAMAC(AMACreg::LV_ENABLE, LVCTRL)) setSWError(2005);
  if(LVCTRL != 0) setError(2004);
  logfile << "Write_0_to_enable__readback: " << LVCTRL << endl;
  if(TestBoard->get_AMACout_LVCTRL(LVCTRL, LVCTRLB)) setSWError(2006);
  if(LVCTRL != 0) setError(2005);
  if(LVCTRLB != 1) setError(2006);
  logfile << "Write_0_to_enable__LVCTRL: " << LVCTRL << endl;
  logfile << "Write_0_to_enable__LVCTRLB: " << LVCTRLB << endl;
}

void AMACTest::calib_current_measurement(const std::string& name, bool log_append,
					 double start_val, double stop_val, double Stepsize, int Step_delay_ms, int rchannel,
					 bool left_channel)
{
  //Logfile
  std::string logpath = "log/" + TestName + "_calib_ADC_I_" + name + ".log";
  std::fstream logfile;
  if(log_append) 	logfile.open(logpath, std::fstream::out | std::fstream::app);
  else 
    {
      logfile.open(logpath, std::fstream::out);
      logfile << "InputCurrent BandgapControl RampGain OpAmpGain ResistorIdx ADCvalue" << std::endl;
    }

  //Evaluate channel
  AMACreg readout_ch = (left_channel)?AMACreg::VALUE_LEFT_CH6:AMACreg::VALUE_RIGHT_CH6;

  TestBoard->enableCurrent(true);

  // Reusable variables
  double target_val, actual_val;
  int result;

  // Loop over Bandgap control settings
  for(int BGC = 0; BGC < 16; BGC++)
    {
      TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, BGC);
      // Loop over RampGain settings
      for(int RG = 0; RG < 4; RG++)
	{
	  if(left_channel) TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN , RG);
	  else             TestBoard->writeAMAC(AMACreg::RIGHT_RAMP_GAIN, RG);

	  // Loop over OpAmp Gain settings
	  for(int OA = 0; OA < 16; OA++)
	    {
	      if(left_channel) TestBoard->writeAMAC(AMACreg::OPAMP_GAIN_LEFT , OA);
	      else             TestBoard->writeAMAC(AMACreg::OPAMP_GAIN_RIGHT, OA);

	      //
	      // Loop over input currents
	      for(target_val = start_val; target_val <= stop_val; target_val += Stepsize)
		{
		  //target_val=0.001;
		  if(TestBoard->setCurrent(left_channel, rchannel, target_val)) setSWError(3001);
		  if(TestBoard->getCurrent(left_channel, actual_val)) setSWError(3002);
		  usleep(Step_delay_ms * 1000);

		  // Read the AMAC ADC value
		  if(TestBoard->readAMAC(readout_ch, result)) setSWError(3003);
		  logfile << actual_val << " " << BGC << " " << RG << " " << OA << " " << rchannel << " " << result << std::endl;
#ifdef DEBUG
		  std::cout << "----Current: " << actual_val << ",\tBandgap Control: " << BGC << ",\tRampGain: " << RG << ",\tOpAmpGain: " << OA << ",\tRchannel: " << rchannel << ",\tADC: " << result << std::endl;
#endif
		}
	    }
	}
    }

  TestBoard->enableCurrent(false);
}

void AMACTest::test_current_measurement(const std::string& name, bool log_append, const std::string& limits_file,
					double start_val, double stop_val, double Stepsize, int Step_delay_ms,
					bool left_channel)
{
  //Logfile
  string logpath = "log/" + TestName + "_ADC_I_" + name + ".log";
  fstream logfile;
  if(log_append) 	logfile.open(logpath, fstream::out | fstream::app);
  else 
    {
      logfile.open(logpath, fstream::out);
      logfile << "#InputCurrent[A] ADCreg_value" << endl;
    }

  //Evaluate channel
  AMACreg readout_ch = AMACreg::VALUE_RIGHT_CH6; if(left_channel) readout_ch = AMACreg::VALUE_LEFT_CH6;

  TestBoard->enableCurrent(true);

  double target_val, actual_val;
  int result;
  double result_min=0, result_max=0;
  for(target_val = start_val; target_val <= stop_val; target_val += Stepsize)
    {
      if(TestBoard->setCurrent(left_channel, target_val)) setSWError(3001);
      if(TestBoard->getCurrent(left_channel, actual_val)) setSWError(3002);
      usleep(Step_delay_ms * 1000);
      if(TestBoard->readAMAC(readout_ch, result)) setSWError(3003);
      logfile << actual_val << " " << result << endl;
      if(limits_file != "")
	{
	  if(getLimits(limits_file, actual_val, result_min, result_max)) setSWError(3004);
	  if(result < result_min) setError(3001);
	  if(result > result_max) setError(3002);
	}
#ifdef DEBUG
      std::cout << "----Current: " << actual_val << ",	read: " << result << ",	limits " << result_min << "..." << result_max << std::endl;
#endif
    }

  TestBoard->enableCurrent(false);
}

void AMACTest::test_I2C_reliability(const std::string& name, bool log_append, const std::string& limits_file, 
				    double start_val, double stop_val, double Stepsize, 
				    int num_attempts_per_val)
{
  //Logfile
  string logpath = "log/" + TestName + "_I2C_" + name + ".log";
  fstream logfile;
  if(log_append) 	logfile.open(logpath, fstream::out | fstream::app);
  else
    {
      logfile.open(logpath, fstream::out);
      logfile << "VCC_H I2C_SuccessRate" << endl;
    }

  //Initialize Randomizer
  srand (time(NULL));

  double target_val, actual_val;
  double result, result_min=0, result_max=0;
  for(target_val = start_val; target_val <= stop_val; target_val += Stepsize)
    {
      if(TestBoard->setReset(true)) setSWError(4000);
      if(TestBoard->setVCC_IO(target_val)) setSWError(4001);
      if(TestBoard->getVCC_IO(actual_val)) setSWError(4002);
      if(TestBoard->setReset(false)) setSWError(4003);
      usleep(100000);
    
      int error_ctr = 0;
      for(int j = 0; j < num_attempts_per_val; j++)
	{
	  int val_to_write = rand()%256;
	  int* val_received = 0;
	  //Write to any RW register
	  if(TestBoard->I2C_write(66, 1, &val_to_write))
	    error_ctr++;
	  else
	    {
	      if(TestBoard->I2C_read(66,1,val_received))
		error_ctr++;
	      else if(val_to_write != *val_received)
		error_ctr++;
	    }
	}
      result = 1.0 - (double)error_ctr / num_attempts_per_val;

      logfile << actual_val << " " << result << endl;
      if(limits_file != "")
	{
	  if(getLimits(limits_file, actual_val, result_min, result_max)) setSWError(4004);
	  if(result < result_min) setError(4001);
	  if(result > result_max) setError(4002);
	}
#ifdef DEBUG
      cout << "----VCC_H: " << actual_val << ",	Success rate: " << result << ",	limits " << result_min << "..." << result_max << endl;
#endif
    }
}

void AMACTest::test_clock_and_HVCTRL(const std::string& name, const std::string& limits_file, 
				     int clock_eval_timespan)
{
  int freq, dutycycle;
  int fmin, fmax, dmin, dmax;

  //Logfile
  std::string logpath = "log/" + TestName + "_CLK_" + name + ".log";
  fstream logfile(logpath, fstream::out);	

  //ClkOut disabled
  if(TestBoard->writeAMAC(AMACreg::CLK_OUT_ENABLE, 0)) setSWError(5001);	
  if(TestBoard->writeAMAC(AMACreg::CLK_SELECT, 0)) setSWError(5002);		
  if(TestBoard->ClkOut_analysis(CLK_CHANNEL_CLKOUT, clock_eval_timespan, freq, dutycycle)) setSWError(5003);	
  logfile << "ClkOut_disabled: " << freq << " " << dutycycle << std::endl;
  if(freq > 10) setError(5001);
  if(dutycycle > 1) setError(5002);
#ifdef VERBOSE
  cout << "----Clock: Disabled - ClkOut Frequency: " << freq << "(0...10)		Duty Cycle: " << dutycycle << "(0...1)"<< endl;
#endif

  //ClkOut enabled - Internal Oscillator
  if(TestBoard->writeAMAC(AMACreg::CLK_OUT_ENABLE, 1)) setSWError(5011);	
  if(TestBoard->ClkOut_analysis(CLK_CHANNEL_CLKOUT, clock_eval_timespan, freq, dutycycle)) setSWError(5012);
  if(limits_file != "")
    {
      if(getLimitsByIndex(limits_file, 1, fmin, fmax)) setSWError(5013);
      if(freq < fmin) setError(5011);
      if(freq > fmax) setError(5012);
      if(getLimitsByIndex(limits_file, 2, dmin, dmax)) setSWError(5014);
      if(dutycycle < dmin) setError(5013);
      if(dutycycle > dmax) setError(5014);
    }	
  logfile << "Internal_Oscillator: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
  cout << "----Clock: Internal - ClkOut Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif

  //External source - 10kHz
  if(TestBoard->writeAMAC(AMACreg::CLK_SELECT, 1)) setSWError(5021);		
  if(TestBoard->set_ClkIn(10000)) setSWError(5022);	
  usleep(1000);
  if(TestBoard->ClkOut_analysis(CLK_CHANNEL_CLKOUT, clock_eval_timespan, freq, dutycycle)) setSWError(5023);
  if(limits_file != "")
    {
      if(getLimitsByIndex(limits_file, 3, fmin, fmax)) setSWError(5024);
      if(freq < fmin) setError(5021);
      if(freq > fmax) setError(5022);
      if(getLimitsByIndex(limits_file, 4, dmin, dmax)) setSWError(5025);
      if(dutycycle < dmin) setError(5023);
      if(dutycycle > dmax) setError(5024);	
    }
  logfile << "External_Oscillator_10k: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
  cout << "----Clock: External 10kHz - ClkOut Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif

  //External source - 1MHz
  if(TestBoard->writeAMAC(AMACreg::CLK_SELECT, 1)) setSWError(5031);		
  if(TestBoard->set_ClkIn(1000000)) setSWError(5032);	
  usleep(1000);
  if(TestBoard->ClkOut_analysis(CLK_CHANNEL_CLKOUT, clock_eval_timespan, freq, dutycycle)) setSWError(5033);
  if(limits_file != "")
    {
      if(getLimitsByIndex(limits_file, 5, fmin, fmax)) setSWError(5034);
      if(freq < fmin) setError(5031);
      if(freq > fmax) setError(5032);
      if(getLimitsByIndex(limits_file, 6, dmin, dmax)) setSWError(5035);
      if(dutycycle < dmin) setError(5033);
      if(dutycycle > dmax) setError(5034);	
    }
  logfile << "External_Oscillator_1M: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
  cout << "----Clock: External 1MHz - ClkOut Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif

  //External source - 50MHz
  if(TestBoard->writeAMAC(AMACreg::CLK_SELECT, 1)) setSWError(5041);		
  if(TestBoard->set_ClkIn(50000000)) setSWError(5042);	
  usleep(1000);
  if(TestBoard->ClkOut_analysis(CLK_CHANNEL_CLKOUT, clock_eval_timespan, freq, dutycycle)) setSWError(5043);	
  if(limits_file != "")
    {
      if(getLimitsByIndex(limits_file, 7, fmin, fmax)) setSWError(5044);
      if(freq < fmin) setError(5041);
      if(freq > fmax) setError(5042);
      if(getLimitsByIndex(limits_file, 8, dmin, dmax)) setSWError(5045);
      if(dutycycle < dmin) setError(5043);
      if(dutycycle > dmax) setError(5044);
    }	
  logfile << "External_Oscillator_50M: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
  std::cout << "----Clock: External 50MHz - ClkOut Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif

  //Test Divider
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 1)) setSWError(5051);
  if(TestBoard->set_ClkIn(10000000)) setSWError(5052);	
  usleep(1000);
  for(int i = 0; i < 4; i++)
    {
      if(TestBoard->writeAMAC(AMACreg::HV_FREQ, i)) setSWError(5053 + i);	
      if(TestBoard->ClkOut_analysis(CLK_CHANNEL_HVCNTRL, clock_eval_timespan, freq, dutycycle)) setSWError(5057 + i);
      if(limits_file != "")
	{
	  if(getLimitsByIndex(limits_file, 9+i, fmin, fmax)) setSWError(5061+i);
	  if(freq < fmin) setError(5051+i);
	  if(freq > fmax) setError(5055+i);
	  if(getLimitsByIndex(limits_file, 13+i, dmin, dmax)) setSWError(5065+i);
	  if(dutycycle < dmin) setError(5059+i);
	  if(dutycycle > dmax) setError(5063+i);	
	}
      logfile << "External_Oscillator_10M_HVenabled_div" << i << "_HVCTRL: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
      cout << "----Clock: External 10MHz, Divider " << i << ", HV enabled - HVCTRL Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif
      if(TestBoard->ClkOut_analysis(CLK_CHANNEL_HVCNTRLB, clock_eval_timespan, freq, dutycycle)) setSWError(5069 + i);
      if(limits_file != "")
	{
	  if(getLimitsByIndex(limits_file, 17+i, fmin, fmax)) setSWError(5073+i);
	  if(freq < fmin) setError(5067+i);
	  if(freq > fmax) setError(5071+i);
	  if(getLimitsByIndex(limits_file, 21+i, dmin, dmax)) setSWError(5077+i);
	  if(dutycycle < dmin) setError(5075+i);
	  if(dutycycle > dmax) setError(5079+i);	
	}
      logfile << "External_Oscillator_10M_HVenabled_div" << i << "_HVCTRLB: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
      cout << "----Clock: External 10MHz, Divider " << i << ", HV enabled - HVCTRLB Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif
    }
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 0)) setSWError(5081);
  usleep(1000);
  for(int i = 0; i < 4; i++)
    {
      if(TestBoard->writeAMAC(AMACreg::HV_FREQ, i)) setSWError(5083 + i);	
      if(TestBoard->ClkOut_analysis(CLK_CHANNEL_HVCNTRL, clock_eval_timespan, freq, dutycycle)) setSWError(5087 + i);
      if(limits_file != "")
	{
	  if(getLimitsByIndex(limits_file, 17+i, fmin, fmax)) setSWError(5091+i);
	  if(freq < fmin) setError(5083+i);
	  if(freq > fmax) setError(5087+i);
	  if(getLimitsByIndex(limits_file, 21+i, dmin, dmax)) setSWError(5095+i);
	  if(dutycycle < dmin) setError(5091+i);
	  if(dutycycle > dmax) setError(5095+i);	
	}
      logfile << "External_Oscillator_10M_HVdisabled_div" << i << "_HVCTRL: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
      cout << "----Clock: External 10MHz, Divider " << i << ", HV disabled - HVCTRL Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif
      if(TestBoard->ClkOut_analysis(CLK_CHANNEL_HVCNTRLB, clock_eval_timespan, freq, dutycycle)) setSWError(5099 + i);
      if(limits_file != "")
	{
	  if(getLimitsByIndex(limits_file, 9+i, fmin, fmax)) setSWError(5103+i);
	  if(freq < fmin) setError(5099+i);
	  if(freq > fmax) setError(5103+i);
	  if(getLimitsByIndex(limits_file, 13+i, dmin, dmax)) setSWError(5107+i);
	  if(dutycycle < dmin) setError(5107+i);
	  if(dutycycle > dmax) setError(5111+i);	
	}
      logfile << "External_Oscillator_10M_HVdisabled_div" << i << "_HVCTRLB: " << freq << " " << dutycycle << endl;
#ifdef VERBOSE
      cout << "----Clock: External 10MHz, Divider " << i << ", HV disabled - HVCTRLB Frequency: " << freq << "(" << fmin << "..."<< fmax << ")		Duty Cycle: " << dutycycle << "(" << dmin << "..."<< dmax << ")"<< endl;
#endif
    }
}

void AMACTest::test_ILock(const std::string& name,
			  int channel)
{
  int value, trash, threshold_under, threshold_over;
  bool isOn;

  //Logfile
  std::string logpath = "log/" + TestName + "_ILOCK_noDAC_" + name + ".log";
  fstream logfile(logpath, fstream::out);

  //Get Threshold values
  if(TestBoard->readAMAC(ADC_VALUE[channel], value)) setSWError(6001);
  threshold_under = 0;
  threshold_over = 0x3FF;
  logfile << "ADC_channel_value: " << value << endl;

  //Enable ILock Flags to test
  if(TestBoard->writeAMAC(ILOCK_EN_HV_HI[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6002);
  if(TestBoard->writeAMAC(ILOCK_EN_HV_LO[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6003);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_HI[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6004);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_LO[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6005);
  //Check if no ILocks present
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6006);
  logfile << "ILOCK_Flag_HV_HI_reset_val: " << value << endl;
  if(value) setError(6006);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6007);
  logfile << "ILOCK_Flag_HV_LO_reset_val: " << value << endl;
  if(value) setError(6007);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6008);
  logfile << "ILOCK_Flag_LV_HI_reset_val: " << value << endl;
  if(value) setError(6008);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6009);
  logfile << "ILOCK_Flag_LV_LO_reset_val: " << value << endl;
  if(value) setError(6009);

  //Read pulse ctr once (so ring counter is tared)
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6010);

  //Check HV H Threshold
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], threshold_under)) setSWError(6011);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6012);
  if(value != ILOCK_FLAGS(channel)) setError(6011);
  logfile << "ILOCK_Flag_HV_HI_triggered_val: " << value << endl;

  //Check HV L Threshold
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_LO[channel], threshold_over)) setSWError(6013);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6014);
  if(value != ILOCK_FLAGS(channel)) setError(6014);
  logfile << "ILOCK_Flag_HV_LO_triggered_val: " << value << endl;

  //Check LV H Threshold
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], threshold_under)) setSWError(6015);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6016);
  if(value != ILOCK_FLAGS(channel)) setError(6016);
  logfile << "ILOCK_Flag_LV_HI_triggered_val: " << value << endl;

  //Check LV L Threshold
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_LO[channel], threshold_over)) setSWError(6017);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6018);
  if(value != ILOCK_FLAGS(channel)) setError(6018);
  logfile << "ILOCK_Flag_LV_LO_triggered_val: " << value << endl;

  //Set Thresholds back to non-critical values and reset flags
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], threshold_over )) setSWError(6021);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_LO[channel], threshold_under)) setSWError(6022);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], threshold_over )) setSWError(6023);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_LO[channel], threshold_under)) setSWError(6024);
  if(TestBoard->writeAMAC(AMACreg::RESET_HV_ILOCK, 1)) setSWError(6025);
  if(TestBoard->writeAMAC(AMACreg::RESET_LV_ILOCK, 1)) setSWError(6026);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6027);
  if(value != 0) setError(6027);
  logfile << "ILOCK_Flag_HV_HI_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6028);
  if(value != 0) setError(6028);
  logfile << "ILOCK_Flag_HV_LO_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6029);
  if(value != 0) setError(6029);
  logfile << "ILOCK_Flag_LV_HI_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6030);
  if(value != 0) setError(6030);
  logfile << "ILOCK_Flag_LV_LO_SWreset_val: " << value << endl;

  //Check Output Disable
  //	Thresholds to critical value
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], threshold_under)) setSWError(6031);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], threshold_under)) setSWError(6032);
  //	Outputs enable
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 1)) setSWError(6033);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 1)) setSWError(6034);
  //	Outputs should be on
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6035);
  if(!isOn) setError(6035);
  logfile << "ILOCK_disabled_Output_enabled_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6036);
  if(!isOn) setError(6036);
  logfile << "ILOCK_disabled_Output_enabled_Output_val_LV: " << (int)isOn << endl;
  //	No LAM pulses expected yet
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6037);
  if(value) setError(6037);
  logfile << "ILOCK_disabled_Output_enabled_No_LAM_pulses: " << value << endl;
  //	Enable LAM outputs
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK_LAM, 1)) setSWError(6041);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK_LAM, 1)) setSWError(6042);
  //	Enable ILocks
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK, 1)) setSWError(6043);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK, 1)) setSWError(6044);
  //	Outputs should be off
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6045);
  if(isOn) setError(6045);
  logfile << "ILOCK_enabled_Output_enabled_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6046);
  if(isOn) setError(6046);
  logfile << "ILOCK_enabled_Output_enabled_Output_val_LV: " << (int)isOn << endl;
  //	LAM pulse should have appeared
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6047);
  if(value != 1) setError(6047);
  logfile << "ILOCK_enabled_Output_enabled_LAM_pulses: " << value << endl;

  //Check ILock Reset
  //	Disable ILocks
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK, 0)) setSWError(6051);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK, 0)) setSWError(6052);
  //	Disable LAM outputs
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK_LAM, 0)) setSWError(6041);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK_LAM, 0)) setSWError(6042);
  //	Outputs Reset
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 0)) setSWError(6053);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 0)) setSWError(6054);
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 1)) setSWError(6055);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 1)) setSWError(6056);
  //	Outputs should be back on
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6057);
  if(!isOn) setError(6057);
  logfile << "ILOCK_disabled_Output_reset_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6058);
  if(!isOn) setError(6058);
  logfile << "ILOCK_disabled_Output_reset_Output_val_LV: " << (int)isOn << endl;

  //Set Tresholds to non-critical values
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], 0x7FF)) setSWError(6060);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_LO[channel],   0x0)) setSWError(6061);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], 0x7FF)) setSWError(6062);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_LO[channel],   0x0)) setSWError(6063);

  //Disable ILock Flags
  if(TestBoard->writeAMAC(ILOCK_EN_HV_HI[isLeftChannel_int(channel)], 0)) setSWError(6064);
  if(TestBoard->writeAMAC(ILOCK_EN_HV_LO[isLeftChannel_int(channel)], 0)) setSWError(6065);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_HI[isLeftChannel_int(channel)], 0)) setSWError(6066);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_LO[isLeftChannel_int(channel)], 0)) setSWError(6067);

  //Reset
  if(TestBoard->writeAMAC(AMACreg::RESET_HV_ILOCK, 1)) setSWError(6068);
  if(TestBoard->writeAMAC(AMACreg::RESET_LV_ILOCK, 1)) setSWError(6069);
}

void AMACTest::test_ILock(const std::string& name,
			  int channel, int dac,
			  double minDAC, double maxDAC)
{
  int value, trash, threshold_max, threshold_min;
  int minvalue, maxvalue;
  bool isOn;

  double avgDAC=(maxDAC+minDAC)/2;

  //Logfile
  std::string logpath = "log/" + TestName + "_ILOCK_DAC_" + name + ".log";
  fstream logfile(logpath, fstream::out);

  //
  // Quick calibration
  if(TestBoard->setDACvoltage(dac, minDAC)) setSWError(6001);
  if(TestBoard->readAMAC(ADC_VALUE[channel], minvalue)) setSWError(6002);

  if(TestBoard->setDACvoltage(dac, maxDAC)) setSWError(6003);
  if(TestBoard->readAMAC(ADC_VALUE[channel], maxvalue)) setSWError(6004);

  int avgvalue=(maxvalue+minvalue)/2;
  int adcrange=maxvalue-minvalue;

  threshold_max = avgvalue+adcrange/4;
  threshold_min = avgvalue-adcrange/4;
  logfile << "ADC_channel_settings (min, minthr, avg, maxthr, max): " << minvalue << " " << threshold_min << " " << avgvalue << " " << threshold_max << " " << maxvalue << std::endl;

  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6005);
  if(TestBoard->readAMAC(ADC_VALUE[channel], value)) setSWError(6006);
  logfile << "ADC_channel_value: " << value << std::endl;

  // Set Tresholds to critical values
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], threshold_max)) setSWError(6007);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_LO[channel], threshold_min)) setSWError(6008);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], threshold_max)) setSWError(6009);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_LO[channel], threshold_min)) setSWError(6010);

  //Enable ILock Flags to test
  if(TestBoard->writeAMAC(ILOCK_EN_HV_HI[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6011);
  if(TestBoard->writeAMAC(ILOCK_EN_HV_LO[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6012);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_HI[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6013);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_LO[isLeftChannel_int(channel)], ILOCK_FLAGS(channel))) setSWError(6014);
  //Check if no ILocks present
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6015);
  logfile << "ILOCK_Flag_HV_HI_reset_val: " << value << endl;
  if(value) setError(6015);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6016);
  logfile << "ILOCK_Flag_HV_LO_reset_val: " << value << endl;
  if(value) setError(6016);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6017);
  logfile << "ILOCK_Flag_LV_HI_reset_val: " << value << endl;
  if(value) setError(6017);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6018);
  logfile << "ILOCK_Flag_LV_LO_reset_val: " << value << endl;
  if(value) setError(6018);

  //Read pulse ctr once (so ring counter is tared)
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6019);

  //
  // Check if threshold flags work
  // The generic procedure is
  // 1) Drive the DAC to maxDAC value (should trigger ilock)
  // 2) Drive the DAC to avgDAC value (should be OK)
  // 3) Check ilock flag

  //Check HV H Threshold
  if(TestBoard->setDACvoltage(dac, maxDAC)) setSWError(6020);
  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6021);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6022);
  if(value != ILOCK_FLAGS(channel)) setError(6020);
  logfile << "ILOCK_Flag_HV_HI_triggered_val: " << value << endl;

  //Check HV L Threshold
  if(TestBoard->setDACvoltage(dac, minDAC)) setSWError(6023);
  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6024);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6025);
  if(value != ILOCK_FLAGS(channel)) setError(6023);
  logfile << "ILOCK_Flag_HV_LO_triggered_val: " << value << endl;

  //Check LV H Threshold
  if(TestBoard->setDACvoltage(dac, maxDAC)) setSWError(6026);
  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6027);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6028);
  if(value != ILOCK_FLAGS(channel)) setError(6029);
  logfile << "ILOCK_Flag_LV_HI_triggered_val: " << value << endl;

  //Check LV L Threshold
  if(TestBoard->setDACvoltage(dac, minDAC)) setSWError(6029);
  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6030);
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6031);
  if(value != ILOCK_FLAGS(channel)) setError(6029);
  logfile << "ILOCK_Flag_LV_LO_triggered_val: " << value << endl;

  //Reset flags
  if(TestBoard->writeAMAC(AMACreg::RESET_HV_ILOCK, 1)) setSWError(6032);
  if(TestBoard->writeAMAC(AMACreg::RESET_LV_ILOCK, 1)) setSWError(6033);
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_HI[isLeftChannel_int(channel)], value)) setSWError(6034);
  if(value != 0) setError(6034);
  logfile << "ILOCK_Flag_HV_HI_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_HV_LO[isLeftChannel_int(channel)], value)) setSWError(6035);
  if(value != 0) setError(6035);
  logfile << "ILOCK_Flag_HV_LO_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_HI[isLeftChannel_int(channel)], value)) setSWError(6036);
  if(value != 0) setError(6036);
  logfile << "ILOCK_Flag_LV_HI_SWreset_val: " << value << endl;
  if(TestBoard->readAMAC(ILOCK_FLAG_LV_LO[isLeftChannel_int(channel)], value)) setSWError(6037);
  if(value != 0) setError(6037);
  logfile << "ILOCK_Flag_LV_LO_SWreset_val: " << value << endl;

  //Check Output Disable on high spike
  //	Outputs enable
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 1)) setSWError(6038);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 1)) setSWError(6039);
  //	Outputs should be on
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6040);
  if(!isOn) setError(6040);
  logfile << "ILOCK_disabled_Output_enabled_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6041);
  if(!isOn) setError(6041);
  logfile << "ILOCK_disabled_Output_enabled_Output_val_LV: " << (int)isOn << endl;
  //	No LAM pulses expected yet
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6042);
  if(value) setError(6042);
  logfile << "ILOCK_disabled_Output_enabled_No_LAM_pulses: " << value << endl;
  //	Enable LAM outputs
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK_LAM, 1)) setSWError(6043);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK_LAM, 1)) setSWError(6044);
  //	Enable ILocks
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK, 1)) setSWError(6045);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK, 1)) setSWError(6046);
  //    Pulse the DAC
  if(TestBoard->setDACvoltage(dac, maxDAC)) setSWError(6047);
  if(TestBoard->setDACvoltage(dac, avgDAC)) setSWError(6048);
  //	Outputs should be off
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6049);
  if(isOn) setError(6049);
  logfile << "ILOCK_enabled_Output_enabled_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6050);
  if(isOn) setError(6050);
  logfile << "ILOCK_enabled_Output_enabled_Output_val_LV: " << (int)isOn << endl;
  //	LAM pulse should have appeared
  if(TestBoard->get_AMACout_LAM_pulses(value, trash)) setSWError(6051);
  if(value != 1) setError(6051);
  logfile << "ILOCK_enabled_Output_enabled_No_LAM_pulses: " << value << endl;

  //Check ILock Reset
  //	Disable ILocks
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK, 0)) setSWError(6052);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK, 0)) setSWError(6053);
  //	Disable LAM outputs
  if(TestBoard->writeAMAC(AMACreg::HV_ILOCK_LAM, 0)) setSWError(6041);
  if(TestBoard->writeAMAC(AMACreg::LV_ILOCK_LAM, 0)) setSWError(6042);
  //	Outputs Reset
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 0)) setSWError(6054);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 0)) setSWError(6055);
  if(TestBoard->writeAMAC(AMACreg::HV_ENABLE, 1)) setSWError(6056);
  if(TestBoard->writeAMAC(AMACreg::LV_ENABLE, 1)) setSWError(6057);
  //	Outputs should be back on
  if(TestBoard->HVCNTRL_isOn(isOn)) setSWError(6058);
  if(!isOn) setError(6058);
  logfile << "ILOCK_disabled_Output_reset_Output_val_HV: " << (int)isOn << endl;
  if(TestBoard->LVCNTRL_isOn(isOn)) setSWError(6059);
  if(!isOn) setError(6059);
  logfile << "ILOCK_disabled_Output_reset_Output_val_LV: " << (int)isOn << endl;

  // Set Tresholds to non-critical values
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_HI[channel], 0x7FF)) setSWError(6060);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_HV_LO[channel],   0x0)) setSWError(6061);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_HI[channel], 0x7FF)) setSWError(6062);
  if(TestBoard->writeAMAC(ILOCK_THRESHOLD_LV_LO[channel],   0x0)) setSWError(6063);

  //Disable ILock Flags
  if(TestBoard->writeAMAC(ILOCK_EN_HV_HI[isLeftChannel_int(channel)], 0)) setSWError(6064);
  if(TestBoard->writeAMAC(ILOCK_EN_HV_LO[isLeftChannel_int(channel)], 0)) setSWError(6065);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_HI[isLeftChannel_int(channel)], 0)) setSWError(6066);
  if(TestBoard->writeAMAC(ILOCK_EN_LV_LO[isLeftChannel_int(channel)], 0)) setSWError(6067);

  //Reset
  if(TestBoard->writeAMAC(AMACreg::RESET_HV_ILOCK, 1)) setSWError(6068);
  if(TestBoard->writeAMAC(AMACreg::RESET_LV_ILOCK, 1)) setSWError(6069);
}

void AMACTest::test_Voltage_f_BG(const std::string& name, const std::string& limits_file, 
				 int ADC_channel, int measurements_per_Step, int measurement_delay_ms,
				 int Step_delay_ms)
{
  int i,j;
  double readout_val[measurements_per_Step];

  double sum, dif, stddev;
  double result, result_min=0, result_max=0;

  //Logfile
  string logpath = "log/" + TestName + "_BG_" + name + ".log";
  fstream logfile(logpath, fstream::out);
  logfile << "BGreg_val Voltage stddev" << endl;

  for(i = 0; i <= 16; i++)
    {
      if(TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, i)) setSWError(7000 + i);
      usleep(1000*Step_delay_ms);

      sum = 0;
      for(j = 0; j < measurements_per_Step; j++)
	{
	  if(TestBoard->getADCvoltage(ADC_channel, readout_val[j])) setSWError(7020+i);
	  sum += readout_val[j];
	  usleep(1000*measurement_delay_ms);
	}
      result = sum / measurements_per_Step;
      dif=0;
      for(j = 0; j < measurements_per_Step; j++)
	{
	  double mdif = result - readout_val[j];
	  dif += pow(mdif,2);
	}
      stddev = sqrt(dif/measurements_per_Step);

      if(limits_file != "")
	{
	  if(getLimits(limits_file, i, result_min, result_max)) setSWError(7041);
	  if(result < result_min) setError(7042);
	  if(result > result_max) setError(7043);
	}

#ifdef VERBOSE
      std::cout << "----BGreg " << i << ":	" << result << "V (" << stddev << "V Std deviation)	(" << result_min << "..." << result_max << "V)" << std::endl;
#endif
      logfile << i << " " << result << " " << stddev << std::endl;
    }
}

void AMACTest::test_ADC_f_BG(const std::string& name, const std::string& limits_file, 
			     AMACreg ADC_channel, int measurements_per_Step, int measurement_delay_ms,
			     int Step_delay_ms)
{
  int i,j;
  int readout_val;
  std::vector<int> readout_vals;

  double stddev;
  double result, result_min=0, result_max=0;

  //Logfile
  string logpath = "log/" + TestName + "_BG_ADC_" + name + ".log";
  fstream logfile(logpath, fstream::out);
  logfile << "BGreg_val mean stddev" << endl;

  for(i = 0; i <= 16; i++)
    {
      if(TestBoard->writeAMAC(AMACreg::BANDGAP_CONTROL, i)) setSWError(7000 + i);
      usleep(1000*Step_delay_ms);

      readout_vals.clear();
      for(j = 0; j < measurements_per_Step; j++)
	{
	  if(TestBoard->readAMAC(ADC_channel, readout_val)) setSWError(7020+i);
	  readout_vals.push_back(readout_val);
	  usleep(1000*measurement_delay_ms);
	}

      //Remove any mismeasurements
      std::sort(readout_vals.begin(),readout_vals.end());
      int mode, lastval=readout_vals[0];
      uint modecount=0,tmpmodecount=0;
      for(auto val : readout_vals)
	{
	  if(val==lastval)
	    tmpmodecount++;
	  else
	    {
	      if(tmpmodecount>modecount)
		{
		  modecount=tmpmodecount;
		  mode=lastval;
		}
	    }
	  lastval=val;
	}

      auto itr = std::begin(readout_vals);
      while (itr != std::end(readout_vals))
	{
	  if(abs(*itr-mode)>=4)
	    itr = readout_vals.erase(itr);
	  else
	    itr++;
	}

      // Calculate mean
      double sum=0;
      double sumsum=0;
      //Calculate std deviation
      for(auto val : readout_vals)
	{
	  sum+=val;
	  sumsum+=val*val;
	}
      result = sum/readout_vals.size();
      stddev = sqrt(sumsum/readout_vals.size()-result*result);

      if(limits_file != "")
	{
	  if(getLimits(limits_file, i, result_min, result_max)) setSWError(7041);
	  if(result < result_min) setError(7042);
	  if(result > result_max) setError(7043);
	}

#ifdef VERBOSE
      std::cout << "----BGreg " << i << ":	" << result << " (" << stddev << " Std deviation)	(" << result_min << "..." << result_max << ")" << std::endl;
#endif
      logfile << i << " " << result << " " << stddev << std::endl;
    }
}

void AMACTest::test_CurrentConsumption(const std::string& name, const std::string& limits_file,
				       int ADC_channel, int num_measurements, int measurement_delay_ms)
{
  int j;
  double readout_val[num_measurements];
  double sum, dif, stddev;
  double result, result_min=0, result_max=0;

  //Logfile
  std::string logpath = "log/" + TestName + "_Consumption_" + name + ".log";
  fstream logfile(logpath, fstream::out);
  logfile << "#Reset SupplyCurrent[A] stddev" << endl;

  //Set to reset state
  if(TestBoard->setReset(true)) setSWError(8001);
  usleep(100000);

  //Take measurements
  sum = 0;
  for(j = 0; j < num_measurements; j++){
    if(TestBoard->getADCcurrent(ADC_channel, readout_val[j])) setSWError(8011);
    sum += readout_val[j];
    usleep(1000*measurement_delay_ms);
  }
  result = sum / num_measurements;
  dif=0;
  for(j = 0; j < num_measurements; j++)
    {
      double mdif = result - readout_val[j];
      dif += pow(mdif,2);
    }
  stddev = sqrt(dif/num_measurements);

  //Compare to Limits
  if(limits_file != "")
    {
      if(getLimits(limits_file, 1, result_min, result_max)) setSWError(8021);
      if(result < result_min) setError(8001);
      if(result > result_max) setError(8002);
    }

#ifdef VERBOSE
  cout << "----Current in reset state:	" << result << "A (" << stddev << "A Std deviation)	(" << result_min << "..." << result_max << "A)" << endl;
#endif
  logfile << "1 " << result << " " << stddev << endl;

  //Set to rrun state
  if(TestBoard->setReset(false)) setSWError(8031);
  usleep(100000);

  //Take measurements
  sum = 0;
  for(j = 0; j < num_measurements; j++)
    {
      if(TestBoard->getADCcurrent(ADC_channel, readout_val[j])) setSWError(8041);
      sum += readout_val[j];
      usleep(1000*measurement_delay_ms);
    }
  result = sum / num_measurements;
  dif=0;
  for(j = 0; j < num_measurements; j++)
    {
      double mdif = result - readout_val[j];
      dif += pow(mdif,2);
    }
  stddev = sqrt(dif/num_measurements);

  //Compare to Limits
  if(limits_file != "")
    {
      if(getLimits(limits_file, 0, result_min, result_max)) setSWError(8051);
      if(result < result_min) setError(8011);
      if(result > result_max) setError(8012);
    }

#ifdef VERBOSE
  cout << "----Current in run state:	" << result << "A (" << stddev << "A Std deviation)	(" << result_min << "..." << result_max << "A)" << endl;
#endif
  logfile << "0 " << result << " " << stddev << endl;
}

void AMACTest::readParam_average(const std::string& name, bool log_append, const std::string& limits_file, 
				 AMACreg param, int num_readouts, int delay_ms)
{
  int j;
  int readout_val;
  uint sum=0, sumsum=0;
  double result, result_min=0, result_max=0;
  double stddev;

  //Logfile
  std::string logpath = "log/" + TestName + "_GeneralParams.log";
  fstream logfile;
  if(log_append) 	logfile.open(logpath, fstream::out | fstream::app);
  else
    {
      logfile.open(logpath, fstream::out);
      logfile << "Param val stddev" << endl;
    }

  //Take measurements
  for(j = 0; j < num_readouts; j++)
    {
      if(TestBoard->readAMAC(param, readout_val)) setSWError(9000);
      sum+=readout_val;
      sumsum+=readout_val*readout_val;
      usleep(1000*delay_ms);
    }


  result=((double)sum)/num_readouts;
  stddev=sqrt(((double)sumsum)/num_readouts-result*result);

  if(limits_file != "")
    {
      if(getLimits(limits_file, 0, result_min, result_max)) setSWError(9010);
      if(result < result_min) setError(9011);
      if(result > result_max) setError(9012);
    }

#ifdef VERBOSE
  cout << "----Param " << name << ":	" << result << " (" << stddev << " Std deviation)	(" << result_min << "..." << result_max << "V)" << endl;
#endif
  logfile << name << " " << result << " " << stddev << endl;
}

void AMACTest::measureParam_average	(const std::string& name, bool log_append, const std::string& limits_file, 
					 int ADC_channel, int num_readouts, int delay_ms)
{
  int j;
  double readout_val[num_readouts];
  double sum, dif, stddev;
  double result, result_min=0, result_max=0;

  //Logfile
  string logpath = "log/" + TestName + "_GeneralParams.log";
  fstream logfile;
  if(log_append) 	logfile.open(logpath, fstream::out | fstream::app);
  else
    {
      logfile.open(logpath, fstream::out);
      logfile << "Param val stddev" << std::endl;
    }

  sum = 0;
  for(j = 0; j < num_readouts; j++)
    {
      if(ADC_channel <= MOJO_ADC_ISENSE_1V5) { if(TestBoard->getADCcurrent(ADC_channel, readout_val[j])) setSWError(9020); }
      else if(TestBoard->getADCvoltage(ADC_channel, readout_val[j])) { setSWError(9020); }
      sum += readout_val[j];
      usleep(1000*delay_ms);
    }
  result = sum / num_readouts;
  dif=0;
  for(j = 0; j < num_readouts; j++)
    {
      double mdif = result - readout_val[j];
      dif += pow(mdif,2);
    }
  stddev = sqrt(dif/num_readouts);

  if(limits_file != "")
    {
      if(getLimits(limits_file, 0, result_min, result_max)) setSWError(9030);
      if(result < result_min) setError(9031);
      if(result > result_max) setError(9032);
    }

#ifdef VERBOSE
  std::cout << "----Param " << name << ":	" << result << " (" << stddev << " Std deviation)	(" << result_min << "..." << result_max << "V)" << std::endl;
#endif
  logfile << name << " " << result << " " << stddev << std::endl;
}

//////////////////////////////////////
// Internal Functions
//////////////////////////////////////

void AMACTest::setSWError(int error_nr)
{
#ifdef VERBOSE
  cout << "[" << TestName << "] SW Error " << error_nr << endl;
#endif
  //If log errors options selected, create separate logfile that lists all the occurred SW errors
  if(log_errors)
    {
      fstream ErrorLog("log/" + TestName + "_Errors.log", fstream::out | fstream::app);
      //Get system time
      time_t now = time(0);
      //Convert to string
      char* dt = ctime(&now);
      //Replace endl at the end by space
      char* temp = dt;
      while(temp[1] != 0) temp++;
      *temp = ' ';
      //Write
      ErrorLog << dt << "[" << TestName << "] SW Error " << error_nr << endl;
    }
  SWErrors.push_back(error_nr);
}

void AMACTest::setError(int error_nr){
	#ifdef VERBOSE
		printError(error_nr);
	#endif
	//If log errors options selected, create separate logfile that lists all the occurred errors
	if(log_errors){
		fstream ErrorLog("log/" + TestName + "_Errors.log", fstream::out | fstream::app);
		//Get system time
		time_t now = time(0);
		//Convert to string
		char* dt = ctime(&now);
		//Replace endl at the end by space
		char* temp = dt;
		while(temp[1] != 0) temp++;
		*temp = ' ';
		//Write
		ErrorLog << dt << "[" << TestName << "] Error " << error_nr << endl;
	}
	Errors.push_back(error_nr);
}

void AMACTest::printError(int error_nr){
  std::cout << "[" << TestName << "] ERROR " << error_nr << "	-	";
  switch(error_nr)
    {
    case 1001:	std::cout << "Voltage Test - Out of Range (Below)"; break;
    case 1002:	std::cout << "Voltage Test - Out of Range (Above)"; break;
    case 2001:	std::cout << "LVCTRL Test - Written 1 to enable - Read back 0"; break;
    case 2002:	std::cout << "LVCTRL Test - Written 1 to enable - Output 0"; break;
    case 2003:	std::cout << "LVCTRL Test - Written 1 to enable - Inverting Output 1"; break;
    case 2004:	std::cout << "LVCTRL Test - Written 0 to enable - Read back 1"; break;
    case 2005:	std::cout << "LVCTRL Test - Written 0 to enable - Output 1"; break;
    case 2006:	std::cout << "LVCTRL Test - Written 0 to enable - Inverting Output 0"; break;
    default:	std::cout << "This error is not defined"; break;
    }
  std::cout << std::endl;
}

int AMACTest::getLimits(const std::string& limits_path, double input, double &min, double &max)
{
  double input_below = 0, input_above;
  double min_below = 0, min_above, max_below = 0, max_above;

  min = 0;
  max = 0;

  //Configuration File
  fstream limit_file(limits_path, fstream::in);
  if(!limit_file) return -1;

  //Search for limits that surround input value
  while(limit_file)
    {
      limit_file >> input_above >> min_above >> max_above;
      if(input_above >= input) break;
      input_below = input_above;
      min_below = min_above;
      max_below = max_above;
    }
  if(input_above < input) return -1;

  //Get accurate limits for input value with linear interpolation
  double diff = input_above - input_below;
  if(diff == 0)
    {
      min = min_above;
      max = max_above;
      return 0;
    }
  double step = diff / (min_above - min_below);
  double diff2 = input - input_below;
  min = min_below + (diff2/step);
  step = diff / (max_above - max_below);
  max = max_below + (diff2/step);
  return 0;
}

int AMACTest::getLimitsByIndex(const std::string& limits_path, int index, int &min, int &max)
{
  int test_index, test_min, test_max;
  min = 0;
  max = 0;

  //Configuration File
  fstream limit_file(limits_path, fstream::in);
  if(!limit_file) return -1;

  //Search for limits that match the index
  while(limit_file)
    {
      limit_file >> test_index >> test_min >> test_max;
      if(index==test_index)
	{
	  min=test_min;
	  max=test_max;
	  return 0;
	}
    }
  return -1;
}
