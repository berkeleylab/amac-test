#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void cleanup()
{
  /*vector<AMACTest*> all_tests;

    for(AMACTest* t : AMACTest::activeTests) all_tests.push_back(t);
    for(AMACTest* t : all_tests) delete t;*/
}

void siginthandler(int sig)
{
  cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("TEST");
  std::vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);
  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  //General Measurements
  std::cout << "====== General Measurements ======" << std::endl;

  //	Read Values
  bool log_append = false;
  for(int i = 3; i <= 6; i++)
    {
      float VCC = ((float)i)/2;
      TestBoard.setVCC_IO(VCC);
      std::cout << "Set VCC IO to: " << VCC << std::endl;

      // Reset measuremnets
      TestBoard.setReset(true);
      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_current_1V5_Reset", log_append, "", 
      					 MOJO_ADC_ISENSE_1V5, 100, 100);
      log_append = true;
      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_Reset", log_append, "", 
      					 MOJO_ADC_1V2, 100, 100);
      TestBoard.setReset(false);

      // Update ADC configurations
      TestBoard.setVCC_IO(2.5);
      TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 3);
      TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 3);
      TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 10);
      TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 0);
      TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 0);
      TestBoard.setVCC_IO(VCC);

      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_current_1V5_Run", log_append, "", 
      					 MOJO_ADC_ISENSE_1V5, 100, 100);
      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_Run", log_append, "", 
      					 MOJO_ADC_1V2, 100, 100);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_2_R", log_append, "", 
				 AMACreg::VALUE_RIGHT_CH0, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_2_L", log_append, "", 
				 AMACreg::VALUE_LEFT_CH0, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_DVDD_2", log_append, "", 
				 AMACreg::VALUE_LEFT_CH1, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_BG", log_append, "", 
				 AMACreg::VALUE_LEFT_CH2, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_Temp", log_append, "", 
				 AMACreg::VALUE_LEFT_CH4, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_H_4", log_append, "", 
				 AMACreg::VALUE_RIGHT_CH4, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_OTA_R", log_append, "", 
				 AMACreg::VALUE_RIGHT_CH5, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_OTA_L", log_append, "", 
				 AMACreg::VALUE_LEFT_CH5, 10, 200);
    }

  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;
	
  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();
  cleanup();
  exit(0);    	
}
