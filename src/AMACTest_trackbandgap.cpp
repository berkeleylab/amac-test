////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <ctime>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) 
    {
      std::cerr << "Cannot conntect to the test board" << std::endl;
      exit(-1);
    }

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF1");
  std::vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  TestBoard.setVCC_IO((float)2.5);

  // 	Set ADCconfiguration
  uint bg=10;

  TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 3);
  TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 3);
  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 0);
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 0);
  TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, bg);

  // output log
  std::string logpath = "log/AMAC_B02_BG10_BGTIME.log";
  std::fstream logfile(logpath, std::fstream::out);
  logfile << "timestr BGreg_val amac mojo" << std::endl;

  auto start = std::chrono::high_resolution_clock::now();
  while(true)
    {
      usleep(100000);

      int amac_val;
      double mojo_val,mojo_val_sum=0;
      auto elapsed = std::chrono::high_resolution_clock::now() - start;
      long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
      TestBoard.readAMAC(AMACreg::VALUE_LEFT_CH2, amac_val);
      for(uint i=0;i<10;i++)
	{
	  TestBoard.getADCvoltage(MOJO_ADC_BGO,mojo_val);
	  mojo_val_sum+=mojo_val;
	}
      mojo_val=mojo_val_sum/10;

      std::cout << "----BGreg " << bg << ":	" << amac_val << " " << mojo_val << std::endl;
      logfile << microseconds << " " << bg << " " << amac_val << " " << mojo_val << std::endl;
    }
  
  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;
      
  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();

  sleep(1);
  return 0;
}
