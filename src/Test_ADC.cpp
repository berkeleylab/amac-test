#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <chrono>

#include "AMAC_TB.h"
#include <signal.h>

using namespace std;

AMAC_TB* TestBoard;

void siginthandler(int sig){
	char val;

	// Power down
	do{
		val = 0;TestBoard->PowerDown();
		cout << "--------------------------------" << endl;
		cout << "Power Down: " << (int)val << endl;
		cout << "--------------------------------" << endl;
	}while(val != 0);

	exit(-2);   	
}

int main(int argc, char *argv[]){
	int i, val;

	TestBoard = new AMAC_TB();
	if(!TestBoard->isConnected()) exit(-1);

	//fstream logfile(("log.dat"), fstream::out);

	//Signal to exit program
	signal(SIGINT, siginthandler);

	TestBoard->PowerUp();
	while(TestBoard->I2C_write(46,1,&(i=3)));
	TestBoard->I2C_enable();

	cout << "Test board up and running" << endl;
	cout << "-------------------------" << endl << endl;

	cout << "--Changing gain of OPAMP (0x00): " << TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN, 0x0) << endl;

	cout << "--Set VchanLEFT0 to 0: " << TestBoard->setDAC(DAC_VCHANLEFT0, 0) << endl;
	usleep(100000);
	cout << "--Value measured by AMAC(" << TestBoard->readAMAC(AMACreg::VALUE_LEFT_CH0, val) << "): ";
	cout << val << endl;

	cout << "--Set VchanLEFT0 to MAX: " << TestBoard->setDAC(DAC_VCHANLEFT0, 0xFFF) << endl;
	usleep(100000);
	cout << "--Value measured by AMAC(" << TestBoard->readAMAC(AMACreg::VALUE_LEFT_CH0, val) << "): ";
	cout << val << endl;

	cout << "--Changing gain of OPAMP (0x01): " << TestBoard->writeAMAC(AMACreg::LEFT_RAMP_GAIN, 0x1) << endl;

	cout << "--Set VchanLEFT0 to 0: " << TestBoard->setDAC(DAC_VCHANLEFT0, 0) << endl;
	usleep(100000);
	cout << "--Value measured by AMAC(" << TestBoard->readAMAC(AMACreg::VALUE_LEFT_CH0, val) << "): ";
	cout << val << endl;

	cout << "--Set VchanLEFT0 to MAX: " << TestBoard->setDAC(DAC_VCHANLEFT0, 0xFFF) << endl;
	usleep(200000);
	cout << "--Value measured by AMAC(" << TestBoard->readAMAC(AMACreg::VALUE_LEFT_CH0, val) << "): ";
	cout << val << endl;

	TestBoard->PowerDown();

	//End of Program
	exit(0);    	
}
