////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

using namespace std;

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("TEST");
  vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  cout << "====== InitTest: " << Test1.Init() << " ======" << endl;


  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  cout << "====== Evaluation ======" << endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	cout << "PASSED ";
  else		cout << "FAILED ";
  cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << endl;
	
  //Check for SW Errors
  vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  cout << "[" << SWErrors.size() << " SW Errors occured]" << endl;

  //End of Program
  TestBoard.PowerDown();
  exit(0);    	
}
