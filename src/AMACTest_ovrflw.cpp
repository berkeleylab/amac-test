////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) 
    {
      std::cerr << "Cannot connect to the test board" << std::endl;
      exit(-1);
    }

  //Signal to exit program
  signal(SIGINT, siginthandler);

  int result;

  TestBoard.setVCC_IO((float)2.2);

  TestBoard.PowerUp();
  TestBoard.I2C_enable();

  TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 10);
  TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN,  3);
  TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN ,  3);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL,  0);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL,  0);

  TestBoard.setDACvoltage(DAC_VCHANRIGHT3, 0.8);
  sleep(10);
  TestBoard.readAMAC(AMACreg::VALUE_RIGHT_CH3, result);
  std::cout << "Set to 0.8 V, read " << result << " ADC counts." << std::endl;

  TestBoard.setDACvoltage(DAC_VCHANRIGHT3, 1.5);
  sleep(10);
  TestBoard.readAMAC(AMACreg::VALUE_RIGHT_CH3, result);
  std::cout << "Set to 1.5 V, read " << result << " ADC counts." << std::endl;

  TestBoard.setDACvoltage(DAC_VCHANRIGHT3, 1.0);
  sleep(10);
  TestBoard.readAMAC(AMACreg::VALUE_RIGHT_CH3, result);
  std::cout << "Set to 1.0 V, read " << result << " ADC counts." << std::endl;

  TestBoard.setDACvoltage(DAC_VCHANRIGHT3, 1.5);
  sleep(10);
  TestBoard.readAMAC(AMACreg::VALUE_RIGHT_CH3, result);
  std::cout << "Set to 1.5 V, read " << result << " ADC counts." << std::endl;

  //End of Program
  TestBoard.PowerDown();
  exit(0);    	
}
