////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <ctime>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) 
    {
      std::cerr << "Cannot conntect to the test board" << std::endl;
      exit(-1);
    }

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF1");
  std::vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  TestBoard.setVCC_IO((float)2.5);

  /**
   * Voltage Noise Test
   */
  std::cout << "--CH0_L" << std::endl;
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1);
  Test1.noise_ADC_ext	("CH0_L", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANLEFT0, AMACreg::VALUE_LEFT_CH0);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_L_1" << std::endl;
  Test1.noise_ADC_ext	("CH3_L_Gain1", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_L_23" << std::endl;
  Test1.noise_ADC_ext	("CH3_L_Gain23", LOG_NEW,
			 AMAC_1V2*3/2/2, 1000,
			 10, 3,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  std::cout << "--CH0_R" << std::endl;
  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1);
  Test1.noise_ADC_ext	("CH0_R", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANRIGHT0, AMACreg::VALUE_RIGHT_CH0);
  std::cout << "--CH1_R" << std::endl;
  Test1.noise_ADC_ext	("CH1_R", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANRIGHT1, AMACreg::VALUE_RIGHT_CH1);
  std::cout << "--CH2_R" << std::endl;
  Test1.noise_ADC_ext	("CH2_R", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANRIGHT2, AMACreg::VALUE_RIGHT_CH2);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_R_1" << std::endl;
  Test1.noise_ADC_ext	("CH3_R_Gain1", LOG_NEW,
			 AMAC_1V2/2, 1000,
			 10, 3,
			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_R_23" << std::endl;
  Test1.noise_ADC_ext	("CH3_R_Gain23", LOG_NEW,
			 AMAC_1V2*3/2/2, 1000,
			 10, 3,
			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);

  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;

  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();

  return 0;
}
