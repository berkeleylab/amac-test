#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void cleanup()
{
  /*vector<AMACTest*> all_tests;

    for(AMACTest* t : AMACTest::activeTests) all_tests.push_back(t);
    for(AMACTest* t : all_tests) delete t;*/
}

void siginthandler(int sig)
{
  cleanup();
  exit(-2);
}

void setSWError(int fuck){ std::cout << "FUCK " << fuck << std::endl; }

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF1");
  std::vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);
  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  TestBoard.setVCC_IO((float)2);

  // Prepare configuration
  TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 10);
  TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN,  3);
  TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN ,  3);

  // ILock Test
  std::cout << "====== ILock Test ======" << std::endl;
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 0);
  std::cout << "Channel: CH0_L (sel = 0)" << std::endl;
  Test1.test_ILock("CH0_L_0", AMAC_ADC_LEFT_CH0);

  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1);
  std::cout << "Channel: CH0_L (sel = 1)" << std::endl;
  Test1.test_ILock("CH0_L_1", AMAC_ADC_LEFT_CH0, DAC_VCHANLEFT0, 0.1, 0.9);

  std::cout << "Channel: CH1_L" << std::endl;
  Test1.test_ILock("CH1_L", AMAC_ADC_LEFT_CH1);

  std::cout << "Channel: CH2_L" << std::endl;
  Test1.test_ILock("CH2_L", AMAC_ADC_LEFT_CH2);

  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL,  0);
  std::cout << "Channel: CH3_L_GAIN1" << std::endl;
  Test1.test_ILock("CH3_L_GAIN1", AMAC_ADC_LEFT_CH3, DAC_VCHANLEFT3, 0.1, 0.9);

  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL,  1);
  std::cout << "Channel: CH3_L_GAIN23" << std::endl;
  Test1.test_ILock("CH3_L_GAIN23", AMAC_ADC_LEFT_CH3, DAC_VCHANLEFT3, 0.1, 0.9);

  std::cout << "Channel: CH4_L" << std::endl;
  Test1.test_ILock("CH4_L", AMAC_ADC_LEFT_CH4);

  std::cout << "Channel: CH5_L" << std::endl;
  Test1.test_ILock("CH5_L", AMAC_ADC_LEFT_CH5);


  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 0);
  std::cout << "Channel: CH0_R (sel = 0)" << std::endl;
  Test1.test_ILock("CH0_R_0", AMAC_ADC_RIGHT_CH0);

  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1);
  std::cout << "Channel: CH0_R (sel = 1)" << std::endl;
  Test1.test_ILock("CH0_R_1", AMAC_ADC_RIGHT_CH0, DAC_VCHANRIGHT0, 0.1, 0.9);

  std::cout << "Channel: CH1_R" << std::endl;
  Test1.test_ILock("CH1_R", AMAC_ADC_RIGHT_CH1);

  std::cout << "Channel: CH2_R" << std::endl;
  Test1.test_ILock("CH2_R", AMAC_ADC_RIGHT_CH2);

  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL,  0);
  std::cout << "Channel: CH3_R_GAIN1" << std::endl;
  Test1.test_ILock("CH3_R_GAIN1", AMAC_ADC_RIGHT_CH3, DAC_VCHANRIGHT3, 0.1, 0.9);

  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL,  1);
  std::cout << "Channel: CH3_R_GAIN23" << std::endl;
  Test1.test_ILock("CH3_R_GAIN23", AMAC_ADC_RIGHT_CH3, DAC_VCHANRIGHT3, 0.1, 0.9);

  std::cout << "Channel: CH4_R" << std::endl;
  Test1.test_ILock("CH4_R", AMAC_ADC_RIGHT_CH4);

  std::cout << "Channel: CH5_R" << std::endl;
  Test1.test_ILock("CH5_R", AMAC_ADC_RIGHT_CH5);

  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;

  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();
  cleanup();
  exit(0);    	
}
