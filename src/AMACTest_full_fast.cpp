#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

using namespace std;

int main(int argc, char *argv[])
{
  string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard;
  if(!TestBoard.isConnected()) exit(-1);

  //Start Test
  AMACTest Test1("AMACREF1");
  vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  cout << "====== InitTest: " << Test1.Init() << " ======" << endl;
	
  /*	//Current consumption
	cout << "====== Current Consumption ======" << endl;
	Test1.test_CurrentConsumption	("", "limits/Current_Limits.dat", 
					MOJO_ADC_ISENSE_1V5, 10, 50);

	
	//General Measurements
	cout << "====== General Measurements ======" << endl;
	// 	Set ADCconfiguration
	while(TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 1));
	while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
	while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 0));
	while(TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1));
	while(TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1));

	//	Read Values
	bool log_append = false;
	for(int i = 3; i <= 6; i++){
		float VCC = ((float)i)/2;
		while(TestBoard.setVCC_IO(VCC));
		cout << "Set VCC IO to: " << VCC << endl;
		TestBoard.setReset(true);
		Test1.measureParam_average	("VCC_H_" + to_string(VCC) + "_current_1V5_Reset", log_append, "", 
						MOJO_ADC_ISENSE_1V5, 10, 100);
		log_append = true;
		Test1.measureParam_average	("VCC_H_" + to_string(VCC) + "_VDD_Reset", log_append, "", 
						MOJO_ADC_1V2, 10, 100);
		TestBoard.setReset(false);
		Test1.measureParam_average	("VCC_H_" + to_string(VCC) + "_current_1V5_Run", log_append, "", 
						MOJO_ADC_ISENSE_1V5, 10, 100);
		Test1.measureParam_average	("VCC_H_" + to_string(VCC) + "_VDD_Run", log_append, "", 
						MOJO_ADC_1V2, 10, 100);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_VDD_2_R", log_append, "", 
					AMACreg::VALUE_RIGHT_CH0, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_VDD_2_L", log_append, "", 
					AMACreg::VALUE_LEFT_CH0, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_DVDD_2", log_append, "", 
					AMACreg::VALUE_LEFT_CH1, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_BG", log_append, "", 
					AMACreg::VALUE_LEFT_CH2, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "Temp", log_append, "", 
					AMACreg::VALUE_LEFT_CH4, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "VDD_H_4", log_append, "", 
					AMACreg::VALUE_RIGHT_CH4, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_OTA_R", log_append, "", 
					AMACreg::VALUE_RIGHT_CH5, 10, 200);
		Test1.readParam_average	("VCC_H_" + to_string(VCC) + "_OTA_L", log_append, "", 
					AMACreg::VALUE_LEFT_CH5, 10, 200);
	}

	// I2C Test
	cout << "====== I2C reliability Test ======" << endl;
	Test1.test_I2C_reliability	("", LOG_NEW, "limits/I2C_dummy.dat", 
					0.6, 3, 0.4,
					100);

	// Bandgap Dependency Test
	cout << "====== BG dependency Test ======" << endl;
	cout << "--BGO" << endl;
	Test1.test_Voltage_f_BG		("BGO", "limits/BG_dummy.dat", 
					MOJO_ADC_BGO, 10, 50,
					200);
	cout << "--OTA" << endl;
	Test1.test_Voltage_f_BG		("OTA", "limits/BG_dummy.dat", 
					MOJO_ADC_OTA, 10, 50,
					200);
	cout << "--NTCPWR" << endl;
	Test1.test_Voltage_f_BG		("NTCPWR", "limits/BG_dummy.dat", 
					MOJO_ADC_NTCPWR, 10, 50,
					200);
	cout << "--1V2" << endl;
	Test1.test_Voltage_f_BG		("1V2", "limits/BG_dummy.dat", 
					MOJO_ADC_1V2, 10, 50,
					200);

	//Voltage Test
	cout << "====== Voltage Test ======" << endl;
	while(TestBoard.setVCC_IO((float)2.2));
	while(TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 1));
	while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
	for(int i = 0; i < 16; i++){
		cout << "Set Bandgap Register to: " << i << endl;
		while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, i));
		cout << "--CH0_L" << endl;
		Test1.test_ADC_ext	("CH0_L_RampGain1_BGO" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANLEFT0, AMACreg::VALUE_LEFT_CH0);
		while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0));
		cout << "--CH3_L_1" << endl;
		Test1.test_ADC_ext	("CH3_L_RampGain1_BGO" + to_string(i) + "_Gain1", LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
		while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 1));
		cout << "--CH3_L_23" << endl;
		Test1.test_ADC_ext	("CH3_L_RampGain1_BGO" + to_string(i) + "_Gain23", LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2*3/2, 100e-3, 200,
					DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
		cout << "--CH0_R" << endl;
		Test1.test_ADC_ext	("CH0_R_RampGain1_BGO" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANRIGHT0, AMACreg::VALUE_RIGHT_CH0);
		cout << "--CH1_R" << endl;
		Test1.test_ADC_ext	("CH1_R_RampGain1_BGO" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANRIGHT1, AMACreg::VALUE_RIGHT_CH1);
		cout << "--CH2_R" << endl;
		Test1.test_ADC_ext	("CH2_R_RampGain1_BGO" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANRIGHT2, AMACreg::VALUE_RIGHT_CH2);
		while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0));
		cout << "--CH3_R_1" << endl;
		Test1.test_ADC_ext	("CH3_R_RampGain1_BGO" + to_string(i) + "_Gain1", LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2, 100e-3, 200,
					DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);
		while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 1));
		cout << "--CH3_R_23" << endl;
		Test1.test_ADC_ext	("CH3_R_RampGain1_BGO" + to_string(i) + "_Gain23", LOG_NEW, "limits/ADC_dummy.dat", 
					0, AMAC_1V2*3/2, 100e-3, 200,
					DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);
	}
*/
	//Current Test
/*	cout << "====== Current Test ======" << endl;
	while(TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 1));
	while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
	for(int i = 0; i < 1; i++){
		cout << "Set Bandgap Register to: " << i << endl;
		while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, i));
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_RIGHT, 1));
		cout << "--Gain 1, right" << endl;
		Test1.test_current_measurement	("RIGHT_RampGain1_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 100e-6, 1e-6, 200, 
						false);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_LEFT, 1));
		cout << "--Gain 1, left" << endl;
		Test1.test_current_measurement	("LEFT_RampGain1_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 100e-6, 1e-6, 200, 
						true);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_RIGHT, 2));
		cout << "--Gain 2, right" << endl;
		Test1.test_current_measurement	("RIGHT_RampGain2_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 2e-3, 10e-6, 200, 
						false);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_LEFT, 2));
		cout << "--Gain 2, left" << endl;
		Test1.test_current_measurement	("LEFT_RampGain2_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 2e-3, 10e-6, 200, 
						true);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_RIGHT, 4));
		cout << "--Gain 4, right" << endl;
		Test1.test_current_measurement	("RIGHT_RampGain4_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 10e-3, 100e-6, 200, 
						false);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_LEFT, 4));
		cout << "--Gain 4, left" << endl;
		Test1.test_current_measurement	("LEFT_RampGain4_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 10e-3, 100e-6, 200, 
						true);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_RIGHT, 8));
		cout << "--Gain 8, right" << endl;
		Test1.test_current_measurement	("RIGHT_RampGain2_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 20e-3, 100e-6, 200, 
						false);
		while(TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_LEFT, 8));
		cout << "--Gain 8, left" << endl;
		Test1.test_current_measurement	("LEFT_RampGain2_BGO" + to_string(i) + "_OPGain1", LOG_NEW, "limits/ADC_dummy.dat", 
						0, 20e-3, 100e-6, 200, 
						true);
	}

	//Workaround
	// Clock and CTRL outputs do not work properly when VCC_H > 2V
	// But VCC_H needs to be > 2V so I2C communication always works
	while(TestBoard.setVCC_IO((float)2.2));

	//LVCTRL Test
	cout << "====== LVCTRL Test ======" << endl;
	Test1.test_LVCTRL("", 100);*/

	// Clock Test
	cout << "====== Clock Test ======" << endl;
	Test1.test_clock_and_HVCTRL	("main", "limits/Clock_Limits.dat", 
					200);

	// ILock Test
	cout << "====== ILock Test ======" << endl;
	for(int i = AMAC_ADC_LEFT_CH0; i <= AMAC_ADC_RIGHT_CH6; i++) {
		cout << "Channel: " << i << endl;
		Test1.test_ILock	("noDACset_CH" + to_string(i), 
					i);
	}

	//Evaluate Test
	bool passed;
	int num_Errors, num_Criticals;
	cout << "====== Evaluation ======" << endl;
	Test1.evaluateTest(passed, num_Errors, num_Criticals);
	if(passed) 	cout << "PASSED ";
	else		cout << "FAILED ";
	cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << endl;
	
	//Check for SW Errors
	vector<int> SWErrors;
	Test1.getSWErrors(SWErrors);
	cout << "[" << SWErrors.size() << " SW Errors occured]" << endl;

	//End of Program
	TestBoard.PowerDown();
	exit(0);    	
}
