////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  if(argc!=2)
    {
      std::cerr << "usage: " << argv[0] << " amac" << std::endl;
      exit(-1);
    }

  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) 
    {
      std::cerr << "Cannot conntect to the test board" << std::endl;
      exit(-1);
    }

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1(argv[1]);
  std::vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  /**
   * GENERAL PARAMETERS
   */
  std::cout << "====== General Measurements ======" << std::endl;

  //	Read Values
  bool log_append = false;
  for(int i = 3; i <= 6; i++)
    {
      float VCC = ((float)i)/2;
      TestBoard.setVCC_IO(VCC);
      std::cout << "Set VCC IO to: " << VCC << std::endl;

      // Reset measuremnets
      TestBoard.setReset(true);
      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_current_1V5_Reset", log_append, "", 
      					 MOJO_ADC_ISENSE_1V5, 100, 100);
      log_append = true;
      Test1.measureParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_Reset", log_append, "", 
      					 MOJO_ADC_1V2, 100, 100);
      TestBoard.setReset(false);

      // Update ADC configurations
      TestBoard.setVCC_IO(2.5);
      TestBoard.writeAMAC(AMACreg::RIGHT_RAMP_GAIN, 3);
      TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 3);
      TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 10);
      TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 0);
      TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 0);
      TestBoard.setVCC_IO(VCC);

      Test1.measureParam_average("VCC_H_" + std::to_string(VCC) + "_current_1V5_Run", log_append, "", 
      				 MOJO_ADC_ISENSE_1V5, 100, 100);
      Test1.measureParam_average("VCC_H_" + std::to_string(VCC) + "_VDD_Run", log_append, "", 
      				 MOJO_ADC_1V2, 100, 100);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_2_R", log_append, "", 
  				 AMACreg::VALUE_RIGHT_CH0, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_2_L", log_append, "", 
  				 AMACreg::VALUE_LEFT_CH0, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_DVDD_2", log_append, "", 
  				 AMACreg::VALUE_LEFT_CH1, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_BG", log_append, "", 
  				 AMACreg::VALUE_LEFT_CH2, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_Temp", log_append, "", 
  				 AMACreg::VALUE_LEFT_CH4, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_VDD_H_4", log_append, "", 
  				 AMACreg::VALUE_RIGHT_CH4, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_OTA_R", log_append, "", 
  				 AMACreg::VALUE_RIGHT_CH5, 10, 200);
      Test1.readParam_average	("VCC_H_" + std::to_string(VCC) + "_OTA_L", log_append, "", 
  				 AMACreg::VALUE_LEFT_CH5, 10, 200);
    }

  /**
   * I2C Reliability
   */
  std::cout << "====== I2C Reliability ======" << std::endl;
  Test1.test_I2C_reliability	("main", LOG_NEW, "limits/I2C_dummy.dat", 
  				 0.65, 2.95, 0.05,
  				 100);


  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  TestBoard.setVCC_IO((float)2.5);

  /**
   * Bandgap Dependency Test
   */
  std::cout << "====== BG dependency Test ======" << std::endl;
  std::cout << "--current_1V5_Run" << std::endl;
  Test1.test_Voltage_f_BG		("current_1V5_Run", "limits/BG_dummy.dat", 
  					 MOJO_ADC_ISENSE_1V5, 100, 50,
  					 200);

  std::cout << "--VDD_Run" << std::endl;
  Test1.test_Voltage_f_BG		("VDD_Run", "limits/BG_dummy.dat", 
  					 MOJO_ADC_1V2, 100, 50,
  					 200);

  std::cout << "--BGO" << std::endl;
  Test1.test_Voltage_f_BG		("BGO", "limits/BG_dummy.dat", 
  					 MOJO_ADC_BGO, 100, 50,
  					 200);
  std::cout << "--OTA" << std::endl;
  Test1.test_Voltage_f_BG		("OTA", "limits/BG_dummy.dat", 
  					 MOJO_ADC_OTA, 100, 50,
  					 200);
  std::cout << "--NTCPWR" << std::endl;
  Test1.test_Voltage_f_BG		("NTCPWR", "limits/BG_dummy.dat", 
  					 MOJO_ADC_NTCPWR, 100, 50,
  					 200);
  std::cout << "--1V2" << std::endl;
  Test1.test_Voltage_f_BG		("1V2", "limits/BG_dummy.dat", 
  					 MOJO_ADC_1V2, 100, 50,
  					 200);

  /**
   * Voltage Noise Test
   */
  std::cout << "--CH0_L" << std::endl;
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1);
  Test1.noise_ADC_ext	("CH0_L", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANLEFT0, AMACreg::VALUE_LEFT_CH0);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_L_1" << std::endl;
  Test1.noise_ADC_ext	("CH3_L_Gain1", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_L_23" << std::endl;
  Test1.noise_ADC_ext	("CH3_L_Gain23", LOG_NEW,
  			 AMAC_1V2*3/2/2, 1000,
  			 10, 3,
  			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  std::cout << "--CH0_R" << std::endl;
  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1);
  Test1.noise_ADC_ext	("CH0_R", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANRIGHT0, AMACreg::VALUE_RIGHT_CH0);
  std::cout << "--CH1_R" << std::endl;
  Test1.noise_ADC_ext	("CH1_R", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANRIGHT1, AMACreg::VALUE_RIGHT_CH1);
  std::cout << "--CH2_R" << std::endl;
  Test1.noise_ADC_ext	("CH2_R", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANRIGHT2, AMACreg::VALUE_RIGHT_CH2);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_R_1" << std::endl;
  Test1.noise_ADC_ext	("CH3_R_Gain1", LOG_NEW,
  			 AMAC_1V2/2, 1000,
  			 10, 3,
  			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_R_23" << std::endl;
  Test1.noise_ADC_ext	("CH3_R_Gain23", LOG_NEW,
  			 AMAC_1V2*3/2/2, 1000,
  			 10, 3,
  			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);

  /**
   * Voltage Calibration
   */
  std::cout << "====== Voltage Calibration ======" << std::endl;

  std::cout << "--CH0_L" << std::endl;
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1);
  Test1.calib_ADC_ext	("CH0_L", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANLEFT0, AMACreg::VALUE_LEFT_CH0);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_L_1" << std::endl;
  Test1.calib_ADC_ext	("CH3_L_Gain1", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_L_23" << std::endl;
  Test1.calib_ADC_ext	("CH3_L_Gain23", LOG_NEW,
  			 0, AMAC_1V2*3/2, 10e-3, 1,
  			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  std::cout << "--CH0_R" << std::endl;
  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1);
  Test1.calib_ADC_ext	("CH0_R", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANRIGHT0, AMACreg::VALUE_RIGHT_CH0);
  std::cout << "--CH1_R" << std::endl;
  Test1.calib_ADC_ext	("CH1_R", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANRIGHT1, AMACreg::VALUE_RIGHT_CH1);
  std::cout << "--CH2_R" << std::endl;
  Test1.calib_ADC_ext	("CH2_R", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANRIGHT2, AMACreg::VALUE_RIGHT_CH2);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 0);
  std::cout << "--CH3_R_1" << std::endl;
  Test1.calib_ADC_ext	("CH3_R_Gain1", LOG_NEW,
  			 0, AMAC_1V2, 10e-3, 1,
  			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);
  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL, 1);
  std::cout << "--CH3_R_23" << std::endl;
  Test1.calib_ADC_ext	("CH3_R_Gain23", LOG_NEW,
  			 0, AMAC_1V2*3/2, 10e-3, 1,
  			 DAC_VCHANRIGHT3, AMACreg::VALUE_RIGHT_CH3);

  /**
   * Current Calibration
   */
  std::cout << "====== Current Test ======" << std::endl;

  std::cout << "--LEFT" << std::endl;
  Test1.calib_current_measurement	("LEFT", LOG_NEW,
  					 0, 3.3e-6, 3.3e-8, 1, 3,
  					 true);
  Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  					 0, 3.3e-4, 3.3e-6, 1, 2,
  					 true);
  Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  					 0, 20e-3, 20e-5, 1, 1,
  					 true);

  std::cout << "--RIGHT" << std::endl;
  Test1.calib_current_measurement	("RIGHT", LOG_NEW,
  					 0, 3.3e-6, 3.3e-8, 1, 3,
  					 false);
  Test1.calib_current_measurement	("RIGHT", LOG_APPEND,
  					 0, 3.3e-4, 3.3e-6, 1, 2,
  					 false);
  Test1.calib_current_measurement	("RIGHT", LOG_APPEND,
  					 0, 20e-3, 20e-5, 1, 1,
  					 false);


  /**
   * LVCTRL
   */
  std::cout << "====== LVCTRL Test ======" << std::endl;
  Test1.test_LVCTRL("main");

  /**
   * HVCTRL
   */
  std::cout << "====== HVCTRL Test ======" << std::endl;
  Test1.test_clock_and_HVCTRL	("main", "limits/Clock_Limits.dat", 
  				 200);

  /**
   * ILock test
   */
  std::cout << "====== ILock Test ======" << std::endl;
  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 0);
  std::cout << "Channel: CH0_L (sel = 0)" << std::endl;
  Test1.test_ILock("CH0_L_0", AMAC_ADC_LEFT_CH0);

  TestBoard.writeAMAC(AMACreg::LT_CH0_SEL, 1);
  std::cout << "Channel: CH0_L (sel = 1)" << std::endl;
  Test1.test_ILock("CH0_L_1", AMAC_ADC_LEFT_CH0, DAC_VCHANLEFT0, 0.1, 0.9);

  std::cout << "Channel: CH1_L" << std::endl;
  Test1.test_ILock("CH1_L", AMAC_ADC_LEFT_CH1);

  std::cout << "Channel: CH2_L" << std::endl;
  Test1.test_ILock("CH2_L", AMAC_ADC_LEFT_CH2);

  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL,  0);
  std::cout << "Channel: CH3_L_GAIN1" << std::endl;
  Test1.test_ILock("CH3_L_GAIN1", AMAC_ADC_LEFT_CH3, DAC_VCHANLEFT3, 0.1, 0.9);

  TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL,  1);
  std::cout << "Channel: CH3_L_GAIN23" << std::endl;
  Test1.test_ILock("CH3_L_GAIN23", AMAC_ADC_LEFT_CH3, DAC_VCHANLEFT3, 0.1, 0.9);

  std::cout << "Channel: CH4_L" << std::endl;
  Test1.test_ILock("CH4_L", AMAC_ADC_LEFT_CH4);

  std::cout << "Channel: CH5_L" << std::endl;
  Test1.test_ILock("CH5_L", AMAC_ADC_LEFT_CH5);


  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 0);
  std::cout << "Channel: CH0_R (sel = 0)" << std::endl;
  Test1.test_ILock("CH0_R_0", AMAC_ADC_RIGHT_CH0);

  TestBoard.writeAMAC(AMACreg::RT_CH0_SEL, 1);
  std::cout << "Channel: CH0_R (sel = 1)" << std::endl;
  Test1.test_ILock("CH0_R_1", AMAC_ADC_RIGHT_CH0, DAC_VCHANRIGHT0, 0.1, 0.9);

  std::cout << "Channel: CH1_R" << std::endl;
  Test1.test_ILock("CH1_R", AMAC_ADC_RIGHT_CH1);

  std::cout << "Channel: CH2_R" << std::endl;
  Test1.test_ILock("CH2_R", AMAC_ADC_RIGHT_CH2);

  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL,  0);
  std::cout << "Channel: CH3_R_GAIN1" << std::endl;
  Test1.test_ILock("CH3_R_GAIN1", AMAC_ADC_RIGHT_CH3, DAC_VCHANRIGHT3, 0.1, 0.9);

  TestBoard.writeAMAC(AMACreg::RT_CH3_GAIN_SEL,  1);
  std::cout << "Channel: CH3_R_GAIN23" << std::endl;
  Test1.test_ILock("CH3_R_GAIN23", AMAC_ADC_RIGHT_CH3, DAC_VCHANRIGHT3, 0.1, 0.9);

  std::cout << "Channel: CH4_R" << std::endl;
  Test1.test_ILock("CH4_R", AMAC_ADC_RIGHT_CH4);

  std::cout << "Channel: CH5_R" << std::endl;
  Test1.test_ILock("CH5_R", AMAC_ADC_RIGHT_CH5);




  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;
	
  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();
  exit(0);    	
}
