////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF2_test");
  std::vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  while(TestBoard.setVCC_IO((float)2.2));

  //Current Test
  std::cout << "====== Current Test ======" << std::endl;

  std::string logpath = "log/currentRI.log";
  std::fstream logfile(logpath, std::fstream::out);
  logfile << "InputCurrent BandgapControl RampGain OpAmpGain Resistor ADCvalue" << std::endl;

  TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 10);
  TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN , 3);
  TestBoard.writeAMAC(AMACreg::OPAMP_GAIN_LEFT, 5);

  double actual_val, min_val=0, max_val, step;
  int result;

  TestBoard.enableCurrent(true);
  for(int RTidx=1;RTidx<=3;RTidx++)
    {
      switch(RTidx)
	{
	case 1:
	  max_val=3e-3;
	  step=1e-5;
	  break;
	case 2:
	  max_val=3e-4;
	  step=1e-6;
	  break;
	case 3:
	  max_val=3e-6;
	  step=1e-7;
	  break;
	default:
	  max_val=0;
	  step=0;
	  break;
	}

      for(double target_val = min_val; target_val <= max_val; target_val += step)
	{
	  TestBoard.setCurrent(true, RTidx, target_val);
	  TestBoard.getCurrent(true, actual_val);

	  TestBoard.readAMAC(AMACreg::VALUE_LEFT_CH6, result);
	  logfile << actual_val << " " << 10 << " " << 3 << " " << 5 << " " << RTidx << " " << result << std::endl;
	}
    }
  TestBoard.enableCurrent(false);

  // std::cout << "--LEFT" << std::endl;
  // Test1.calib_current_measurement	("LEFT", LOG_NEW,
  // 					 0, 3e-6, 1e-7, 200, 
  // 					 true);
  // Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  // 					 0, 3e-4, 1e-5, 200, 
  // 					 true);
  // Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  // 					 0, 20e-3, 1e-4, 200, 
  // 					 true);
      
  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;

  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();
  exit(0);    	
}
