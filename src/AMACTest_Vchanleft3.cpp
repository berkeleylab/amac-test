#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

using namespace std;

void cleanup()
{
  /*vector<AMACTest*> all_tests;

    for(AMACTest* t : AMACTest::activeTests) all_tests.push_back(t);
    for(AMACTest* t : all_tests) delete t;*/
}

void siginthandler(int sig)
{
  cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyACM0");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("VoltageTest_DVDDsupply");
  vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  cout << "====== InitTest: " << Test1.Init() << " ======" << endl;

  //Voltage Test
  cout << "====== Voltage Test ======" << endl;
  cout << "Should be measured for VDD supply as well" << endl;
  /*//Produce one reference curve
  cout << "Reference Curve" << endl;
  while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
  while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0));
  while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 0));
  Test1.test_ADC_ext	("CH3_L_Reference", LOG_NEW, "limits/ADC_dummy.dat", 
			 0, AMAC_1V2, 10e-3, 200,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  //Bandgap Dependance
  cout << "Bandgap Dependance" << endl;
  while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
  while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0));
  for(int i = 0; i <= 16; i++){
    cout << "--Set Bandgap Register to: " << i << endl;
    while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, i));
    Test1.test_ADC_ext	("CH3_L_BGO" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
			 0, AMAC_1V2, 10e-3, 200,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  }

  //Gain Dependance
  cout << "Gain Dependance" << endl;
  while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 0));
  while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, 0));
  for(int i = 0; i < 4; i++){
    cout << "Set Ramp Gain to: " << i << endl;
    while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, i));
    Test1.test_ADC_ext	("CH3_L_RampGain" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
			 0, AMAC_1V2, 10e-3, 200,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
			 }*/
  
  //Attenuation Dependance
  cout << "Attenuation Dependance" << endl;
  while(TestBoard.writeAMAC(AMACreg::BANDGAP_CONTROL, 0));
  while(TestBoard.writeAMAC(AMACreg::LEFT_RAMP_GAIN, 1));
  for(int i = 0; i < 2; i++){
    cout << "Set Attenuation to: " << i << endl;
    while(TestBoard.writeAMAC(AMACreg::LT_CH3_GAIN_SEL, i));
    Test1.test_ADC_ext	("CH3_L_Attenuation" + to_string(i), LOG_NEW, "limits/ADC_dummy.dat", 
			 0, AMAC_1V2*1.5, 10e-3, 200,
			 DAC_VCHANLEFT3, AMACreg::VALUE_LEFT_CH3);
  }


  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  cout << "====== Evaluation ======" << endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	cout << "PASSED ";
  else		cout << "FAILED ";
  cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << endl;
	
  //Check for SW Errors
  vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  cout << "[" << SWErrors.size() << " SW Errors occured]" << endl;

  //End of Program
  TestBoard.PowerDown();
  cleanup();
  exit(0);    	
}
