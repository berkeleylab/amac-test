////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMAC_TB.h
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef AMAC_TB_H
#define AMAC_TB_H

#include <iostream>
#include <string>
#include <unistd.h>

#include "SerialCom.h"
#include "MOJO.h"
#include "AMAC_TB_regs.h"

//Debugging
//#define AMAC_TB_DEBUG

//AMAC registers
enum class AMACreg{
	STATUS_ID,
	STATUS_HV_ILOCK,
	STATUS_LV_ILOCK,
	STATUS_HV_WARN,
	STATUS_LV_WARN,
	HV_ILOCK_LEFT_HIGH,
	HV_ILOCK_LEFT_LOW,
	HV_ILOCK_RIGHT_HIGH,
	HV_ILOCK_RIGHT_LOW,
	LV_ILOCK_LEFT_HIGH,
	LV_ILOCK_LEFT_LOW,
	LV_ILOCK_RIGHT_HIGH,
	LV_ILOCK_RIGHT_LOW,
	HV_WARN_LEFT_HIGH,
	HV_WARN_LEFT_LOW,
	HV_WARN_RIGHT_HIGH,
	HV_WARN_RIGHT_LOW,
	LV_WARN_LEFT_HIGH,
	LV_WARN_LEFT_LOW,
	LV_WARN_RIGHT_HIGH,
	LV_WARN_RIGHT_LOW,
	VALUE_LEFT_CH0, 
	VALUE_LEFT_CH1, 
	VALUE_LEFT_CH2, 
	VALUE_LEFT_CH3, 
	VALUE_LEFT_CH4, 
	VALUE_LEFT_CH5, 
	VALUE_LEFT_CH6, 
	VALUE_RIGHT_CH0, 
	VALUE_RIGHT_CH1,
	VALUE_RIGHT_CH2,
	VALUE_RIGHT_CH3,
	VALUE_RIGHT_CH4,
	VALUE_RIGHT_CH5,
	VALUE_RIGHT_CH6,
	OPAMP_GAIN_RIGHT,
	OPAMP_GAIN_LEFT,
	RIGHT_RAMP_GAIN,
	LEFT_RAMP_GAIN,
	BANDGAP_CONTROL,
	RT_CH0_SEL,
	LT_CH0_SEL,
	RT_CH3_GAIN_SEL,
	LT_CH3_GAIN_SEL,
	CLK_OUT_ENABLE,
	CLK_SELECT,
	HV_FREQ,
	HV_ENABLE,
	LV_ENABLE,
	HV_ILOCK,
	HV_ILOCK_LAM,
	LV_ILOCK,
	LV_ILOCK_LAM,
	RESET_HV_ILOCK,
	RESET_HV_WARN,
	RESET_LV_ILOCK,
	RESET_LV_WARN,
	ILOCK_FLAG_HV_LEFT_HI,
	ILOCK_FLAG_HV_LEFT_LO,
	ILOCK_FLAG_HV_RIGHT_HI,
	ILOCK_FLAG_HV_RIGHT_LO,
	ILOCK_FLAG_LV_LEFT_HI,
	ILOCK_FLAG_LV_LEFT_LO,
	ILOCK_FLAG_LV_RIGHT_HI,
	ILOCK_FLAG_LV_RIGHT_LO,
	WARN_FLAG_HV_LEFT_HI,
	WARN_FLAG_HV_LEFT_LO,
	WARN_FLAG_HV_RIGHT_HI,
	WARN_FLAG_HV_RIGHT_LO,
	WARN_FLAG_LV_LEFT_HI,
	WARN_FLAG_LV_LEFT_LO,
	WARN_FLAG_LV_RIGHT_HI,
	WARN_FLAG_LV_RIGHT_LO,
	ILOCK_EN_HV_LEFT_HI,
	ILOCK_EN_HV_LEFT_LO,
	ILOCK_EN_HV_RIGHT_HI,
	ILOCK_EN_HV_RIGHT_LO,
	ILOCK_EN_LV_LEFT_HI,
	ILOCK_EN_LV_LEFT_LO,
	ILOCK_EN_LV_RIGHT_HI,
	ILOCK_EN_LV_RIGHT_LO,
	WARN_EN_HV_LEFT_HI,
	WARN_EN_HV_LEFT_LO,
	WARN_EN_HV_RIGHT_HI,
	WARN_EN_HV_RIGHT_LO,
	WARN_EN_LV_LEFT_HI,
	WARN_EN_LV_LEFT_LO,
	WARN_EN_LV_RIGHT_HI,
	WARN_EN_LV_RIGHT_LO,
	ILOCK_HV_THRESH_HI_L_CH0,
	ILOCK_HV_THRESH_HI_L_CH1,
	ILOCK_HV_THRESH_HI_L_CH2,
	ILOCK_HV_THRESH_HI_L_CH3,
	ILOCK_HV_THRESH_HI_L_CH4,
	ILOCK_HV_THRESH_HI_L_CH5,
	ILOCK_HV_THRESH_HI_L_CH6,
	ILOCK_HV_THRESH_LO_L_CH0,
	ILOCK_HV_THRESH_LO_L_CH1,
	ILOCK_HV_THRESH_LO_L_CH2,
	ILOCK_HV_THRESH_LO_L_CH3,
	ILOCK_HV_THRESH_LO_L_CH4,
	ILOCK_HV_THRESH_LO_L_CH5,
	ILOCK_HV_THRESH_LO_L_CH6,
	ILOCK_HV_THRESH_HI_R_CH0,
	ILOCK_HV_THRESH_HI_R_CH1,
	ILOCK_HV_THRESH_HI_R_CH2,
	ILOCK_HV_THRESH_HI_R_CH3,
	ILOCK_HV_THRESH_HI_R_CH4,
	ILOCK_HV_THRESH_HI_R_CH5,
	ILOCK_HV_THRESH_HI_R_CH6,
	ILOCK_HV_THRESH_LO_R_CH0,
	ILOCK_HV_THRESH_LO_R_CH1,
	ILOCK_HV_THRESH_LO_R_CH2,
	ILOCK_HV_THRESH_LO_R_CH3,
	ILOCK_HV_THRESH_LO_R_CH4,
	ILOCK_HV_THRESH_LO_R_CH5,
	ILOCK_HV_THRESH_LO_R_CH6,
	ILOCK_LV_THRESH_HI_L_CH0,
	ILOCK_LV_THRESH_HI_L_CH1,
	ILOCK_LV_THRESH_HI_L_CH2,
	ILOCK_LV_THRESH_HI_L_CH3,
	ILOCK_LV_THRESH_HI_L_CH4,
	ILOCK_LV_THRESH_HI_L_CH5,
	ILOCK_LV_THRESH_HI_L_CH6,
	ILOCK_LV_THRESH_LO_L_CH0,
	ILOCK_LV_THRESH_LO_L_CH1,
	ILOCK_LV_THRESH_LO_L_CH2,
	ILOCK_LV_THRESH_LO_L_CH3,
	ILOCK_LV_THRESH_LO_L_CH4,
	ILOCK_LV_THRESH_LO_L_CH5,
	ILOCK_LV_THRESH_LO_L_CH6,
	ILOCK_LV_THRESH_HI_R_CH0,
	ILOCK_LV_THRESH_HI_R_CH1,
	ILOCK_LV_THRESH_HI_R_CH2,
	ILOCK_LV_THRESH_HI_R_CH3,
	ILOCK_LV_THRESH_HI_R_CH4,
	ILOCK_LV_THRESH_HI_R_CH5,
	ILOCK_LV_THRESH_HI_R_CH6,
	ILOCK_LV_THRESH_LO_R_CH0,
	ILOCK_LV_THRESH_LO_R_CH1,
	ILOCK_LV_THRESH_LO_R_CH2,
	ILOCK_LV_THRESH_LO_R_CH3,
	ILOCK_LV_THRESH_LO_R_CH4,
	ILOCK_LV_THRESH_LO_R_CH5,
	ILOCK_LV_THRESH_LO_R_CH6,
	WARN_HV_THRESH_HI_L_CH0,
	WARN_HV_THRESH_HI_L_CH1,
	WARN_HV_THRESH_HI_L_CH2,
	WARN_HV_THRESH_HI_L_CH3,
	WARN_HV_THRESH_HI_L_CH4,
	WARN_HV_THRESH_HI_L_CH5,
	WARN_HV_THRESH_HI_L_CH6,
	WARN_HV_THRESH_LO_L_CH0,
	WARN_HV_THRESH_LO_L_CH1,
	WARN_HV_THRESH_LO_L_CH2,
	WARN_HV_THRESH_LO_L_CH3,
	WARN_HV_THRESH_LO_L_CH4,
	WARN_HV_THRESH_LO_L_CH5,
	WARN_HV_THRESH_LO_L_CH6,
	WARN_HV_THRESH_HI_R_CH0,
	WARN_HV_THRESH_HI_R_CH1,
	WARN_HV_THRESH_HI_R_CH2,
	WARN_HV_THRESH_HI_R_CH3,
	WARN_HV_THRESH_HI_R_CH4,
	WARN_HV_THRESH_HI_R_CH5,
	WARN_HV_THRESH_HI_R_CH6,
	WARN_HV_THRESH_LO_R_CH0,
	WARN_HV_THRESH_LO_R_CH1,
	WARN_HV_THRESH_LO_R_CH2,
	WARN_HV_THRESH_LO_R_CH3,
	WARN_HV_THRESH_LO_R_CH4,
	WARN_HV_THRESH_LO_R_CH5,
	WARN_HV_THRESH_LO_R_CH6,
	WARN_LV_THRESH_HI_L_CH0,
	WARN_LV_THRESH_HI_L_CH1,
	WARN_LV_THRESH_HI_L_CH2,
	WARN_LV_THRESH_HI_L_CH3,
	WARN_LV_THRESH_HI_L_CH4,
	WARN_LV_THRESH_HI_L_CH5,
	WARN_LV_THRESH_HI_L_CH6,
	WARN_LV_THRESH_LO_L_CH0,
	WARN_LV_THRESH_LO_L_CH1,
	WARN_LV_THRESH_LO_L_CH2,
	WARN_LV_THRESH_LO_L_CH3,
	WARN_LV_THRESH_LO_L_CH4,
	WARN_LV_THRESH_LO_L_CH5,
	WARN_LV_THRESH_LO_L_CH6,
	WARN_LV_THRESH_HI_R_CH0,
	WARN_LV_THRESH_HI_R_CH1,
	WARN_LV_THRESH_HI_R_CH2,
	WARN_LV_THRESH_HI_R_CH3,
	WARN_LV_THRESH_HI_R_CH4,
	WARN_LV_THRESH_HI_R_CH5,
	WARN_LV_THRESH_HI_R_CH6,
	WARN_LV_THRESH_LO_R_CH0,
	WARN_LV_THRESH_LO_R_CH1,
	WARN_LV_THRESH_LO_R_CH2,
	WARN_LV_THRESH_LO_R_CH3,
	WARN_LV_THRESH_LO_R_CH4,
	WARN_LV_THRESH_LO_R_CH5,
	WARN_LV_THRESH_LO_R_CH6,
	};

// Clock Input channels
#define CLK_CHANNEL_CLKOUT 0
#define CLK_CHANNEL_HVCNTRL 1
#define CLK_CHANNEL_HVCNTRLB 2

// Board individual Parameters
#define MOJO_CLK_FREQUENCY 100000000
#define MOJO_3V3 3.28
#define AMAC_1V2 1.194
#define VCC_IO_MAX 2.992
#define VCC_IO_MIN 0.612

// MOJO ADC channels
#define MOJO_ADC_ISENSE_1V2 0
#define MOJO_ADC_ISENSE_1V5 1
#define MOJO_ADC_OTA 4
#define MOJO_ADC_NTCPWR 5
#define MOJO_ADC_BGO 6
#define MOJO_ADC_VCC_IO 7
#define MOJO_ADC_1V2 8

//DAC channels mapped to AMAC ADCs
#define DAC_VCHANLEFT0 0
#define DAC_VCHANLEFT3 11
#define DAC_VCHANRIGHT0 1
#define DAC_VCHANRIGHT1 2
#define DAC_VCHANRIGHT2 3
#define DAC_VCHANRIGHT3 10

class AMAC_TB{
public:
  AMAC_TB();
  AMAC_TB(std::string serial_port);
  ~AMAC_TB();

  int Init();
  bool isConnected();

  int PowerDown();
  int PowerUp();
  int Restart();
  int setReset(bool reset_active);
  int doReset();
  int setVCC_IO(int Vset);
  int setVCC_IO(double value);
  int getVCC_IO(double &value);

  int I2C_reset();
  int I2C_enable();
  int I2C_writeBytes(int num, int* Bytes);
  int I2C_readBytes(int slave_addr, int reg_addr, int num, int* &Output);
  int I2C_write(int addr, int num, int* Bytes);
  int I2C_read(int addr, int num, int* &Buf);

  int setDAC(int channel, int value);
  int setDACvoltage(int channel, double voltage);
  int getDACvoltage(int channel, double &voltage);
  int setCurrent(bool left_channel, double value);
  int setCurrent(bool left_channel, int rchannel, double value);
  int setCurrent(bool left_channel, int rchannel, int value);
  int enableCurrent(bool enable);
  int getCurrent(bool left_Channel, double &value);
  int getADC(int channel, int &value);
  int getADCvoltage(int channel, double &value);
  int getADCcurrent(int channel, double &value);

  int readAMAC(AMACreg reg, int &val);
  int writeAMAC(AMACreg reg, int val);
  int writeAMACreg(int reg, int val);

  int ClkOut_analysis(int channel, int timespan_ms, int &frequency, int &DutyCycle);
  int set_ClkIn(int frequency);
  int get_AMACout_HVCTRL(int &HVCTRL, int &HVCTRLB);
  int get_AMACout_LVCTRL(int &LVCTRL, int &LVCTRLB);
  int get_AMACout_LAM_pulses(int &ILOCKLAM, int &WARNLAM);
  int HVCNTRL_isOn(bool &isOn);
  int LVCNTRL_isOn(bool &isOn);
  int setAMAC_ID(int _ID);

private:
  int readMojoReg(int regnr, int &regvalue);
  int writeMojoReg(int regnr, int regvalue);
  int AMAC_writeBits(int startreg, int num_bits, int offset, int value);
  int AMAC_readBits(int startreg, int num_bits, int offset, int &value);
  Mojo* MOJO;
  std::string serial_port;
  int ID = 0;
  int Reg_RW_Max_Attempts = 5;
  int VCC_IO_Vset = 0x400;
  int DAC_values[12];
  int Current_channels[2];
  int Current_values[2];
};

#endif
