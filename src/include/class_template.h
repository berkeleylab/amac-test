////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    class_template.h
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef CLASSES_H
#define CLASSES_H

class ComInterface {
    	public:
		virtual void init() = 0;
		virtual int write(char *buf, size_t length) = 0;
		virtual int read(char *buf, size_t length) = 0;
		virtual int flush() = 0;
		virtual bool isConnected() = 0;
};

class ControlInterface {
   	public:
		virtual int init() = 0;
		virtual int resetCom() = 0;
		virtual int writeReg(int reg, int val) = 0;
		virtual int readReg(int reg, int &val) = 0;
		virtual bool isConnected() = 0;
};


#endif
