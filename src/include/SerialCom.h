////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    SerialCom.h
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <iostream>
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>      
#include <errno.h>

#include <class_template.h>

class SerialCom: public ComInterface
{
public:
  SerialCom();
  SerialCom(std::string deviceName);
  ~SerialCom();
	    
  void set_deviceName(std::string deviceName);       

  //Inherited from ComInterface
  void init();
  int write(char *buf, size_t length);
  int read(char *buf, size_t length);
  int flush();
  bool isConnected();

private:	
  void config();

  bool _isConnected = false;
  unsigned int timeout_ms = 200;
  std::string deviceName;

  int dev;
  speed_t baudrate;
  struct termios tty;
  struct termios tty_old;
};

#endif
