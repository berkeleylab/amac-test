////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    MOJO.h
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef MOJO_H
#define MOJO_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>

#include <class_template.h>
#include <SerialCom.h>

//#define MOJO_DEBUG

class Mojo: public ControlInterface
{
public:
  Mojo();
  ~Mojo();

  void setCom(ComInterface* _Com);

  //Inherited from ComInterface
  int init();
  int resetCom();
  int writeReg(int reg, int val);
  int readReg(int reg, int &val);
  bool isConnected();
private:
  ComInterface* Com;
  bool Communication_established = false;
  int Com_maxAttempts = 4;
};

#endif
