////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest.h
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef AMACTEST_H
#define AMACTEST_H

#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <signal.h>
#include "AMAC_TB.h"

#define VERBOSE
#define DEBUG

#define LOG_NEW false
#define LOG_APPEND true

class AMACTest{
public:
  static std::vector<AMACTest*> activeTests;
  AMACTest();
  AMACTest(const std::string& name);
  ~AMACTest();
  void setTestBoard(AMAC_TB* TB);
  void setErrorLog(bool log);
  void clearCriticalErrors();
  void addCriticalErrors(std::vector<int> criticals);
  void addCriticalErrors(int range_start, int range_stop);
  int Init();
  int getErrors(std::vector<int> &_errors);
  int getSWErrors(std::vector<int> &_errors);
  int evaluateTest(bool &passed, int &num_Errors, int &num_Criticals);

  void calib_ADC_ext	        (const std::string& name, bool log_append,
				 double start_val, double stop_val, double Stepsize, int Step_delay_ms,
				 int DAC_ch, AMACreg ADC_ch);
  void noise_ADC_ext            (const std::string& name, bool log_append,
				 double target_voltage, uint nReads,
				 int BGC, int RG,
				 int DAC_ch, AMACreg ADC_ch);
  void calib_current_measurement(const std::string& name, bool log_append,
				 double start_val, double stop_val, double Stepsize, int Step_delay_ms, int rchannel,
				 bool left_channel);

  void test_ADC_ext	        (const std::string& name, bool log_append, const std::string& limits_file, 
				 double start_val, double stop_val, double Stepsize, int Step_delay_ms, 
				 int DAC_ch, AMACreg readout_ch);

  void calib_ADC_int	        (const std::string& name, bool log_append,
				 int DAC_ch, AMACreg ADC_ch);

  void test_LVCTRL	        (const std::string& name);
  void test_current_measurement	(const std::string& name, bool log_append, const std::string& limits_file, 
				 double start_val, double stop_val, double Stepsize, int Step_delay_ms, 
				 bool left_channel);
  void test_I2C_reliability	(const std::string& name, bool log_append, const std::string& limits_file, 
				 double start_val, double stop_val, double Stepsize, 
				 int num_attempts_per_val);
  void test_clock_and_HVCTRL	(const std::string& name, const std::string& limits_file, 
				 int clock_eval_timespan);
  void test_ILock		(const std::string& name,
				 int channel);
  void test_ILock               (const std::string& name,
				 int channel, int dac,
				 double minDAC, double maxDAC);
  void test_Voltage_f_BG	(const std::string& name, const std::string& limits_file, 
				 int ADC_channel, int measurements_per_Step, int measurement_delay_ms,
				 int Step_delay_ms);
  void test_ADC_f_BG            (const std::string& name, const std::string& limits_file, 
				 AMACreg ADC_channel, int measurements_per_Step, int measurement_delay_ms,
				 int Step_delay_ms);
  void readParam_average	(const std::string& name, bool log_append, const std::string& limits_file, 
				 AMACreg param, int num_readouts, int delay_ms);
  void measureParam_average	(const std::string& name, bool log_append, const std::string& limits_file, 
				 int ADC_channel, int num_readouts, int delay_ms);
  void test_CurrentConsumption	(const std::string& name, const std::string& limits_file, 
				 int ADC_channel, int num_measurements, int measurement_delay_ms);
private:
  std::string TestName;
  bool log_errors = false;
  AMAC_TB* TestBoard;
  std::vector<int> Errors;
  std::vector<int> criticalErrors;
  std::vector<int> SWErrors;
  //static void siginthandler(int sig);
  void setError(int error_nr);
  void setSWError(int error_nr);
  void printError(int error_nr);
  int getLimits(const std::string& limits_path, double input, double &min, double &max);
  int getLimitsByIndex(const std::string& limits_path, int index, int &min, int &max);
};

// ILOCK test Macros
#define ILOCK_LEFT(channel) (0 + channel)
#define ILOCK_RIGHT(channel) (7 + channel)
#define isLeftChannel(channel) (channel < 7)
#define isLeftChannel_int(channel) (channel / 7)
#define getChannelNo(channel) (channel%7)
#define ILOCK_FLAGS(channel) (1<<getChannelNo(channel))

//ADC channels
#define AMAC_ADC_LEFT_CH0 0
#define AMAC_ADC_LEFT_CH1 1
#define AMAC_ADC_LEFT_CH2 2
#define AMAC_ADC_LEFT_CH3 3
#define AMAC_ADC_LEFT_CH4 4
#define AMAC_ADC_LEFT_CH5 5
#define AMAC_ADC_LEFT_CH6 6
#define AMAC_ADC_RIGHT_CH0 7
#define AMAC_ADC_RIGHT_CH1 8
#define AMAC_ADC_RIGHT_CH2 9
#define AMAC_ADC_RIGHT_CH3 10
#define AMAC_ADC_RIGHT_CH4 11
#define AMAC_ADC_RIGHT_CH5 12
#define AMAC_ADC_RIGHT_CH6 13

//AMAC registers sorted by channel
const AMACreg ADC_VALUE[] = { 
	AMACreg::VALUE_LEFT_CH0,
	AMACreg::VALUE_LEFT_CH1,
	AMACreg::VALUE_LEFT_CH2,
	AMACreg::VALUE_LEFT_CH3,
	AMACreg::VALUE_LEFT_CH4,
	AMACreg::VALUE_LEFT_CH5,
	AMACreg::VALUE_LEFT_CH6,
	AMACreg::VALUE_RIGHT_CH0,
	AMACreg::VALUE_RIGHT_CH1,
	AMACreg::VALUE_RIGHT_CH2,
	AMACreg::VALUE_RIGHT_CH3,
	AMACreg::VALUE_RIGHT_CH4,
	AMACreg::VALUE_RIGHT_CH5,
	AMACreg::VALUE_RIGHT_CH6};
const AMACreg ILOCK_EN_HV_HI[] = {AMACreg:: ILOCK_EN_HV_LEFT_HI, AMACreg::ILOCK_EN_HV_RIGHT_HI};
const AMACreg ILOCK_EN_HV_LO[] = {AMACreg:: ILOCK_EN_HV_LEFT_LO, AMACreg::ILOCK_EN_HV_RIGHT_LO};
const AMACreg ILOCK_EN_LV_HI[] = {AMACreg:: ILOCK_EN_LV_LEFT_HI, AMACreg::ILOCK_EN_LV_RIGHT_HI};
const AMACreg ILOCK_EN_LV_LO[] = {AMACreg:: ILOCK_EN_LV_LEFT_LO, AMACreg::ILOCK_EN_LV_RIGHT_LO};
const AMACreg ILOCK_FLAG_HV_HI[] = {AMACreg:: ILOCK_FLAG_HV_LEFT_HI, AMACreg::ILOCK_FLAG_HV_RIGHT_HI};
const AMACreg ILOCK_FLAG_HV_LO[] = {AMACreg:: ILOCK_FLAG_HV_LEFT_LO, AMACreg::ILOCK_FLAG_HV_RIGHT_LO};
const AMACreg ILOCK_FLAG_LV_HI[] = {AMACreg:: ILOCK_FLAG_LV_LEFT_HI, AMACreg::ILOCK_FLAG_LV_RIGHT_HI};
const AMACreg ILOCK_FLAG_LV_LO[] = {AMACreg:: ILOCK_FLAG_LV_LEFT_LO, AMACreg::ILOCK_FLAG_LV_RIGHT_LO};
const AMACreg ILOCK_THRESHOLD_HV_HI[] = {
	AMACreg::ILOCK_HV_THRESH_HI_L_CH0,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH1,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH2,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH3,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH4,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH5,
	AMACreg::ILOCK_HV_THRESH_HI_L_CH6,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH0,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH1,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH2,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH3,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH4,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH5,
	AMACreg::ILOCK_HV_THRESH_HI_R_CH6};
const AMACreg ILOCK_THRESHOLD_HV_LO[] = {
	AMACreg::ILOCK_HV_THRESH_LO_L_CH0,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH1,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH2,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH3,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH4,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH5,
	AMACreg::ILOCK_HV_THRESH_LO_L_CH6,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH0,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH1,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH2,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH3,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH4,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH5,
	AMACreg::ILOCK_HV_THRESH_LO_R_CH6};
const AMACreg ILOCK_THRESHOLD_LV_HI[] = {
	AMACreg::ILOCK_LV_THRESH_HI_L_CH0,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH1,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH2,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH3,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH4,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH5,
	AMACreg::ILOCK_LV_THRESH_HI_L_CH6,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH0,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH1,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH2,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH3,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH4,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH5,
	AMACreg::ILOCK_LV_THRESH_HI_R_CH6};
const AMACreg ILOCK_THRESHOLD_LV_LO[] = {
	AMACreg::ILOCK_LV_THRESH_LO_L_CH0,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH1,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH2,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH3,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH4,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH5,
	AMACreg::ILOCK_LV_THRESH_LO_L_CH6,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH0,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH1,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH2,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH3,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH4,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH5,
	AMACreg::ILOCK_LV_THRESH_LO_R_CH6};


#endif
