#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

using namespace std;

void cleanup()
{
  /*vector<AMACTest*> all_tests;
	
    for(AMACTest* t : AMACTest::activeTests) all_tests.push_back(t);
    for(AMACTest* t : all_tests) delete t;*/
}

void siginthandler(int sig)
{
  cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyACM0");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF1");
  vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);
  cout << "====== InitTest: " << Test1.Init() << " ======" << endl;

  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  while(TestBoard.setVCC_IO((float)3.0));

  TestBoard.doReset();

  //	Enable outputs
  cout << "Enable Outputs" << endl;
  TestBoard.writeAMAC(AMACreg::HV_ENABLE, 1);
  TestBoard.writeAMAC(AMACreg::LV_ENABLE, 1);

  int i = 0;
  while(i++ < 100) usleep(100000);

  /*//	Enable LAM outputs
    cout << "Enable LAMS" << endl;
    if(TestBoard.writeAMAC(AMACreg::HV_ILOCK_LAM, 1)) setSWError(6041);
    if(TestBoard.writeAMAC(AMACreg::LV_ILOCK_LAM, 1)) setSWError(6042);
    //	Enable ILocks
    cout << "Enable ILOCKS" << endl;
    if(TestBoard.writeAMAC(AMACreg::HV_ILOCK, 1)) setSWError(6043);
    if(TestBoard.writeAMAC(AMACreg::LV_ILOCK, 1)) setSWError(6044);

    sleep(1);

    cout << "Enable Flags" << endl;
    if(TestBoard.writeAMAC(ILOCK_EN_HV_HI[isLeftChannel_int(0)], ILOCK_FLAGS(0))) setSWError(6002);
    if(TestBoard.writeAMAC(ILOCK_EN_HV_LO[isLeftChannel_int(0)], ILOCK_FLAGS(0))) setSWError(6003);
    if(TestBoard.writeAMAC(ILOCK_EN_LV_HI[isLeftChannel_int(0)], ILOCK_FLAGS(0))) setSWError(6004);
    if(TestBoard.writeAMAC(ILOCK_EN_LV_LO[isLeftChannel_int(0)], ILOCK_FLAGS(0))) setSWError(6005);

    sleep(1);
    cout << "Set Thresholds" << endl;
    if(TestBoard.writeAMAC(ILOCK_THRESHOLD_HV_HI[0], 0)) setSWError(6031);
    if(TestBoard.writeAMAC(ILOCK_THRESHOLD_LV_HI[0], 0)) setSWError(6032);*/

  TestBoard.PowerDown();
  exit(0); 

  // ILock Test
  cout << "====== ILock Test ======" << endl;
  for(int i = AMAC_ADC_LEFT_CH0; i <= AMAC_ADC_RIGHT_CH6; i++) {
    cout << "Channel: " << i << endl;
    Test1.test_ILock	("noDACset_CH" + to_string(i), 
			 i);
  }

  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  cout << "====== Evaluation ======" << endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	cout << "PASSED ";
  else		cout << "FAILED ";
  cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << endl;
	
  //Check for SW Errors
  vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  cout << "[" << SWErrors.size() << " SW Errors occured]" << endl;

  //End of Program
  TestBoard.PowerDown();
  cleanup();
  exit(0);    	
}
