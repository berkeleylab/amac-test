////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    AMACTest_full.cpp
////////////////////////////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

void siginthandler(int sig)
{
  //cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  std::string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("AMACREF1");
  std::vector<int> Test1_criticalErrors = {2001, 2002, 5001, 5002, 5003, 5004};
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  std::cout << "====== InitTest: " << Test1.Init() << " ======" << std::endl;

  //Workaround
  // Clock and CTRL outputs do not work properly when VCC_H > 2V
  // But VCC_H needs to be > 2V so I2C communication always works
  while(TestBoard.setVCC_IO((float)2.5));

  //Current Test
  std::cout << "====== Current Test ======" << std::endl;

  std::cout << "--LEFT" << std::endl;
  Test1.calib_current_measurement	("LEFT", LOG_NEW,
  					 0, 3.3e-6, 3.3e-8, 1, 3,
  					 true);
  Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  					 0, 3.3e-4, 3.3e-6, 1, 2,
  					 true);
  Test1.calib_current_measurement	("LEFT", LOG_APPEND,
  					 0, 20e-3, 20e-5, 1, 1,
  					 true);

  std::cout << "--RIGHT" << std::endl;
  Test1.calib_current_measurement	("RIGHT", LOG_NEW,
  					 0, 3.3e-6, 3.3e-8, 1, 3,
  					 false);
  Test1.calib_current_measurement	("RIGHT", LOG_APPEND,
  					 0, 3.3e-4, 3.3e-6, 1, 2,
  					 false);
  Test1.calib_current_measurement	("RIGHT", LOG_APPEND,
  					 0, 20e-3, 20e-5, 1, 1,
  					 false);
      
  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  std::cout << "====== Evaluation ======" << std::endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	std::cout << "PASSED ";
  else		std::cout << "FAILED ";
  std::cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << std::endl;

  //Check for SW Errors
  std::vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  std::cout << "[" << SWErrors.size() << " SW Errors occured]" << std::endl;

  //End of Program
  TestBoard.PowerDown();
  exit(0);    	
}
