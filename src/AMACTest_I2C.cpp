#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <cmath>
#include <chrono>
#include <ctime>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

#include "AMAC_TB.h"
#include "AMACTest.h"

using namespace std;

void cleanup()
{
  /*vector<AMACTest*> all_tests;

    for(AMACTest* t : AMACTest::activeTests) all_tests.push_back(t);
    for(AMACTest* t : all_tests) delete t;*/
}

void siginthandler(int sig)
{
  cleanup();
  exit(-2);
}

int main(int argc, char *argv[])
{
  string logpath;

  //Connect to TestBoard
  AMAC_TB TestBoard("/dev/ttyUSB1");
  if(!TestBoard.isConnected()) exit(-1);

  //Signal to exit program
  signal(SIGINT, siginthandler);

  //Start Test
  AMACTest Test1("TEST");
  vector<int> Test1_criticalErrors = {2001, };
  Test1.setTestBoard(&TestBoard);
  Test1.setErrorLog(true);
  Test1.addCriticalErrors(Test1_criticalErrors);

  TestBoard.setVCC_IO((float)2.5);
  cout << "====== InitTest: " << Test1.Init() << " ======" << endl;

  cout << "====== I2C reliability Test ======" << endl;
  Test1.test_I2C_reliability	("", LOG_NEW, "limits/I2C_dummy.dat", 
  				 0.65, 2.95, 0.05,
  				 100);
  //Evaluate Test
  bool passed;
  int num_Errors, num_Criticals;
  cout << "====== Evaluation ======" << endl;
  Test1.evaluateTest(passed, num_Errors, num_Criticals);
  if(passed) 	cout << "PASSED ";
  else		cout << "FAILED ";
  cout << "(" << num_Errors << " Errors, " << num_Criticals << " critical)" << endl;

  //Check for SW Errors
  vector<int> SWErrors;
  Test1.getSWErrors(SWErrors);
  cout << "[" << SWErrors.size() << " SW Errors occured]" << endl;

  //End of Program
  TestBoard.PowerDown();
  cleanup();
  exit(0);    	
}
