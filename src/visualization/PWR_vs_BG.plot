set style line 11 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff6600'
set style line 12 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff9900'
set style line 13 lw 3 pt 1 ps 1 lt 1 lc rgb '#ffbb00'
set style line 14 lw 3 pt 2 ps 2 lt 1 lc rgb '#ffff00'
set style line 15 lw 3 pt 1 ps 1 lt 1 lc rgb '#aaff00'
set style line 16 lw 3 pt 1 ps 1 lt 1 lc rgb '#55ff00'
set style line 17 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff00'
set style line 18 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff55'
set style line 19 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffaa'
set style line 20 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffff'
set style line 21 lw 3 pt 1 ps 1 lt 1 lc rgb '#00aaff'
set style line 22 lw 3 pt 1 ps 1 lt 1 lc rgb '#0055ff'
set style line 23 lw 3 pt 2 ps 2 lt 1 lc rgb '#0000ff'
set style line 24 lw 3 pt 2 ps 2 lt 1 lc rgb '#5500ff'
set style line 25 lw 3 pt 1 ps 1 lt 1 lc rgb '#aa00ff'
set style line 26 lw 3 pt 1 ps 1 lt 1 lc rgb '#ff00ff'
set style line 27 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff00aa'
set style line 28 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff0055'

set terminal postscript enhanced color "Helvetica" 22 eps size 8,8
set output "Output/PWR_v_BG.eps"
set grid
set lmargin 10
set ytics
#set xrange [0:1.2]
#set key top left

set y2label "Standard Deviation"
set y2tics
set y2range [0:0.6]

set multiplot layout 4,1

set yrange [0:1.5]
set ylabel "Bandgap Voltage [V]"
plot \
         '../log/AMACREF1_BG_BGO.log' u 1:2 title "Voltage" ls 24 w linespoints, \
         '../log/AMACREF1_BG_BGO.log' u 1:3 title "StdDev" ls 27 w linespoints axes x1y2

set yrange [0:1.5]
set ylabel "VDD regulated [V]"
plot \
         '../log/AMACREF1_BG_1V2.log' u 1:2 title "Voltage" ls 24 w linespoints, \
         '../log/AMACREF1_BG_1V2.log' u 1:3 title "StdDev" ls 27 w linespoints axes x1y2

set yrange [0:1.5]
set ylabel "NTCPWR [V]"
plot \
         '../log/AMACREF1_BG_NTCPWR.log' u 1:2 title "Voltage" ls 24 w linespoints, \
         '../log/AMACREF1_BG_NTCPWR.log' u 1:3 title "StdDev" ls 27 w linespoints axes x1y2

set xlabel "BGO control register"
set yrange [0:1.5]
set ylabel "OTA [V]"
plot \
         '../log/AMACREF1_BG_OTA.log' u 1:2 title "Voltage" ls 24 w linespoints, \
         '../log/AMACREF1_BG_OTA.log' u 1:3 title "StdDev" ls 27 w linespoints axes x1y2

unset multiplot