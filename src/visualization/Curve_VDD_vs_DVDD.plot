set style line 11 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff6600'
set style line 12 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff9900'
set style line 13 lw 3 pt 1 ps 1 lt 1 lc rgb '#ffbb00'
set style line 14 lw 3 pt 2 ps 2 lt 1 lc rgb '#ffff00'
set style line 15 lw 3 pt 1 ps 1 lt 1 lc rgb '#aaff00'
set style line 16 lw 3 pt 1 ps 1 lt 1 lc rgb '#55ff00'
set style line 17 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff00'
set style line 18 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff55'
set style line 19 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffaa'
set style line 20 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffff'
set style line 21 lw 3 pt 1 ps 1 lt 1 lc rgb '#00aaff'
set style line 22 lw 3 pt 1 ps 1 lt 1 lc rgb '#0055ff'
set style line 23 lw 3 pt 2 ps 2 lt 1 lc rgb '#0000ff'
set style line 24 lw 3 pt 2 ps 2 lt 1 lc rgb '#5500ff'
set style line 25 lw 3 pt 1 ps 1 lt 1 lc rgb '#aa00ff'
set style line 26 lw 3 pt 1 ps 1 lt 1 lc rgb '#ff00ff'
set style line 27 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff00aa'
set style line 28 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff0055'

set terminal postscript enhanced color "Helvetica" 21 eps size 8,8
set output "Output/Vchanleft3_VDD_vs_DVDD.eps"
set grid
set lmargin 10
set ytics
set xlabel "Input Voltage [V]"
set ylabel "Register Value"
set yrange [0:1100]
set xrange [0:1.2]
set key top left

plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_BGO0.log' u 1:2 title "DVDD supply, BGO 0" ls 11 w lines, \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_BGO16.log' u 1:2 title "DVDD supply, BGO 16" ls 28 w lines, \
         '../log/VoltageTest_VDDsupply_ADC_V_CH3_L_BGO0.log' u 1:2 title "VDD supply, BGO 0" ls 13 w lines, \
         '../log/VoltageTest_VDDsupply_ADC_V_CH3_L_BGO16.log' u 1:2 title "VDD supply, BGO 16" ls 26 w lines, \


