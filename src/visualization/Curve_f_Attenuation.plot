set style line 11 lw 4 pt 1 ps 1 lt 1 lc rgb '#8800ff'
set style line 12 lw 4 pt 1 ps 1 lt 1 lc rgb '#ff0088'
set style line 13 lw 4 pt 1 ps 1 lt 1 lc rgb '#440088'
set style line 14 lw 4 pt 1 ps 1 lt 1 lc rgb '#880044'

set terminal postscript enhanced color "Helvetica" 21 eps size 8,8
set output "Output/Vchanleft3_f_Attenuation.eps"
set grid
set lmargin 10
set ytics
set title "CH3 Gain"
set xlabel "Input Voltage [V]"
set ylabel "Register Value"
set yrange [0:1100]
set xrange [0:2.1]
set key top left

plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_Attenuation0.log' u 1:2 title "No Gain" ls 11 w lines, \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_Attenuation1.log' u 1:2 title "Gain 2/3" ls 12 w lines, \



