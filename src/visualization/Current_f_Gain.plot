set style line 11 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff6600'
set style line 12 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff9900'
set style line 13 lw 3 pt 1 ps 1 lt 1 lc rgb '#ffbb00'
set style line 14 lw 3 pt 2 ps 2 lt 1 lc rgb '#ffff00'
set style line 15 lw 3 pt 1 ps 1 lt 1 lc rgb '#aaff00'
set style line 16 lw 3 pt 1 ps 1 lt 1 lc rgb '#55ff00'
set style line 17 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff00'
set style line 18 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ff55'
set style line 19 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffaa'
set style line 20 lw 3 pt 1 ps 1 lt 1 lc rgb '#00ffff'
set style line 21 lw 3 pt 1 ps 1 lt 1 lc rgb '#00aaff'
set style line 22 lw 3 pt 1 ps 1 lt 1 lc rgb '#0055ff'
set style line 23 lw 3 pt 2 ps 2 lt 1 lc rgb '#0000ff'
set style line 24 lw 3 pt 2 ps 2 lt 1 lc rgb '#5500ff'
set style line 25 lw 3 pt 1 ps 1 lt 1 lc rgb '#aa00ff'
set style line 26 lw 3 pt 1 ps 1 lt 1 lc rgb '#ff00ff'
set style line 27 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff00aa'
set style line 28 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff0055'

set terminal postscript enhanced color "Helvetica" 21 eps size 8,8
set output "Output/Current_f_Gain.eps"
set grid
set lmargin 10
set ytics
set xlabel "Current [mA]"
set ylabel "Register Value"
set autoscale
#set yrange [0:1100]
#set xrange [0:1.2]
set key bottom right

plot \
         '../log/Current_0_200u_GAIN0.dat' u ($1*1000):2 title "Gain 0 (0000)" ls 11 w lines, \
         '../log/Current_0_200u_GAIN1.dat' u ($1*1000):2 title "Gain 1 (0001)" ls 12 w lines, \
         '../log/Current_0_200u_GAIN2.dat' u ($1*1000):2 title "Gain 2 (0010)" ls 13 w lines, \
         '../log/Current_0_200u_GAIN3.dat' u ($1*1000):2 title "Gain 3 (0011)" ls 14 w lines, \
         '../log/Current_0_200u_GAIN4.dat' u ($1*1000):2 title "Gain 4 (0100)" ls 15 w lines, \
         '../log/Current_0_200u_GAIN5.dat' u ($1*1000):2 title "Gain 5 (0101)" ls 16 w lines, \
         '../log/Current_0_200u_GAIN6.dat' u ($1*1000):2 title "Gain 6 (0110)" ls 17 w lines, \
         '../log/Current_0_200u_GAIN7.dat' u ($1*1000):2 title "Gain 7 (0111)" ls 18 w lines, \
         '../log/Current_0_200u_GAIN8.dat' u ($1*1000):2 title "Gain 8 (1000)" ls 19 w lines, \
         '../log/Current_0_200u_GAIN9.dat' u ($1*1000):2 title "Gain 9 (1001)" ls 20 w lines, \
         '../log/Current_0_200u_GAIN10.dat' u ($1*1000):2 title "Gain 10 (1010)" ls 22 w lines, \
         '../log/Current_0_200u_GAIN11.dat' u ($1*1000):2 title "Gain 11 (1011)" ls 23 w lines, \
         '../log/Current_0_200u_GAIN12.dat' u ($1*1000):2 title "Gain 12 (1100)" ls 24 w lines, \
         '../log/Current_0_200u_GAIN13.dat' u ($1*1000):2 title "Gain 13 (1101)" ls 25 w lines, \
         '../log/Current_0_200u_GAIN14.dat' u ($1*1000):2 title "Gain 14 (1110)" ls 26 w lines, \
         '../log/Current_0_200u_GAIN15.dat' u ($1*1000):2 title "Gain 15 (1111)" ls 27 w lines, \

set output "Output/Current_f_Gain_Zoom.eps"
set xlabel "Current [uA]"
set yrange [0:500]
set xrange [0:2000]
set key center right
set arrow from 320,0 to 320,500 nohead lw 3 lc rgb 'black'
set label "R = 100R" at 340,490 font " ,30"
set label "R = 10k" at 40,490 font " ,30"

plot \
         '../log/Current_0_200u_GAIN0.dat' u ($1*1000000):2 title "Gain 0 (0000)" ls 11 w lines, \
         '../log/Current_0_200u_GAIN1.dat' u ($1*1000000):2 title "Gain 1 (0001)" ls 12 w lines, \
         '../log/Current_0_200u_GAIN2.dat' u ($1*1000000):2 title "Gain 2 (0010)" ls 13 w lines, \
         '../log/Current_0_200u_GAIN3.dat' u ($1*1000000):2 title "Gain 3 (0011)" ls 14 w lines, \
         '../log/Current_0_200u_GAIN4.dat' u ($1*1000000):2 title "Gain 4 (0100)" ls 15 w lines, \
         '../log/Current_0_200u_GAIN5.dat' u ($1*1000000):2 title "Gain 5 (0101)" ls 16 w lines, \
         '../log/Current_0_200u_GAIN6.dat' u ($1*1000000):2 title "Gain 6 (0110)" ls 17 w lines, \
         '../log/Current_0_200u_GAIN7.dat' u ($1*1000000):2 title "Gain 7 (0111)" ls 18 w lines, \
         '../log/Current_0_200u_GAIN8.dat' u ($1*1000000):2 title "Gain 8 (1000)" ls 19 w lines, \
         '../log/Current_0_200u_GAIN9.dat' u ($1*1000000):2 title "Gain 9 (1001)" ls 20 w lines, \
         '../log/Current_0_200u_GAIN10.dat' u ($1*1000000):2 title "Gain 10 (1010)" ls 22 w lines, \
         '../log/Current_0_200u_GAIN11.dat' u ($1*1000000):2 title "Gain 11 (1011)" ls 23 w lines, \
         '../log/Current_0_200u_GAIN12.dat' u ($1*1000000):2 title "Gain 12 (1100)" ls 24 w lines, \
         '../log/Current_0_200u_GAIN13.dat' u ($1*1000000):2 title "Gain 13 (1101)" ls 25 w lines, \
         '../log/Current_0_200u_GAIN14.dat' u ($1*1000000):2 title "Gain 14 (1110)" ls 26 w lines, \
         '../log/Current_0_200u_GAIN15.dat' u ($1*1000000):2 title "Gain 15 (1111)" ls 27 w lines, \

set output "Output/Current_f_Gain_Zoom2.eps"
#set yrange [0:300]
set xrange [0:200]
set key center right

plot \
         '../log/Current_0_200u_GAIN0.dat' u ($1*1000000):2 title "Gain 0 (0000)" ls 11 w lines, \
         '../log/Current_0_200u_GAIN1.dat' u ($1*1000000):2 title "Gain 1 (0001)" ls 12 w lines, \
         '../log/Current_0_200u_GAIN2.dat' u ($1*1000000):2 title "Gain 2 (0010)" ls 13 w lines, \
         '../log/Current_0_200u_GAIN3.dat' u ($1*1000000):2 title "Gain 3 (0011)" ls 14 w lines, \
         '../log/Current_0_200u_GAIN4.dat' u ($1*1000000):2 title "Gain 4 (0100)" ls 15 w lines, \
         '../log/Current_0_200u_GAIN5.dat' u ($1*1000000):2 title "Gain 5 (0101)" ls 16 w lines, \
         '../log/Current_0_200u_GAIN6.dat' u ($1*1000000):2 title "Gain 6 (0110)" ls 17 w lines, \
         '../log/Current_0_200u_GAIN7.dat' u ($1*1000000):2 title "Gain 7 (0111)" ls 18 w lines, \
         '../log/Current_0_200u_GAIN8.dat' u ($1*1000000):2 title "Gain 8 (1000)" ls 19 w lines, \
         '../log/Current_0_200u_GAIN9.dat' u ($1*1000000):2 title "Gain 9 (1001)" ls 20 w lines, \
         '../log/Current_0_200u_GAIN10.dat' u ($1*1000000):2 title "Gain 10 (1010)" ls 22 w lines, \
         '../log/Current_0_200u_GAIN11.dat' u ($1*1000000):2 title "Gain 11 (1011)" ls 23 w lines, \
         '../log/Current_0_200u_GAIN12.dat' u ($1*1000000):2 title "Gain 12 (1100)" ls 24 w lines, \
         '../log/Current_0_200u_GAIN13.dat' u ($1*1000000):2 title "Gain 13 (1101)" ls 25 w lines, \
         '../log/Current_0_200u_GAIN14.dat' u ($1*1000000):2 title "Gain 14 (1110)" ls 26 w lines, \
         '../log/Current_0_200u_GAIN15.dat' u ($1*1000000):2 title "Gain 15 (1111)" ls 27 w lines, \

set output "Output/Current_f_Gain_Zoom3.eps"
set yrange [0:400]
set xrange [0:20]
set arrow from 3.20,0 to 3.20,270 nohead lw 3 lc rgb 'black'
set label "R = 10k" at 3.4,5 font " ,25"
set label "R = 1M" at 1,5 font " ,25"
set key top left

plot \
         '../log/Current_0_200u_GAIN0.dat' u ($1*1000000):2 title "Gain 0 (0000)" ls 11 w lines, \
         '../log/Current_0_200u_GAIN1.dat' u ($1*1000000):2 title "Gain 1 (0001)" ls 12 w lines, \
         '../log/Current_0_200u_GAIN2.dat' u ($1*1000000):2 title "Gain 2 (0010)" ls 13 w lines, \
         '../log/Current_0_200u_GAIN3.dat' u ($1*1000000):2 title "Gain 3 (0011)" ls 14 w lines, \
         '../log/Current_0_200u_GAIN4.dat' u ($1*1000000):2 title "Gain 4 (0100)" ls 15 w lines, \
         '../log/Current_0_200u_GAIN5.dat' u ($1*1000000):2 title "Gain 5 (0101)" ls 16 w lines, \
         '../log/Current_0_200u_GAIN6.dat' u ($1*1000000):2 title "Gain 6 (0110)" ls 17 w lines, \
         '../log/Current_0_200u_GAIN7.dat' u ($1*1000000):2 title "Gain 7 (0111)" ls 18 w lines, \
         '../log/Current_0_200u_GAIN8.dat' u ($1*1000000):2 title "Gain 8 (1000)" ls 19 w lines, \
         '../log/Current_0_200u_GAIN9.dat' u ($1*1000000):2 title "Gain 9 (1001)" ls 20 w lines, \
         '../log/Current_0_200u_GAIN10.dat' u ($1*1000000):2 title "Gain 10 (1010)" ls 22 w lines, \
         '../log/Current_0_200u_GAIN11.dat' u ($1*1000000):2 title "Gain 11 (1011)" ls 23 w lines, \
         '../log/Current_0_200u_GAIN12.dat' u ($1*1000000):2 title "Gain 12 (1100)" ls 24 w lines, \
         '../log/Current_0_200u_GAIN13.dat' u ($1*1000000):2 title "Gain 13 (1101)" ls 25 w lines, \
         '../log/Current_0_200u_GAIN14.dat' u ($1*1000000):2 title "Gain 14 (1110)" ls 26 w lines, \
         '../log/Current_0_200u_GAIN15.dat' u ($1*1000000):2 title "Gain 15 (1111)" ls 27 w lines, \


