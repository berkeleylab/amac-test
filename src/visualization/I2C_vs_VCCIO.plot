set style line 11 lw 3 pt 2 ps 2 lt 1 lc rgb '#ff6600'

set terminal postscript enhanced color "Helvetica" 26 eps size 8,6
set output "Output/I2C_v_VCCIO.eps"
set grid
set lmargin 10
set ytics
set xlabel "VCC_H [V]"
set ylabel "Success Rate [%]"
set autoscale
#set yrange [0:1100]
#set xrange [0:1.2]
#set key top left

plot \
         '../log/I2CTest_I2C_.log' u 1:($2*100):(sqrt($2*100)) notitle ls 11 w yerrorbars


