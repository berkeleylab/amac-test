set style line 11 lw 4 pt 2 ps 2 lt 1 lc rgb '#0072bd' # blue
set style line 12 lw 2 pt 2 ps 2 lt 1 lc rgb '#a2142f' # red
set style line 13 lw 2 pt 1 ps 1 lt 1 lc rgb '#7e2f8e' # purple
set style line 14 lw 2 pt 1 ps 1 lt 1 lc rgb '#77ac30' # green

set terminal postscript enhanced color "Helvetica" 21 eps size 8,8
set output "Output/Vchanleft3_f_Gain.eps"
set grid
set lmargin 10
set autoscale
set ytics
set xlabel "Input Voltage [V]"
set ylabel "Register Value"
set yrange [0:1000]
set key off

set multiplot layout 2,2

set title "Gain 0x0"
plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_RampGain0.log' u 1:2 title "Voltage" ls 11 w lines

set title "Gain 0x1"
plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_RampGain1.log' u 1:2 title "Voltage" ls 11 w lines

set title "Gain 0x2"
plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_RampGain2.log' u 1:2 title "Voltage" ls 11 w lines

set title "Gain 0x3"
plot \
         '../log/VoltageTest_DVDDsupply_ADC_V_CH3_L_RampGain3.log' u 1:2 title "Voltage" ls 11 w lines

